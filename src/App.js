import './Styles/App.sass';
import './Styles/Homepage/Homepage.sass';
import Homepage from './Components/Homepage/index'
import Javascript from './Components/Javascript/index'
import Testing from './Components/Testing/index'
import Docker from './Components/Docker/index'
import CleanCode from './Components/CleanCode/index'
import Git from './Components/Git/index'
import NPM from './Components/NPM/index'
import CICD from './Components/CICD/index'
import Typescript from './Components/Typescript/index'

import React, { useEffect, useState } from 'react'
import UI from './Components/UI/index'
import {
  BrowserRouter as Router,
  Routes,
  Route,
  Link
} from "react-router-dom";

function App() {
  const [activeComponent, setActiveComponent] = useState("Javascript")
  const [navigationVisibility, setNavigationVisibility] = useState(true)

  useEffect(() => {
    const accordionItems = document.querySelectorAll('h3')
    for (const accordionItem of accordionItems) {
      accordionItem.addEventListener('click', (e) => {
        accordionItem.classList.toggle('opened');
        accordionItem.nextElementSibling?.classList.toggle('visible');
      })
    }
    const navigationItems = document.querySelectorAll('.navigation-box')
    for (const navigationItem of navigationItems) {
      navigationItem.addEventListener('click', (e) => {
        navigationItem.classList.toggle('opened');
        if (navigationItem.nextElementSibling?.classList.contains('invisible')) {
          navigationItem.nextElementSibling?.classList.toggle('visible');
        }
      })
    }

  }, [activeComponent]);

  function handleActiveComponent(value) {
    setActiveComponent(value)
  }

  return (
    <div className="app-container">
      <UI
        activeComponent={activeComponent}
        handleActiveComponent={handleActiveComponent}
      />

      <div className="content">
        <Routes>
          <Route path="/" element={<Homepage />} />
          <Route path="/javascript" element={<Javascript />} />
          <Route path="/typescript" element={<Typescript />} />
          <Route path="/testy" element={<Testing />} />
          <Route path="/czysty-kod" element={<CleanCode />} />
          <Route path="/docker" element={<Docker />} />
          <Route path="/npm" element={<NPM />} />
          <Route path="/git" element={<Git />} />
          <Route path="/cicd" element={<CICD />} />
        </Routes>
      </div>
    </div>
  );
}

export default App;
