import React, { useEffect, useState } from "react";
import { GithubFilled, SettingFilled, DownOutlined } from "@ant-design/icons";
import { CSSTransition } from "react-transition-group";

import { Navigation as JavascriptNavigation } from "../../Javascript/Navigation/Navigation";
import { Navigation as TestingNavigation } from "../../Testing/Navigation/Navigation";
import { Navigation as DockerNavigation } from "../../Docker/Navigation/Navigation";
import { Navigation as CleanCodeNavigation } from "../../CleanCode/Navigation/Navigation";
import { Navigation as NPMNavigation } from "../../NPM/Navigation/Navigation";
import { Navigation as GitNavigation } from "../../Git/Navigation/Navigation";
import { Navigation as CICDNavigation } from "../../CICD/Navigation/Navigation";
import { Navigation as TypescriptNavigation } from "../../Typescript/Navigation/Navigation";

import { BrowserRouter as Router, Routes, Route, Link } from "react-router-dom";

function TableOfContents(props) {
  const { activeComponent, navigationVisibility } = props;

  useEffect(() => {});

  function onChange(e) {}

  return (
    <>
      <div
        className={`table-of-contents ${
          navigationVisibility ? "" : "disabled"
        }`}
      >
        <div
          className={`navigation-container ${
            navigationVisibility ? "" : "disabled"
          }`}
        >
          <Routes>
            <Route path="/javascript" element={<JavascriptNavigation />} />
            <Route path="/typescript" element={<TypescriptNavigation />} />
            <Route path="/testy" element={<TestingNavigation />} />
            <Route path="/czysty-kod" element={<CleanCodeNavigation />} />
            <Route path="/docker" element={<DockerNavigation />} />
            <Route path="/npm" element={<NPMNavigation />} />
            <Route path="/git" element={<GitNavigation />} />
            <Route path="/cicd" element={<CICDNavigation />} />
          </Routes>
        </div>
      </div>
    </>
  );
}

export default TableOfContents;
