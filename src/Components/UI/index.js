
import Navbar from './Navbar/Navbar'
import TableOfContents from './TableOfContents/TableOfContents'
import React, { useState } from "react";
import { useLocation } from 'react-router-dom'

export const UI = (props) => {

    const [navigationVisibility, setNavigationVisibility] = useState(true);
    const { activeComponent, handleActiveComponent } = props;

    function handleNavigationVisibility(value) {
        setNavigationVisibility(value)
    }
    
    const location = useLocation();
    console.log(location.pathname);

    return (
        <>
            <Navbar
                handleNavigationVisibility={handleNavigationVisibility}
                handleActiveComponent={handleActiveComponent}
                navigationVisibility={navigationVisibility}
                activeComponent={activeComponent}
            />
            {(location.pathname != "/") ?
            <TableOfContents
                navigationVisibility={navigationVisibility}
                activeComponent={activeComponent} /> : ""}
            
        </>

    );
}

export default UI;