import React, { useEffect, useState } from "react";
import { GithubFilled, SettingFilled, DownOutlined } from "@ant-design/icons";
import { CSSTransition } from "react-transition-group";
import { Link } from "react-router-dom";

function Navbar(props) {
  const [contentDropdownVisible, setContentDropdownVisible] = useState(false);
  const [settingsDropdownVisible, setSettingsDropdownVisible] = useState(false);
  const [theme, setTheme] = useState("dark");

  useEffect(() => {});

  useEffect(() => {
    const themeValue = localStorage.getItem("theme");

    if (themeValue === "light") {
      document.querySelector(".content").classList.add("light");
    } else if (themeValue === "dark") {
      document.querySelector(".content").classList.remove("light");
    } else {
      localStorage.setItem("theme", theme);
    }
  }, [theme]);

  function onChange(e) {}

  function contentDropdownHandler(name) {
    props.handleActiveComponent(name);
    setContentDropdownVisible(false);
  }
  function settingsDropdownHandler() {}

  return (
    <>
      <div className="top-navbar">
        <div className="top-navbar-container">
          <div className="top-navbar-left">
            <div
              className="top-navbar-hamburger"
              onClick={() => {
                props.handleNavigationVisibility(!props.navigationVisibility);
                console.log("clicked");
                document.querySelectorAll("h3").forEach((h3) => {
                  if (props.navigationVisibility) {
                    h3.style.marginLeft = "2rem";
                    h3.style.paddingLeft = "4rem";
                    h3.style.marginRight = "2rem";
                  } else {
                    h3.style.marginLeft = "0rem";
                    h3.style.paddingLeft = "7rem";
                    h3.style.marginRight = "0rem";
                  }
                });
              }}
            >
              <div className="top-navbar-hamburger-line top-navbar-hamburger-line1"></div>
              <div className="top-navbar-hamburger-line top-navbar-hamburger-line2"></div>
              <div className="top-navbar-hamburger-line top-navbar-hamburger-line3"></div>
            </div>
            <Link to="/">
              <div href="" className="top-navbar-logo">
                <div className="top-navbar-logo-white">Front-End</div>
                <div className="top-navbar-logo-colored">Notebook</div>
              </div>
            </Link>
          </div>
          <div className="top-navbar-right">
            <div
              className="top-navbar-active-component"
              onClick={() => {
                setContentDropdownVisible(!contentDropdownVisible);
                setSettingsDropdownVisible(false);
              }}
            >
              {props.activeComponent}
              <div className="top-navbar-active-component-arrow">
                <DownOutlined />
              </div>
              <CSSTransition
                in={contentDropdownVisible}
                timeout={500}
                classNames="my-node"
                unmountOnExit
              >
                <div className="top-navbar-dropdown">
                  <div className="top-navbar-dropdown-container">
                    <Link to="/javascript">
                      <div
                        className="nav-option"
                        onClick={() => contentDropdownHandler("Javascript")}
                      >
                        JavaScript
                      </div>
                    </Link>
                    <Link to="/typescript">
                      <div
                        className="nav-option"
                        onClick={() => contentDropdownHandler("TypeScript")}
                      >
                        TypeScript
                      </div>
                    </Link>
                    <Link to="/git">
                      <div
                        className="nav-option"
                        onClick={() => contentDropdownHandler("Git")}
                      >
                        Git
                      </div>
                    </Link>
                    <Link to="/testy">
                      <div
                        className="nav-option"
                        onClick={() => contentDropdownHandler("Testing")}
                      >
                        Testy
                      </div>
                    </Link>

                    <Link to="/npm">
                      <div
                        className="nav-option"
                        onClick={() => contentDropdownHandler("NPM")}
                      >
                        NPM
                      </div>
                    </Link>
                    <Link to="/docker">
                      <div
                        className="nav-option"
                        onClick={() => contentDropdownHandler("Docker")}
                      >
                        Docker
                      </div>
                    </Link>
                    <Link to="/czysty-kod">
                      <div
                        className="nav-option"
                        onClick={() => contentDropdownHandler("CleanCode")}
                      >
                        Czysty Kod
                      </div>
                    </Link>
                    <Link to="/cicd">
                      <div
                        className="nav-option"
                        onClick={() => contentDropdownHandler("CICD")}
                      >
                        CI/CD
                      </div>
                    </Link>
                  </div>
                </div>
              </CSSTransition>
            </div>

            <div className="top-navbar-icons">
              <div className="top-navbar-icon top-navbar-settings-icon">
                <SettingFilled
                  onClick={() => {
                    setSettingsDropdownVisible(!settingsDropdownVisible);
                    setContentDropdownVisible(false);
                  }}
                />
                <CSSTransition
                  in={settingsDropdownVisible}
                  timeout={500}
                  classNames="my-node"
                  unmountOnExit
                >
                  <div className="settings-dropdown">
                    <div className="settings-dropdown-container">
                      <div
                        className="settings-option"
                        onClick={() => {
                          const themeValue = localStorage.getItem("theme");
                          if (themeValue === "dark") {
                            setTheme("light");
                            localStorage.setItem("theme", "light");
                            document.body.classList.add("light");
                          } else if (themeValue === "light") {
                            setTheme("dark");
                            localStorage.setItem("theme", "dark");
                            document.body.classList.remove("light");
                          }
                        }}
                      >
                        Zmień motyw (to-do)
                      </div>
                    </div>
                  </div>
                </CSSTransition>
              </div>
              <a href="https://gitlab.com/matt765/frontend-notebook" className="top-navbar-github-link"  target="_blank">
                <div className="top-navbar-icon top-navbar-github-icon">
                  <GithubFilled />
                </div>
              </a>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default Navbar;
