import SyntaxHighlighter from "react-syntax-highlighter";
import { a11yDark } from "react-syntax-highlighter/dist/esm/styles/hljs";

function Intro() {
  return (
    <>
      <h3>Wprowadzenie</h3>
      <div class="text-section">
        <ul class="visible">
          <li>
            TypeScript jest językiem programowania opracowanym przez Microsoft,
            będącym typowanym supersetem JavaScriptu i zawierającym własny
            kompilator. TypeScript może wyłapywać błędy i usterki na długo przed
            uruchomieniem aplikacji.{" "}
          </li>
          <li>
            Aby użyć TS z Reactem należy dodać go do dependencji, skonfigurować
            plik tsconfig.json i używac odpowiednich rozszerzeń plików
          </li>
          <li>
            Plik .ts możemy przekompilować do pliku .js używając polecenia „tsc
            nazwaPliku.ts”
          </li>
          <li>
            Przy deklaracji zmiennej i przypisaniu jej wartości bez wyznaczonego
            typu, TypeScript automatycznie przypisuje typ danych tej wartości do
            zmiennej (Type Inference)
          </li>
          <li>
            Tablice raz zadeklarowane z danym typem zawartości pozostają z tym
            typem przypisanym, TypeScript nie pozwala wtedy na zmianę wybranej
            wartości z tablicy na inny typ danych. Czyli jeśli tablica ma np. 2
            typy, to możemy dodać do niej wartości tylko w tych 2 typach.
          </li>
          <li>
            TypeScript nie pozwala na dodawanie nowych par klucz-wartość do raz
            zadeklarowanego obiektu
          </li>
          <li>
            W celu modyfikacji zawartości obiektu musimy użyć deklaracji z taką
            samą strukturą typów danych i ilością par klucz-wartość
          </li>
          <li>
            Typy mogą być predefiniowane z użyciem słowa ‘type’ lub ‘Interface’
          </li>
          <li>Folder src – pliki TS, folder dist – pliki przekompilowane</li>
          <li>Nowy projekt CRA z TS</li>
          <SyntaxHighlighter
            language="javascript"
            style={a11yDark}
          >{`npx create-react-app my-app --template typescript`}</SyntaxHighlighter>
          <li>Dodanie TS do istniejącego projektu</li>
          <SyntaxHighlighter
            language="javascript"
            style={a11yDark}
          >{`npm install --save-dev typescript`}</SyntaxHighlighter>
        </ul>
      </div>
      <h3>Typy</h3>
      <div class="text-section">
        <ul class="visible">
          <li>number</li>
          <li>string</li>
          <li>boolean</li>
          <li>any</li>
          <li>tuple</li>
          <li>bigint</li>
          <li>symbol</li>
        </ul>
      </div>
      <h3>tsconfig.json</h3>
      <div class="text-section">
        Plik konfiguracyjny
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`npx tsc --init`}</SyntaxHighlighter>
      </div>
      <h3>TypeScript Compiler</h3>
      <div class="text-section">
        Plik .ts można przekompilować na .js z użyciem poniższego polecenia.
        Type annotations i type assertions są usuwane przez kompilator
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`ts-crash % tsc nazwapliku
ts-crash % tsc nazwapliku –watch // na żywo
ts-crash % tsc // kompiluje wszystkie pliki .ts`}</SyntaxHighlighter>
        Domyślnie kod jest kompilowany na ES5 (bez const/let)
      </div>
      <h3>Type Annotation</h3>{" "}
      <div class="text-section">
        Deklaracja typu zazwyczaj nie jest potrzebna, ponieważ Typescript sam
        przypisuje typ (Type Inference)
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`let myName: string = "Alice";`}</SyntaxHighlighter>
      </div>
      <h3>Type Assertion</h3>{" "}
      <div class="text-section">
        Umożliwia pobranie wartości zmiennej z zadeklarowaniem nowego typu.
        Konwersja jest możliwa tylko na bardziej lub mniej specyficzny typ
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`let value: any = 1
let secondValue = <number>value
lub
let secondValue = value as number`}</SyntaxHighlighter>
        W ponizszym przypadku deklarujemy że typ to nie każdy HTMLElement a
        tylko HTMLCansasElement
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`const myCanvas = document.getElementById("main_canvas") as HTMLCanvasElement`}</SyntaxHighlighter>
      </div>
      <h3>Aliasy (type)</h3>{" "}
      <div class="text-section">
        Pozwalają na zadeklarowanie typu wcześniej, keyword 'type'
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`type ID = number | string;

type Point = {
  x: number;
  y: number;
};
  
// Exactly the same as the earlier example
function printCoord(pt: Point) {
  console.log("The coordinate's x value is " + pt.x);
  console.log("The coordinate's y value is " + pt.y);
}
  
printCoord({ x: 100, y: 100 });

type UserInputSanitizedString = string;
  
function sanitizeInput(str: string): UserInputSanitizedString {
  return sanitize(str);
}`}</SyntaxHighlighter>
      </div>
      <h3>Interfejsy</h3>
      <div class="text-section">
        Podobnie jak aliasy, używane do predefiniowania typów
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`interface Point {
  x: number;
  y: number;
}
  
function printCoord(pt: Point) {
  console.log("The coordinate's x value is " + pt.x);
  console.log("The coordinate's y value is " + pt.y);
}`}</SyntaxHighlighter>
        Deklaracja interfejsu opcjonalnie i readonly
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`interface UserInterface {
  readonly id: number
  name: string
  age?: number
}`}</SyntaxHighlighter>
        Deklaracja interfejsu z funkcją z parametrami zwracającą numer.
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`interface MathFunc {
	(x: number, y: number) : number
}

const add: MathFunc = (x: number, y: number): number => x+y // ‘y: string’ wyrzuci błąd
const sub: MathFunc = (x: number, y: number): number => x-y`}</SyntaxHighlighter>
      </div>
      <h3>Zmienne</h3>
      <div class="text-section">
        Deklaracja zmiennej z deklaracją typu i wartością undefined
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`let name: typeName
let apple: string;`}</SyntaxHighlighter>
        Deklaracja zmiennej z dowolnym typem
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`let cats: any = 25`}</SyntaxHighlighter>
      </div>
      <h3>Funkcje</h3>
      <div class="text-section">
        W nawiasie możemy podać jaki typ argumentów przyjmuje funkcja. Gdy potem
        zostanie ona wywołana Typescript sprawdzi czy typ parametru jest
        odpowiedni. Przy przekazywaniu parametrów funkcji w przypadku braku
        określenia ich typu, TypeScript wskaże jako błąd, że typ to „any”.
        Wyłączenie: tsconfig + noImplicitAny
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`function addNum(x: number, y: number): number {
  return x+y
}`}</SyntaxHighlighter>
        Poza nawiasem możemy zadeklarować jaki typ danych funkcja zwraca
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`function getFavoriteNumber(): number {
  return 26;
}`}</SyntaxHighlighter>
        Jeśli funkcja nic nie zwraca, jej typ należy określić jako ‘void’
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`function addNum(x: number, y: number): void {
  console.log(x+y)
}`}</SyntaxHighlighter>
        Funkcja przyjmująca obiekt z opcjonalną wartością
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`function printName(obj: { first: string; last?: string }) {
  // ...
}`}</SyntaxHighlighter>
      </div>
      <h3>Tuple</h3>
      <div class="text-section">
        Umożlwia określenie typów elementów tablicy
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`let person: [number, string, boolean] = [1, ‘John’, true]`}</SyntaxHighlighter>
        Deklaracja tablicy tuple’ów
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`let employee: [number, string][] 
employee = [ [1, John,], [2, Jeff], [3, Ted]]`}</SyntaxHighlighter>
      </div>
      <h3>Tablice</h3>
      <div class="text-section">
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`const ids: number[] = [1, 2, 3, 4, 5]
const arr: any[] = [1, true, ‘Hello’]
generics -  Array<number>`}</SyntaxHighlighter>
        Deklaracja tablicy stringów
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`let name: string[] = [];`}</SyntaxHighlighter>
        Deklaracja tablicy z różnymi typami danych
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`let name: (string|number|boolean)[] = [];`}</SyntaxHighlighter>
      </div>
      <h3>Union</h3>
      <div class="text-section">
        Pozwala na określenie różnych typów dla jednej zmiennej
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`let value: string | number
value: ‘12’`}</SyntaxHighlighter>
        Funkcja z parametrem union
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`function printId(id: number | string) {
  console.log("Your ID is: " + id);
}’`}</SyntaxHighlighter>
        Wywołanie funkcji na parametrze z Union Type działa tylko jeśli ta
        funkcja działa z oboma typami, można to obejść przez if statement
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`function welcomePeople(x: string[] | string) {
  if (Array.isArray(x)) {
    // Here: 'x' is 'string[]'
    console.log("Hello, " + x.join(" and "));
  } else {
    // Here: 'x' is 'string'
    console.log("Welcome lone traveler " + x);
  }
}`}</SyntaxHighlighter>
        Union types wraz z opcją zadeklarowania np. konkretnego stringa jako
        typu, pozwalają na okreslenie wartości mają mieć parametry
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`function printText(s: string, alignment: "left" | "right" | "center") {
  // ...
}
printText("Hello, world", "left");
printText("G'day, mate", "centre");
Argument of type '"centre"' is not assignable to parameter of type '"left" | "right" | "center"'.`}</SyntaxHighlighter>
      </div>
      <h3>Enums</h3>
      <div class="text-section">
        Pozwala na zadeklarowanie zestawu stałych wraz z przypisaniem im
        wartości numerycznych, zaczynających się od 0
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`enum Direction {
	Up,
	Down,
	Left,
	Right
}`}</SyntaxHighlighter>
        Można też przypisać im wartość string
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`enum Direction {
Up = ‘Up’,
	Down = ‘Down’,
	Left = ‘Left’,
	Right = ‘Right’
}
console.log(Direction.Right) // Right`}</SyntaxHighlighter>
      </div>
      <h3>Obiekty</h3>
      <div class="text-section">
        Deklaracja obiektu
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`let user: object;
user = { name: ‘John’, age: 30 };`}</SyntaxHighlighter>
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`const user: {
  id: 1,
  name: string
} = {
  id: 5,
  name: ‘John’
}`}</SyntaxHighlighter>
        Deklaracja obiektu z predefiniowanymi typami z użyciem słowa ‘type’
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`type User = {
  id: number,
  name: string
}

const user: User = {
  id: ‘1’,
  name: ‘John’,
}`}</SyntaxHighlighter>
        Funkcja przyjmująca obiekt jako parametr
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`function printCoord(pt: { x: number; y: number }) {
  console.log("The coordinate's x value is " + pt.x);
  console.log("The coordinate's y value is " + pt.y);
}
printCoord({ x: 3, y: 7 });`}</SyntaxHighlighter>
        Deklaracja obiektu z predefiniowanymi typami z użyciem interfejsu
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`interface User  {
  id: number,
  name: string
}

const user: UserInterface = {
  id: ‘1’,
  name: ‘John’,
}`}</SyntaxHighlighter>
        Deklaracja obiektu z predefiniowanymi typami z użyciem interfejsu
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`interface User  {
  id: number,
  name: string
}

const user: UserInterface = {
  id: ‘1’,
  name: ‘John’,
}`}</SyntaxHighlighter>
      </div>
      <h3>Generics</h3>
      <div class="text-section">
        Używane są do tworzenia komponentów używalnych ponownie Treść pomiędzy
        znakami wiekszy niz/mniejszy niz to ‘placeholder’ w który możemy potem
        wpisać typ
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`function getArray<T>(items: T[]): t[] {
  return new Array()concat(items)
}
let numArray = getArray<number>([1,2,3,4])
let strArray = getArray<string>([‘john’, ‘william’, ‘brad’])
numArray.push(‘hello’) // wyrzuci błąd`}</SyntaxHighlighter>
      </div>
      <h3>Komponent w React</h3>
      <div class="text-section">
        Header.tsx
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`export interface Props {
  title: string
  color?: string
}

const Header = (props: Props) => {
  return (
    <header>
      <h1 style={{color: props.color ? props.color : ‘blue’}}>{props.title}</h1>
    </header>
  )
}

export default Header`}</SyntaxHighlighter>
        App.tsx
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`import Header from ‘./Header’

function App() {
return (
    <div classname=’App’>
       <Header title=’Hello World’ color=’red’ />
    </div>
  )
)

export default App`}</SyntaxHighlighter>
      </div>
    </>
  );
}

export default Intro;
