
import Start from './Start/Start'
import Intro from './Sections/Intro'

function NPM() {
    return (
        <div className="App">
            <div className="main-container">

                <div className="text-container">
                    <div className="start-container" id="start">
                        <Start />
                    </div>
                    <div className="text-box" >                       
                        <Intro />
                    </div>

                </div>
            </div>
        </div>
    );
}

export default NPM;