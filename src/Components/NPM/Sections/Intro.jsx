import SyntaxHighlighter from "react-syntax-highlighter";
import { a11yDark } from "react-syntax-highlighter/dist/esm/styles/hljs";

function Intro() {
  return (
    <>
      <h3 id="">Wprowadzenie </h3>
      <div className="text-section">
        <ul class="visible">
          <li>Npm jest menedżerem pakietów dla języka JavaScript</li>
          <li>
            Zainstalowane paczki zlokalizowane są w folderze node_modules.
            Zazwyczaj jest to największy i najcięższy folder w naszym projekcie.
            Node_modules nie umieszczamy ani na serwerze produkcyjnym, ani w
            repozytorium. Dodajemy go do .gitignore
          </li>
          <li>
            W środowisku Node.js pakiety możemy instalować globalnie na cały
            komputer oraz lokalnie per dany projekt. Globalnie instalujemy
            zwykle dodatkowe narzędzia do których chcemy mieć dostęp ze
            wszystkich projektów, np. live-server
          </li>
          <li>
            Od strony działania pakietów nie ma znaczenia do której sekcji trafi
            dany pakiet w pliku package.json. Można to traktować jako sprawy
            porządkowe. W razie gdy dany pakiet zostanie zapisany w złej sekcji
            tego pliku, możemy go albo przeinstalować dodając odpowiednią flagę,
            albo ręcznie edytować ten plik po prostu przenosząc odpowiednie
            nazwy z sekcji do sekcji.
          </li>
          <li>
            W nowych wersjach npm dostajemy także narzędzie npx. Polecenie to
            służy do odpalania pakietów. Jeżeli w danym katalogu jest
            zainstalowany lokalny pakiet o danej nazwie, polecenie to spróbuje
            go odpalić. Jeżeli takiego pakietu nie ma, ściągnie go, odpali bez
            instalacji w node_modules, a po zakończonej pracy usunie.
          </li>
          <li>
            "^" - aktualna wersja minor, "~" aktualna wersja patch, "*" -
            najnowsza wersja
          </li>
          <li>
            Przy skonfigurowanym Webpacku możemy używać dependencji poprzez
            "import from 'X'" lub require('X')
          </li>
        </ul>
      </div>
      <h3>Polecenia</h3>
      <div className="text-section">
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`npx <nazwa paczki> - uruchomienie wybranej paczki

npm init - stworzenie nowego projektu / pliku package.json
npm init --yes - 'yes' na wszystkie pytania
npm help - lista komend

npm --version - wersja NPM
npm view <nazwa paczki> version --json - wersja paczki

npm install - zainstalowanie wszystkich paczek domyslnych i developerskich z package.json
npm install --production - zainstalowanie tylko domyslnych paczek z package.json, bez developerskich
npm install <nazwa paczki> - zainstalowanie paczki z dodaniem do package.json jako normalne dependencje
npm install <nazwa paczki> --save - zainstalowanie paczki z dodaniem do package.json jako normalne dependencje
npm install <nazwa paczki> --dev - zainstalowanie zaleznosci developerskich przypisanych do danej paczki (domyślnie jest false)
npm install <nazwa paczki> --global  - instalacja paczki globalnie
npm install <nazwa paczki> --save-prod - zainstalowanie paczki z dodaniem do package.json jako normalne dependencje
npm install <nazwa paczki> --save-dev - zainstalowanie paczki jako zaleznosci developerskiej, wykorzystywanej w fazie developmentu (devDependencies)
npx install-peerdeps react - peer?
npm install <nazwa paczki> --save-optional - w przypadku konkretnych funkcjonalnosci, nie jest wymagana do działania projektu
npm install <nazwa paczki> --save-bundle -?

npm install <nazwa paczki 1> <nazwa paczki 2> - zainstalowanie dwoch paczek naraz
npm install --only=production - gdy na serwerze instalujemy paczki bez tych w devDependencies
npm install <nazwapaczki>@wersja --save - zainstalowanie konkretnej wersji
npm install <nazwa paczki> --loglevel verbose - instalowanie paczki z zapisaniem logów do npm-debug.log

npm list - lista dependencji
npm list --depth 0 - lista głównych tytułów dependencji

npm update <nazwa paczki> - aktualizacja paczek
npm outdated - stan zainstalowanych pakietów
npm uninstall <nazwa paczki> - odinstalowanie paczki
npm run <nazwa polecenia> - uruchomienie polecenia`}</SyntaxHighlighter>
      </div>
      <h3>Package.json i zależności</h3>

      <div className="text-section">
        Package.json jest plikiem w formacie JSON, który istnieje w każdym
        projekcie opartym na Node.js. Jest to plik zawierający informacje o
        projekcie i zależnościach. Gdy inicjujemy nowy projekt zazwyczaj jest
        już dostarczany gotowy, ale możemy go stworzyć sami komendą npm init.
        Jeśli plik już istnieje wystarczy użyć komendy npm install, aby npm
        zainstalował wszystkie zarejestrowane paczki. Powstaje wtedy też plik
        package-lock.json opisujący dokładnie wersje paczek
        <ul className="visible">
          <li>
            dependencies - zależności niezbędne do działania aplikacji, które
            będą obecne w finalnej wersji projektu
            <SyntaxHighlighter
              language="javascript"
              style={a11yDark}
            >{`npm install react    //domyślna instalacja
npm install react --save-prod
npm install react -P
npm i react
npm i react -P`}</SyntaxHighlighter>
          </li>

          <li>
            devDependencies - zależności które nie lądują na produkcji, a
            wykorzystywane są podczas tworzenia aplikacji, np. narzędzia do
            sprawdzania kodu, zarządzania projektem, pisania testów
            <SyntaxHighlighter
              language="javascript"
              style={a11yDark}
            >{`npm install react --save-dev
npm install react -D
npm i react --save-dev
npm i react -D`}</SyntaxHighlighter>
          </li>

          <li>
            peerDependencies - zależności niezbędne do działania aplikacji,
            uzywane głównie przy tworzeniu paczek do npm, gdy aplikacja wymaga
            konkretnej wersji tej zaleznosci, tzn. gdy ktoś zainstaluje zrobioną
            przez nas paczkę i ma juz daną zaleznosc (którą ta paczka
            wykorzystuje) w innej wersji, to pojawi się błąd
            <SyntaxHighlighter
              language="javascript"
              style={a11yDark}
            >{`npx install-peerdeps react`}</SyntaxHighlighter>
          </li>

          <li>
            optionalDependencies - zależności opcjonalne, rzadko używane
            <SyntaxHighlighter
              language="javascript"
              style={a11yDark}
            >{`npm install react --save-optional
npm install react -O
npm i react --save-optional
npm i react -O`}</SyntaxHighlighter>
          </li>

          <li>
            bundleDependencies - podobne do zwykłch dependenscies, pakiety
            pakowane podczas publikowania pakietu
            <SyntaxHighlighter
              language="javascript"
              style={a11yDark}
            >{`npm install react --save-bundle
npm install react -B
npm i react --save-bundle
npm i react -B`}</SyntaxHighlighter>
          </li>
        </ul>
      </div>

      <h3> Konfiguracja</h3>
      <div className="text-section">
        NPM bazuje swoje ustawienia na terminalu, zmiennych srodowiskowych i
        plikach npmrc. 4 podstawowe pliki .npmrc. Priorytet od góry do dołu,
        większy priorytet nadpisuje mniejszy
        <ul className="visible">
          <li>konfiguracja projektu (/path/to/my/project/.npmrc) </li>
          <li>
            konfiguracja dla użytkownika (~/.npmrc) - CLI: --userconfig, ENV:
            $NPM_CONFIG_USERCONFIG
          </li>
          <li>
            konfiguracja globalna ($PREFIX/etc/npmrc) - CLI: --globalconfig,
            ENV: $NPM_CONFIG_GLOBALCONFIG
          </li>
          <li>konfiguracja npm built-in (/path/to/npm/npmrc)</li>
        </ul>
        Lokalny plik .npmrc powinien być umieszczony w głównym katalogu
        projektu. Komenda npm config pozwala na edycję globalnych i lokalnych
        plików nmrc. Domyślnie edytuje npmrc użytkownika
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`npm config ls -l - lista parametrów konfiguracji npm, -l(long) pokazuje również domyślne
npm config list [--json] - lista w formacie json
npm config set <key>=<value> [<key>=<value> ...] - ustawienie wybranej wartości
npm config get [<key> [<key> ...]] - pobiera wybraną wartość
npm config delete <key> [<key> ...] - usuwa wybraną wartość
npm config edit - otwiera config w edytorze tekstu
npm set <key>=<value> [<key>=<value> ...]
npm get [<key> [<key> ...]]

Konfiguracja npm config
json - format json
global - config globalny
editor - do npm edit, wybór edytora tekstu
location - default:user, do wyboru global/user/project
long - wszystkie informacje`}</SyntaxHighlighter>
      </div>
      <h3>Zmienne środowiskowe</h3>
      <div className="text-section">
        {/*Zmienne środowiskowe mogą być zastąpione z użyciem ${NAZWA_ZMIENNEJ}*/}
        <ul className="visible">
		 <li>
            Zmiennych środowiskowych możemy używać po zainstalowaniu paczki dotenv. Trzymane są w pliku 'env' który dopisujemy do .gitignore
          </li>
          <li>
            Zmienne środowiskowe zaczynające się od "npm_config_" są traktowane
            jako parametr konfiguracji npm
          </li>
          <li>
            Wartości konfiguracyjne npm są case-insensitive, czyli
            NPM_CONFIG_FOO=bar zadziała jak npm_config_foo=bar
          </li>
          <li>Należy używać znaków podłogi (_) zamiast myślników (-)</li>
        </ul>
      </div>
      <h3>Import paczek z Gitlab Package Registry</h3>
      <div className="text-section">
        Gdy wpisujemy w terminal npm install boostrap, pakiet zostaje
        zaciągnięty z oficjalnego rejestru NPM: registry.npmjs.org. To on jest
        ustawiony jako domyślny. Oprócz tego istnieją inne rejestry, z reguły
        prywatne repozytoria pakietów prywatnych firm. Jeśli posiada się własny
        serwer www, można taki rejestr stworzyć samemu, dla swoich prywatnych
        pakietów. Można ustawić globalnie rejestr inny niż npmjs. Służy do tego
        plik konfiguracyjny .npmrc umieszczany w katalogu domowym. Samą zmianę
        wykonujemy poleceniem
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`npm config set registry <link_do_mojego_prywatnego_rejestru> - przestawienie na prywatny rejestr
npm config set registry https://registry.npmjs.org/ - powrót do domyślnej konfiguracji
npm install <pakiet> --registry <link_do_mojego_prywatnego_rejestru> - instalacja jednego modulu z prywatnego rejestru`}</SyntaxHighlighter>
        <a href="https://www.youtube.com/watch?v=OqS6jb22AFE">
          https://www.youtube.com/watch?v=OqS6jb22AFE
        </a>
        W projekcie udostępniającym paczkę
        <ol>
          <li>Utworzenie projektu</li>
          <li>
            Konfiguracja package.json <br />
            - scope paczki, dodanie "@nickZGitlaba/" do "name"
            <br />
            - wskazanie plików/folderów do udostępnienia - "files":[]
            <br />- dodanie "publishConfig"
          </li>
          <li>
            Utworzenie access token w settings > repository > deploy token
          </li>
          <li>
            Dodanie access tokena jako variable do settings > ci/cd > variables
          </li>
          <li>Utworzenie pliku .npmrc i umieszczenie w nim tokena</li>
          <li>Dodanie .npmrc do .gitignore</li>
          <li>Dodanie CI/CD z "npm publish"</li>
          <li>Push</li>
        </ol>
        <ol>
          W projekcie wykorzystującym paczkę <br />
          <br />
          <li>
            Utworzenie .npmrc
            <br />
            <SyntaxHighlighter
              language="javascript"
              style={a11yDark}
            >{`@nickGitlab:registry=[adres z gitlaba]<br />
//[adres z gitlaba]:_authToken=[token]`}</SyntaxHighlighter>
          </li>
          <li>
            Instalacja i import
            <SyntaxHighlighter
              language="javascript"
              style={a11yDark}
            >{`npm i @nickGitlab/nazwaPaczki<br />
import X from '@nickGitlab/nazwaPaczki'`}</SyntaxHighlighter>
          </li>
        </ol>
      </div>
      <h3>Skrypty</h3>
      <div className="text-section">
        W pliku package.json mamy możliwość definiowania poleceń. W sekcji
        'scripts' możemy zamieszczać własne skrypty, które potem będziemy
        odpalać bezpośrednio z terminala. Najczęściej są one wykorzystywane do
        uruchamiania lokalnych pakietów, które dość często wymagają dodatkowych
        parametrów.
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`npm run <nazwa skryptu>`}</SyntaxHighlighter>
        Konwersja SASS na CSS
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`npm i sass -D

npx sass src/scss/main.scss dist/css
lub
skrypt: "sass": "sass src/scss/main.scss dist/css/main.css --style=compressed",
npm run sass`}</SyntaxHighlighter>
        Przykład grupowania zadań w skryptach
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`"build:all": "npm run build:css && npm run build:images && npm run build:sass"`}</SyntaxHighlighter>
        Przykłady skryptów
        <SyntaxHighlighter language="javascript" style={a11yDark}>{`"scripts": {
  "dev": "webpack-dev-server --mode development --progress --hot --open",
  "build": "webpack --mode production --progress",
  "test": "echo \"Error: no test specified\" && exit 1",
  "prestart"  : "echo '--- rozpoczynam ---'",
  "start"     : "echo '--- działam ---'",
  "poststart" : "echo '--- kończę ---'"
},`}</SyntaxHighlighter>
        Obserwowanie zmian w plikach
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`npm i onchange -D
"watch:css": "onchange \"src/scss/**/*.scss\" -- npm run build:css"`}</SyntaxHighlighter>
        Równoczesne odpalanie skryptów
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`npm i npm-run-all -D
 "start": "npm-run-all --parallel watch:css watch:images watch:js"`}</SyntaxHighlighter>
      </div>
      <h3>Skróty</h3>
      <div className="text-section">
        <a href="https://docs.npmjs.com/cli/v8/using-npm/config">
          https://docs.npmjs.com/cli/v8/using-npm/config
        </a>
        <SyntaxHighlighter language="javascript" style={a11yDark}>{`-a: --all
--enjoy-by: --before
-c: --call
--desc: --description
-f: --force
-g: --global
-L: --location
-d: --loglevel info
-s: --loglevel silent
--silent: --loglevel silent
--ddd: --loglevel silly
--dd: --loglevel verbose
--verbose: --loglevel verbose
-q: --loglevel warn
--quiet: --loglevel warn
-l: --long
-m: --message
--local: --no-global
-n: --no-yes
--no: --no-yes
-p: --parseable
--porcelain: --parseable
-C: --prefix
--readonly: --read-only
--reg: --registry
-S: --save
-B: --save-bundle
-D: --save-dev
-E: --save-exact
-O: --save-optional
-P: --save-prod
-?: --usage
-h: --usage
-H: --usage
--help: --usage
-v: --version
-w: --workspace
--ws: --workspaces
-y: --yes
rm: remove / uninstall`}</SyntaxHighlighter>
        Można wpisać tylko część słowa i to zadziała jeśli te znaki są unikalne
        dla danego parametru
        <SyntaxHighlighter language="javascript" style={a11yDark}>{`npm ls --par
# same as:
npm ls --parseable`}</SyntaxHighlighter>
        Skróty można łączyć
        <SyntaxHighlighter language="javascript" style={a11yDark}>{`npm ls -gpld
# same as:
npm ls --global --parseable --long --loglevel info`}</SyntaxHighlighter>
      </div>
    </>
  );
}

export default Intro;
