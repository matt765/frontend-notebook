import SyntaxHighlighter from "react-syntax-highlighter";
import { a11yDark } from "react-syntax-highlighter/dist/esm/styles/hljs";

function Intro() {
  return (
    <>
      <h3 id="">Wprowadzenie </h3>
      <div className="text-section">
        <ul class="visible">
          <li>
            Git to rozproszony system kontroli wersji. Śledzi on wszystkie
            zmiany dokonywane na pliku (lub plikach) i umożliwia przywołanie
            dowolnej wcześniejszej wersji.
          </li>
          <li>
            Scentralizowane systemy kontroli wersji (CVCS) składają się z
            jednego serwera, który zawiera wszystkie pliki poddane kontroli
            wersji, oraz klientów którzy mogą się z nim łączyć i uzyskać dostęp
            do najnowszych wersji plików. Przez wiele lat był to standardowy
            model kontroli wersji. Gdy cała historia projektu jest przechowywana
            tylko w jednym miejscu, istnieje ryzyko utraty większości danych. W
            ten sposób dochodzimy do rozproszonych systemów kontroli wersji
            (DVCS - Distributed Version Control System){" "}
          </li>
          <li>
            W systemach DVCS (takich jak Git, Mercurial, Bazaar lub Darcs)
            klienci nie dostają dostępu jedynie do najnowszych wersji plików ale
            w pełni kopiują całe repozytorium. Gdy jeden z serwerów, używanych
            przez te systemy do współpracy, ulegnie awarii, repozytorium każdego
            klienta może zostać po prostu skopiowane na ten serwer w celu
            przywrócenia go do pracy{" "}
          </li>
          <img src="https://git-scm.com/book/en/v2/images/snapshots.png" />
          <li>
            Git nie musi łączyć się z serwerem, aby pobrać historyczne dane -
            zwyczajnie odczytuje je wprost z lokalnej bazy danych. Oznacza to,
            że dostęp do historii jest niemal natychmiastowy. Jeśli chcesz
            przejrzeć zmiany wprowadzone pomiędzy bieżącą wersją pliku, a jego
            stanem sprzed miesiąca, Git może odnaleźć wersję pliku sprzed
            miesiąca i dokonać lokalnego porównania. Nie musi w tym celu prosić
            serwera o wygenerowanie różnicy, czy też o udostępnienie
            wcześniejszej wersji pliku.{" "}
          </li>
          <li>
            Dla każdego obiektu Git wyliczana jest suma kontrolna SHA-1 przed
            jego zapisem i na podstawie tej sumy można od tej pory odwoływać się
            do danego obiektu. Oznacza to, że nie ma możliwości zmiany
            zawartości żadnego pliku, czy katalogu bez reakcji ze strony Git.{" "}
          </li>
          <li>
            3 stany plików
            <ol class="visible">
              <li>
                Zmodyfikowany - oznacza, że plik został zmieniony, ale zmiany
                nie zostały wprowadzone do bazy danych.
              </li>
              <li>
                Śledzony (po git add ~) - oznacza, że zmodyfikowany plik trafił
                do poczekali (Staging Area) i został przeznaczony do
                zatwierdzenia w bieżącej postaci w następnej operacji commit.
              </li>
              <li>
                Zatwierdzony (git commit ~) - oznacza, że dane zostały
                bezpiecznie zachowane w Twojej lokalnej bazie danych.
              </li>
            </ol>
          </li>
          <img src="https://git-scm.com/book/en/v2/images/areas.png" />
        </ul>
      </div>
      <h3 id="">Nowy użytkownik</h3>
      <div class="text-section">
        Opcja --global oznacza, że dane te zostaną wykorzystane w kazdym
        projekcie
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`git config --global user.name "John Doe"
git config --global user.email johndoe@example.com`}</SyntaxHighlighter>
        Jeśli skorzystasz z opcji --global wystarczy, że taka konfiguracja
        zostanie dokonana jednorazowo. Git skorzysta z niej podczas każdej
        operacji wykonywanej przez Ciebie w danym systemie. Jeśli zaistnieje
        potrzeba zmiany tych informacji dla konkretnego projektu, można
        skorzystać z git config bez opcji --global.
      </div>
      <h3 id="">Konfiguracja</h3>
      <div class="text-section">
        Git posiada trzypoziomową konfigurację. Każdy kolejny poziom jest
        bardziej szczegółowy i można z niego nadpisać bardziej ogólną
        konfigurację.
        <ol class="visible">
          <li>
            systemowa (–system) – jest to najbardziej ogólna konfiguracja
            widoczna z poziomu całego systemu operacyjnego. Przechowywana jest w
            pliku tekstowym /etc/gitconfig
          </li>
          <li>
            użytkownika (–global) – to konfiguracja dla aktualnie zalogowanego
            użytkownika do systemu. Przechowywana jest w pliku: ~/.gitconfig
          </li>
          <li>
            repozytorium (–local) – najbardziej szczegółowy poziom ustawień.
            Jest to konfiguracja dla konkretnego repozytorium, która jest
            przechowywana w pliku .git/config. Konfiguracja z tego poziomu ma
            najwyższy priorytet.
          </li>
        </ol>
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`git config --list - biezace ustawienia`}</SyntaxHighlighter>
      </div>
      <h3 id="">Rozpoczęcie pracy</h3>
      <div class="text-section">
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`git init - nowy repozytorium git
git clone <adres> - sklonowanie istniejącego repozytorium
git clone <adres> <folder> – skopiowanie zdalnego repozytorium z URL do wybranego folderu`}</SyntaxHighlighter>
      </div>
      <h3 id=""> Branche / gałęzie</h3>
      <div class="text-section">
        Domyślna nazwa gałęzi to master. Wskaźnik HEAD pokazuje na którym
        branchu aktualnie jesteśmy.
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`git branch <nazwa> – stworzenie nowego brancha
git checkout <nazwa> – przełączenie brancha
git checkout -b <nazwa>- stworzenie brancha i przełączenie na niego
git branch -a - lista wszystkich branchy
git branch -r - lista branchy zdalnych`}</SyntaxHighlighter>
      </div>
      <h3 id="">Praca na plikach lokalnych</h3>
      <div class="text-section">
        <ol>
          <li>Modyfikacja plików w katalogu roboczym </li>
          <li>Oznaczenie zmodyfikowanych</li>
          plików jako śledzonych (add) umieszczając je w przechowalni.
          <li>
            Zatwierdzenie zmian (commit), podczas którego zawartość plików z
            przechowalni zapisywana jest jako migawka projektu w lokalnym
            katalogu Git.
          </li>
        </ol>
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`git add <nazwa> - sledzenie nowego pliku (dodanie go do poczekalni)
git add -A - sledzenie wszystkich niesledzonych plikow
git add . - sledzenie wszystkich plikow
git commit - zatwierdzenie zmian i zapisanie ich w lokalnym repozytorium
git commit --amend –  dodaje pozostałe pliki z poczekalni do ostatniego commita
git commit -a -m 'nazwa' - pominięcie poczekalni i dodanie nazwy commita
git status – sprawdzenie śledzonych plików
git status -s / git status -- short - wersja skrótowa
git diff – porównuje katalog roboczy z poczekalnią, tzn. pokazuje co zostało zmodyfikowane ale jest nieśledzone
git diff --staged - porównuje poczekalnię z repozytorium, tzn. pokazuje co zostanie zcommitowane
git rm – usunięcie pliku z poczekalni i katalogu roboczego
git rm --cached <nazwa> - usuniecie pliku/katalogu z poczekalni
git reset HEAD <plik> - usunięcie wybranego pliku z poczekalni
git checkout -- <plik> - cofnięcie nieśledzonego zmodyfikowanego pliku do stanu z ostatniego commita
git mv file.from file.to - zmiana nazwy
`}</SyntaxHighlighter>

        <ul class="visible">
          <li>
            Kiedy zmieniasz pliki, Git rozpoznaje je jako zmodyfikowane,
            ponieważ różnią się od ostatniej zatwierdzonej zmiany. Zmodyfikowane
            pliki umieszczasz w poczekalni poleceniem GIT ADD, a następnie
            zatwierdzasz oczekujące tam zmiany i tak powtarza się cały cykl.
            Nowo zmodyfikowany plik po wpisaniu 'git status' pojawia się w
            sekcji „Changes not staged for commit“ (Zmienione ale nie
            zaktualizowane), co oznacza, że śledzony plik został zmodyfikowany,
            ale zmiany nie trafiły jeszcze do poczekalni.
          </li>

          <li>
            Git umieszcza plik w poczekalni dokładnie z taką zawartością, jak w
            momencie uruchomienia polecenia git add. Jeśli plik zostanie
            zmodyfikowany po tym poleceniu, 'git status' pokaze, że plik jest
            jednoczesnie w poczekalni i poza nią. Dlatego jeśli modyfikujesz
            plik po uruchomieniu git add, musisz ponownie użyć git add, aby
            najnowsze zmiany zostały umieszczone w poczekalni
          </li>

          <li>
            Git traktuje dane podobnie jak zestaw migawek (ang. snapshots)
            małego systemu plików. Za każdym razem jak tworzysz commit lub
            zapisujesz stan projektu, Git tworzy obraz pprzedstawiający to jak
            wyglądają wszystkie pliki w danym momencie i przechowuje referencję
            do tej migawki. W celu uzyskania dobrej wydajności, jeśli dany plik
            nie został zmieniony, Git nie zapisuje ponownie tego pliku, a tylko
            referencję do jego poprzedniej, identycznej wersji, która jest już
            zapisana.
          </li>
        </ul>
      </div>
      <h3 id="">Workflow</h3>
      <div class="text-section">
        <ul class="visible">
          <li>
            Zdalne repozytorium to wersja twojego projektu utrzymywana na
            serwerze dostępnym poprzez Internet lub inną sieć. Możesz mieć ich
            kilka, z których każde może być tylko do odczytu lub zarówno odczytu
            jak i zapisu. Współpraca w grupie zakłada zarządzanie zdalnymi
            repozytoriami oraz wypychanie zmian na zewnątrz i pobieranie ich w
            celu współdzielenia pracy/kodu.
          </li>
          <li>
            Po sklonowaniu repozytorium automatycznie zostanie dodany skrót o
            nazwie origin wskazujący na oryginalną lokalizację. Domyślnie,
            polecenie git clone ustawia twoją lokalną gałąź główną master tak
            aby śledziła zmiany w zdalnej gałęzi master na serwerze z którego
            sklonowałeś repozytorium (zakładając, że zdalne repozytorium posiada
            gałąź master). Gałąź "śledzona" to gałąź zdalna z którą
            automatycznie będzie łączyła się gałąź lokalna przy użyciu poleceń
            pull/push
          </li>
          <SyntaxHighlighter
            language="javascript"
            style={a11yDark}
          >{`git remote – pokazuje wszystkie skonfigurowane połączenia do zdalnych repozytoriów
git remote set-url origin git://new.url.here - zmiana url
git remote –v – pokazuje URL
git remote add <skrót> <URL> – dodanie nowego zdalnego repozytorium
git remote show <skrót> - informacje o zdalnym repozytorium
git branch --set-upstream my_branch origin/my_branch - ustawienie trackingu
git branch --set-upstream-to=origin/main main - ustawienie trackingu w inny sposób`}</SyntaxHighlighter>
          <li>
            Git nie pozwoli nam wrzucić swoich zmian na serwer, jeśli nie mamy
            zsynchronizowanego repozytorium. Przed wrzuceniem naszej pracy
            musimy ściągnąć ostatnie commity wrzucone przez innych. Czyli
            polecenie git push zadziała tylko jeśli sklonowałeś repozytorium z
            serwera do którego masz prawo zapisu oraz jeśli nikt inny w
            międzyczasie nie wypchnął własnych zmian. Jeśli zarówno ty jak i
            inna osoba sklonowały dane w tym samym czasie, po czym ta druga
            osoba wypchnęła własne zmiany, a następnie ty próbujesz zrobić to
            samo z własnymi modyfikacjami, twoja próba zostanie od razu
            odrzucona(rejected). Będziesz musiał najpierw zespolić (pobrać i
            scalić) najnowsze zmiany ze zdalnego repozytorium zanim będziesz
            mógł wypchnąć własne. Uruchomienie GIT PULL pobiera dane z serwera
            na bazie którego oryginalnie stworzyłeś swoje repozytorium i próbuje
            automatycznie scalić zmiany z kodem roboczym nad którym aktualnie,
            lokalnie pracujesz. Czyli git pull jest równoważne z wykonaniem 2
            poleceń: git fetch i git merge origin/master
          </li>
          <li>
            Polecenie GIT FETCH pobiera dane i zapisuje na "lokalnym zdalnym"
            branchu, tzn. po pobraniu danego brancha jest on oznaczony jako
            zdalny i tylko do odczytu. Wszystkie zdalne branche wyswietla
            polecenie git branch -r.{" "}
          </li>
          <SyntaxHighlighter
            language="javascript"
            style={a11yDark}
          >{`git pull -  automatycznie pobiera dane (fetch) i je scala (merge) z lokalnymi plikami.
git pull --rebase -  czyli git fetch + git rebase, uzywane gdy zmiany "nie zasluguja" na utworzenie nowego brancha
git fetch <skrót> – pobiera wszystkie branche ze zdalnego repozytorium
git fetch origin <nazwa brancha> - fetchuje wybrany branch
git branch -r - pokazuje sfetchowane zdalne branche`}</SyntaxHighlighter>
          <li>
            Standardowy workflow polega na tym, że lokalnie tworzymy nowy branch
            na podstawie lokalnego mastera którego pobraliśmy z serwera i na nim
            pracujemy. Najczęściej aby wypchnąć swoje zmiany na zdalne
            repozytorium najpierw pobiera się (git pull) na lokalnego mastera
            zmiany, sprawdza się czy nie ma konfliktów i wtedy pushuje się go na
            zdalne repozytorium na nowy branch aby tam zrobić MR.
          </li>

          <ol>
            <li>Commit lokalnych zmian na lokalnym branchu</li>
            <li>Pull ze zdalnego mastera na lokalny master</li>
            <li>Zmergowanie lokalnego brancha z lokalnym masterem</li>
            <li>Sprawdzenie konfliktów</li>
            <li>Push lokalnego mastera na nowy zdalny branch</li>
          </ol>
          <li>
            Jeśli w czasie developmentu na lokalnym branchu zdalny master
            przesunął się o 1 lub więcej commitów należy najpierw na lokalnym
            masterze pobrać zmiany ze zdalnego mastera. Następnie gdy chcemy
            zmergować nasz nowy lokalny branch z lokalnym masterem mamy do
            wyboru 2 podstawowe drogi: GIT REBASE lub GIT MERGE
          </li>
          <li>
            Polecenie GIT MERGE [nazwa brancha] wpisane na masterze powoduje
            przyłączenie wybranego brancha do lokalnego mastera. Powstaje tzw.
            merge commit, czyli jeden wspólny commit. Zaletą merge jest prostota
            bo nie modyfikujemy poprzedniego kodu – po prostu tworzymy na samej
            górze dodatkowy commit, który zakańcza rozwidlenie gałęzi. Wadą
            rozwiązania jest fakt, że przy każdym merge, powstaje dodatkowy
            commit, co zwykle utrudnia przeglądanie historii.
          </li>
          <SyntaxHighlighter
            language="javascript"
            style={a11yDark}
          >{`git merge <branch> - wpisane na masterze powoduje zmergowanie wybranego brancha z masterem
git merge origin/master - zmergowanie sfetchowanego zdalnego mastera z lokalnym masterem
git merge --abort - w przypadku konfliktów, anuluje merge commit i próbuje przywrócić poprzedni stan plików
git merge --continue - kontynuacja mergowania po rozwiązaniu konfliktu`}</SyntaxHighlighter>
          <img src="http://www.pzielinski.com/wp-content/uploads/2015/07/image_thumb9.png" />
          <li>
            Polecenie GIT REBASE MASTER uruchomione na lokalnym branchu sprawi,
            że w historii commitów tego brancha commitem z mastera z którego
            stworzono branch (czyli bazowym commitem) stanie się ostatni commit
            z aktualnego lokalnego mastera, a nie ten pierwotny, który był
            ostatnim commitem mastera w momencie utworzenia brancha. Czyli
            wszystkie commity które ktoś w międzyczasie dodał do mastera są
            dzięki temu poleceniu "włączane" do historii commitów tego brancha.
            Oznacza to, że git rebase pozwala na zmianę bazy danego brancha z
            jednego commitu na inny, sprawiając że dany branch wygląda na
            stworzony z innego commita. Branch może wyglądać tak samo ale ma
            zmodyfikowaną historię commitów. Dzięki temu nasz branch będzie
            wyglądał jakby "dopiero co" został stworzony z aktualnego ostatniego
            commita z lokalnego mastera i zapobiegnie to powstaniu konfliktu.
            Głównym powodem używania rebasingu jest utrzymanie ciągłości
            historii projektu. Wg niektórych programistów warto rebazować jak
            najczęściej(np. 2 razy dziennie). Po przełączeniu na mastera i
            zmergowaniu brancha, nasze własne commity z brancha są dodawane do
            historii commitów mastera. Rebase powoduje, ze historia zmian jest
            liniowa i przejrzysta
          </li>
          <img src="http://www.pzielinski.com/wp-content/uploads/2015/07/image_thumb10.png" />
          <img src="https://i.stack.imgur.com/1tGHe.png" />
        </ul>
      </div>
      <h3>Squash</h3>
      <div className="text-section">
        Sposób na przepisanie historii commitów poprzez połączenie wielu
        commitów w jeden. Pomaga oczyścić i uprościć historię commitów przed
        podzieleniem się swoją pracą z członkami zespołu. Nie ma do tego
        oddzielnego polecenia, tzn. squashing wykonujemy poprzez dodanie go jako
        parametru przy uzyciu merge lub rebase. Używamy go gdy niepotrzebna jest
        widoczność wszystkich zmian i commitów, które zachodziły w momencie prac
        nad daną funkcjonalnością
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`git merge --squash feature/login`}</SyntaxHighlighter>
      </div>
      <h3>Push</h3>
      <div className="text-section">
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`git push <skrót> <branch> - wypchnięcie zmian na zdalne repozytorium
git push --set-upstream origin master - wypchniecie na mastera wraz z ustawieniem sledzenia`}</SyntaxHighlighter>
      </div>

      <h3>Amend</h3>
      <div className="text-section">
        Najczęściej wykorzystywaną opcją modyfikacji historii jest edycja
        ostatniego komita. Dzięki mechanizmowi amend commit można modyfikować
        treść komentarza najnowszego komita oraz jego zawartość.
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`git commit –amend -a -m „Edit history”`}</SyntaxHighlighter>
      </div>
      <h3>Cherry pick</h3>
      <div className="text-section">
        Dołączenie wybranej zmiany (można wskazać więcej niż jedną) do obecnej
        gałęzi. Wymagane jest aby obecne repozytorium było czyste (brak
        modyfikacji od ostatniej rewizji).{" "}
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`git cherry-pick <nr commita>`}</SyntaxHighlighter>
      </div>
      <h3>Konflikty scalania</h3>
      <div className="text-section">
        Najczęściej powstają, gdy dany fragment kodu jest edytowany na różnych
        gałęziach. Wywołanie git status pokazuje, których plików nie udało się
        połączyć. Poprawiamy kod oraz komitujemy go do repozytorium. Trzeba też
        usunąć znaczniki dodane podczas merga. Przeniesienie pliku do poczekalni
        i jego zacommitowanie(?) oznacza w Gicie rozwiązanie konfliktu. <br />
        Po rozwiązaniu konfliktu wpisujemy flagę --continue, np. git rebase
        --continue
      </div>
      <h3> Historia commitów (rewizji)</h3>
      <div className="text-section">
        Po kilku rewizjach, lub w przypadku sklonowanego repozytorium
        zawierającego już własną historię, przyjdzie czas, że będziesz chciał
        spojrzeć w przeszłość i sprawdzić dokonane zmiany. Najprostszym, a
        zarazem najsilniejszym, służącym do tego narzędziem jest git log.
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`git log - historia commitów
git log -p -2 - historia wraz z pokazaniem zmian (-p = diff) i ograniczeniem do 2 wyników
git log --stat - statystyki kazdego commita
git log --pretty=oneline - inny prostszy format
git log --since=2.weeks - ostatnie 2 tygodnie
git log -Sfunction_name - przyjmuje ciąg i pokazuje tylko te rewizje w których dodano lub usunięto ten ciąg`}</SyntaxHighlighter>
      </div>
      <h3>Cofanie zmian</h3>
      <div className="text-section">
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`git reset - odwrócenie zmian do stanu z lokalnego repozytorium
git revert <nr commita> - utworzenie commita wycofującego, tzn. przeciwienstwa zmiany z wybranego commita
git reset --soft <nr commita> - cofniecie do wybranego commita, bez usuwania zmian po drodze, zmiany są dodane do poczekalni
git reset --mixed <nr commita> -   cofniecie do wybranego commita, bez usuwania zmian po drodze, bez  dodania do poczekalni
git reset --hard <nr commita> -  cofniecie do wybranego commita, usunięcie wszystkich zmian po drodze`}</SyntaxHighlighter>
      </div>
      <h3>Stash</h3>
      <div className="text-section">
        Git Stash to komenda pozwalająca na umieszczanie zmian w schowku, który
        jest mechanizmem będącym zupełnie odseparowanym od commitów i loga. Nie
        ma możliwości wypchnięcia go do zdalnego repo, a dane w schowku zwykle
        są tam tymczasowo. Domyślnie powyższa komenda doda do schowka tylko
        zmiany ze śledzonych plików. Usunięcie wybranej zmiany z listy
        stash-commitów powoduje reindeksację listy
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`git stash - umieszczenie zmian w schowku, dotyczy plików z poczekalni
git stash list - wykaz "stash-commitów" z numeracją
git stash show stash@{1} - podobna to git diff, pokazuje różnice z aktualną wersją
git stash apply stash@{1} - przywrócenie kodu bez usuwania ich ze schowka, bez dodania do poczekalni
git stash apply stash@{1} --index - przywrócenie z dodaniem do poczekalni
git stash drop stash@{1} - usunięcie zawartości wybranego schowka
git stash pop stash@{1} - połączenie apply i drop, przywrócenie kodu i jego usunięcie
git stash clear - wyczyszczenie wszystkich schowków`}</SyntaxHighlighter>
      </div>
      <h3>Git Flow</h3>
      <div className="text-section">
        Git flow jest koncepcją, która wprowadza nam określoną strukturę gałęzi
        w naszym repozytorium. Wprowadzenie lekkiego i szybkiego modelu
        zarządzania gałęziami w Gicie przyczyniło się do powstania wielu różnych
        schematów pracy, opartych o rozgałęzianie kodu. Częścią wspólną tych
        modeli jest podział na gałęzie długotrwałe oraz krótkotrwałe. Na
        gałęziach długotrwałych utrzymywany jest stabilny kod, który zmienia się
        raczej rzadko. Natomiast gałęzie krótkotrwałe przeznaczone są na rozwój
        aplikacji oraz naprawę błędów. Kod powstały w gałęziach krótkotrwałych
        jest przenoszony podczas mergu do gałęzi długotrwałych.
        <ul className="visible">
          <li>
            Master – gałąź produkcyjna. Ostatni commit na tej gałęzi, jest
            aktualną działającą wersją naszej aplikacji. Bazowe źródło
            wszystkich innych branchy
          </li>
          <li>
            Hotfix – gałąź poprawek. Gałąź ta służy do wprowadzania wszelkiego
            rodzaju poprawek do istniejących już wersji. Zmiany z Hotfix
            trafiają też na branche develop i feature
          </li>
          <li>
            Release – gałąź wydania, pośredniczy pomiędzy develop i master.
            Gałąź ta służy do przygotowania stabilnej wersji oprogramowania z
            gałęzi developerskiej. Zawiera gotowe działające wersje nowych
            zmian. Po skoczeniu release’u jest on mergowany z odpowiednimi
            gałęziami i dodawany jest odpowiedni tag który symbolizuje daną
            wersje{" "}
          </li>
          <li>
            Develop – gałąź rozwojowa. Na tej gałęzi dokonywane są wszelkiego
            rodzaju zmiany czy implementacje nowych rozwiązań. Kod z tej gałęzi
            posłuży nam do tworzenia nowych release’ów naszego oprogramowania.
            Zmiany na branchu develop są mergowane do aktualnych branchy
            feature.
          </li>
          <li>
            Feature – gałąź funkcjonalności. Przy wprowadzaniu zmian lub
            implementacji większej funkcjonalności warto utworzyć nową gałąź
            typu Feature. Po skończeniu zmiany są mergowane do gałęzi develop
          </li>
        </ul>
        <img src="http://datasift.github.io/gitflow/GitFlowHotfixBranch.png" />
      </div>
      <h3> Przykładowy plik .gitignore</h3>
      <div className="text-section">
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`# no .a files
*.a
# but do track lib.a, even though you're ignoring .a files above
!lib.a
# only ignore the root TODO file, not subdir/TODO
/TODO
# ignore all files in the build/ directory
build/
# ignore doc/notes.txt, but not doc/server/arch.txt
doc/*.txt
# ignore all .txt files in the doc/ directory
doc/**/*.txt`}</SyntaxHighlighter>
      </div>
      <h3>Tagowanie</h3>
      <div className="text-section">
        Git posiada możliwość etykietowania konkretnych, ważnych miejsc w
        historii. Ogólnie, większość użytkowników korzysta z tej możliwości do
        zaznaczania ważnych wersji kodu (np. wersja 1.0, itd.)
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`git tag - wyswietla wszystkie dostępne tagi/etykiety
git tag -l 'v1.8.5*' - wyswietla tagi z wybranym ciągiem znaków`}</SyntaxHighlighter>
      </div>
      <h3>Pozostałe polecenia</h3>
      <div className="text-section">
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`git help - lista komend
git help <verb> - pomoc do konkretnego tematu`}</SyntaxHighlighter>
      </div>
      <h3>Skróty</h3>

      <div className="text-section">
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`A: addition of a file
D: deletion of a file
U: file is unmerged (you must complete the merge before it can be committed)
C: copy of a file into a new one
D: deletion of a file
M: modification of the contents or mode of a file
R: renaming of a file
T: change in the type of the file
X: "unknown" change type (most probably a bug, please report it)`}</SyntaxHighlighter>
      </div>
    </>
  );
}

export default Intro;
