import SyntaxHighlighter from "react-syntax-highlighter";
import { a11yDark } from "react-syntax-highlighter/dist/esm/styles/hljs";

function Intro() {
  return (
    <>
      <h3 id="">Wprowadzenie </h3>
      <div className="text-section">
        Continuous integration (CI) i Continuous Delivery (CD) to zbiór zasad,
        wytycznych, kultura pracy i kolekcja dobrych praktyk dotyczących pracy
        nad projektami informatycznymi. Dzięki nim zespół developerów ma
        możliwość częstszego dostarczania pewnych, przetestowanych i
        sprawdzonych zmian w kodzie. Implementacja tych praktyk często nazywana
        jest CI/CD pipeline i jest uważana za jeden z najlepszych i
        najefektywniejszych sposobów pracy nad projektami informatycznymi i ich
        rozwojem.

        <img src="https://docs.gitlab.com/ee/ci/introduction/img/gitlab_workflow_example_11_9.png"/>
      </div>
      <h3 id=""> Continuous integration (CI)</h3>
      <div className="text-section">
        Filozofia wprowadzania zmian w kodzie aplikacji, która polega na tym, że
        zespół developerów wprowadza szereg drobnych zmian w kodzie, które
        następnie okresowo sprawdzane są w repozytorium wersji kodu. CI to
        nieustanne, automatyczne buildowanie programu na dedykowanym serwerze po
        każdym commicie. Przy wykorzystaniu repozytorium GIT, automatyczna
        integracja rozpoczyna się po podaniu komendy „git push”. Następnie na
        dedykowanym serwerze następuje zautomatyzowany proces buildowania
        aplikacji. Później uruchamiany jest zestaw testów zapisanych w kodzie.
      </div>
      <h3 id=""> Continuous delivery (CD)</h3>
      <div className="text-section">
        Continuous delivery rozpoczyna się tam, gdzie continuous integration się
        kończy. CD automatyzuje proces wdrażania aplikacji i wprowadzanych zmian
        w kodzie do przygotowanej infrastruktury serwerowej. Ciągłe dostarczanie
        polega na automatycznym dostarczaniu działającej wersji programu do
        ostatniego środowiska przed produkcją. Automatyczne dostarczenie
        wyzwalane jest po udanym merge’u gałęzi kodu. Mówiąc o działającej
        wersji aplikacji mamy oczywiście na myśli aplikację, która jest w stanie
        się zbudować, ale niekoniecznie posiada wszystkie wymagane
        funkcjonalności, a jej działanie nie musi być do końca prawidłowe. (?? sprawdzić)
        <br /> <br />
        Ciągłe dostarczanie to etap procesu wytwarzania oprogramowania, który
        poprzedza Continous Deployment, czyli ciągłe wdrażanie na produkcję. Do
        produkcji powinny trafiać tylko w pełni funkcjonujące wersje aplikacji,
        stąd konieczność sprawdzenia jej na wcześniejszych etapach.
        <br /> <br />
        Typowy CD pipeline zawiera kroki takie jak:
        <ol>
          Pobranie kodu z repozytorium wersji i uruchomienie build’a <br />
          Automatyczne wykonanie wszystkich niezbędnych kroków dotyczących
          infrastruktury <br />
          Przeniesienie / transfer kodu na wybrane środowisko <br />
          Zarządzanie zmiennymi środowiskowymi i konfiguracja ich dla
          konkretnego przypadku <br />
          Przeniesienie komponentów aplikacji do odpowiadających im usług (np.
          usługi WEB’owe, usługi API, usługi bazodanowe) <br />
          Wykonanie wszystkich niezbędnych restartów usług <br />
          Wykonanie testów (Continous Tests) i cofnięcie zmian w przypadku
          błędów <br />
          Zwrócenie wynikow, udostępnienie logów zdarzeń <br />
          Providing log data and alerts on the state of the delivery. <br />
          <br />
        </ol>
        Wiele zespołów zajmujących się tworzeniem aplikacji implementuje
        rozwiązania CI/CD w oparciu o rozwiązania w chmurze przy wykorzystaniu
        konteneryzacji np. Docker lub Kubernetes.
      </div>
      <h3 id="">Continuous testing (CT)</h3>
      <div className="text-section">
        CI oraz CD wymagają ciągłego przeprowadzania testów (Continous Testing),
        ponieważ celem jest dostarczenie wysokiej jakości aplikacji i kodu do
        użytkownika końcowego. CT jest zazwyczaj implementowane jako zbiór
        automatycznie przeprowadzanych testów np. wydajnościowych czy testów
        regresji. Proces ten zazwyczaj jest częścią procesu CI/CD (pipeline).
      </div>
      <h3>Gitlab</h3>
      <div className="text-section">
        Po zacommitowaniu zmian w odpowiedniej gałęzi (przekazaniu ich do
        zdalnego repozytorium) GitLab uruchamia automatyczny potok (ang.
        pipeline) CI/CD dla projektu. W ten sposób GitLab uruchamia skrypty
        budujące i testujące aplikację, umożliwia przegląd zmian według
        merge’ów, tak samo jak w przypadku lokalnego hosta. Po udanej
        implementacji użytkownik otrzymuje sprawdzony i zaakceptowany kod, może
        zmerge’ować gałąź funkcjonalności z gałęzią główną, natomiast po merge’u
        usługa CI/CD w Gitlab automatycznie dostarcza działającą wersję
        aplikacji do środowiska testowego, a jeżeli aplikacja pomyślnie
        przejdzie testy to GitLab automatycznie dodaje zmiany do środowiska
        produkcyjnego.

        <ul class="visible">
          <li>Jedno repozytorium na GitLabie ma 1 pipeline, który jest stworzony na podstawie pliku gitlab-ci.yml</li>
          <li>Pipeline zawiera 1 lub więcej etapów (stage), który jest grupą zadań. Przykłady stage'ów: install, build, test, deploy </li>
          <li>Etapy są wykonywane po kolei. Każdy etap może mieć 1 lub więcej zadań (jobs).  Gdy wszystkie zadania zostaną wykonane, etap jest zakończony </li>
          <li>Zadania (jobs) to polecenia które wykonują zaplanowaną czynność</li>
          <li>GitLab Runner - proces który wykonuje kolejne zadania opisane w pliku YML</li>
        </ul>
        <ol>
          <li>Weryfikacja</li>
          <ul className="visible">
            <li>
              {" "}
              automatyczne buildowanie i testowanie programu (Continuous
              Integration)
            </li>
            <li> analiza jakości kodu źródłowego (GitLab Code Quality)</li>
            <li>
              {" "}
              określenie wpływu wprowadzonych zmian w kodzie (Browser
              Performance Testing)
            </li>
            <li>
              {" "}
              wykonanie serii testów (Containter Scanning, Dependency Scanning,
              JUnit)
            </li>
            <li>
              {" "}
              wdrożenie zmian za pomocą Review Apps, aby uzyskać podgląd zmian
              na każdej gałęzi
            </li>
          </ul>
          <li>Artefakty</li>
          <ul className="visible">
            <li>przechowywanie obrazów Docker (Container Registry)</li>
            <li> przechowywanie pakietów NPM (NPM Registry)</li>
            <li>przechowywanie artefaktów Maven (Maven Repository)</li>
          </ul>
          <li>Wydanie (release)</li>
          <ul className="visible">
            <li>
              automatyczne wdrożenie aplikacji na produkcję (Continuous
              Deployment)
            </li>
            <li>
              funkcja Continuous Delivery i manualne wdrożenie aplikacji na
              produkcję
            </li>
            <li>wdrażanie statycznych stron internetowych (GitLab Pages)</li>
            <li>
              udostępnianie nowych funkcji tylko w części podów i udostępnianie
              części użytkowników czasowo wdrożonych funkcji (Canary
              Deployments)
            </li>
            <li>wdrożenie funkcji z Feature Flags</li>
            <li>
              możliwość dodania opisów wydań (release notes) do każdego
              repozytorium Git (GitLab Releases)
            </li>
            <li>
              podgląd aktualnego stanu każdego środowiska CI działającego w
              klastrze Kubernetes (Deploy Boards)
            </li>
            <li>
              wdrożenie aplikacji na produkcję w klastrze Kubernetes (Auto
              Deploy)
            </li>
          </ul>
        </ol>
        Aby dodać ci/cd pipeline do gitlaba należy stworzyć plik .gitlab-ci.yml{" "}
        <ul className="visible">
          <li>odpowiednie odstępy są wymagane</li>

          <li>
            {" "}
            jeśli nie ustawimy stage'ów, wszystkie joby będą wykonywane
            równocześnie w domyślnym stage'u "test"
          </li>

          <li>
            {" "}
            po utworzeniu buildu na gitlabie container dockera jest niszczony,
            dlatego aby mieć dostęp do buildu musimy go zapisać jako artefakt
          </li>
        </ul>
      </div>
      <h3>Netlify</h3>
      <div className="text-section">
        <a href="https://www.youtube.com/watch?v=iJ63nXg-LvQ">
          {" "}
          https://www.youtube.com/watch?v=iJ63nXg-LvQ
        </a>
        <ol>
          <li>Stworzenie nowego repozytorium na Gitlabie</li>
          <li>Stworzenie nowego repozytorium w IDE i push na Gitlab</li>
          <li>Nowa strona na Netlify i podłączenie jej do Gitlaba</li>
          <li>
            Umieszczenie tokena NETLIFY_AUTH_TOKEN na Gitlab > settings > ci/cd
            > variables
          </li>
          <li>
            Umieszczenie tokena NETLIFY_SITE_ID (netlify > settings > api id) na
            Gitlab > settings > ci/cd > variables
          </li>
          <li>Utworzenie .gitlab-ci.yml</li>
        </ol>
      </div>
      <h3>Firebase</h3>
      <div className="text-section">
        <a href="https://medium.com/@luazhizhan/how-to-deploy-multiple-sites-to-firebase-hosting-part-1-a56949876c64">
          {" "}
          https://medium.com/@luazhizhan/how-to-deploy-multiple-sites-to-firebase-hosting-part-1-a56949876c64
        </a>
        <ol>
          <li>Stworzenie nowego repozytorium na gitlabie</li>
          <li>Stworzenie nowego repozytorium w IDE i push na gitlab</li>
          <li>npm i firebase-tools -g</li>
          <li>npx firebase login:ci</li>
          <li>Logowanie na google, skopiowanie tokena</li>
          <li>npx firebase init hosting</li>
          <li>
            Umieszczenie tokena na Gitlab > settings > ci/cd > variables >
            "FIREBASE_TOKEN"
          </li>
          <li>
            Konfiguracja .gitlab-ci.yml, .firebaserc i firebase.json. Uwzględnia
            mozliwosc uploadowania na 2 strony: staging i production
          </li>
        </ol>
      </div>
    </>
  );
}

export default Intro;
