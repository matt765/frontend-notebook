import SyntaxHighlighter from "react-syntax-highlighter";
import { a11yDark } from "react-syntax-highlighter/dist/esm/styles/hljs";

function Functions() {
  return (
    <>
      <h3>Zmienne & Deklaracje</h3>
      <ul>
        <li>Używanie === zamiast ==</li>
        <li>Deklarowanie zmiennych na górze kodu</li>
        <li>
          Używanie nazw zmiennych opisujących ich intencję. Dłuższe nazwy są
          dopuszczalne
        </li>
        <SyntaxHighlighter language="javascript" style={a11yDark}>{`// DON'T
let d
let elapsed
const ages = arr.map((i) => i.age)

// DO
let daysSinceModification
const agesOfUsers = users.map((user) => user.age)         
        `}</SyntaxHighlighter>
        <li>Używanie łatwych do wymówienia nazw zmiennych. </li>
        <SyntaxHighlighter language="javascript" style={a11yDark}>{`// DON'T
let fName, lName
let cntr

let full = false
if (cart.size > 100) {
  full = true
}

// DO
let firstName, lastName
let counter

const MAX_CART_SIZE = 100
// ...
const isFull = cart.size > MAX_CART_SIZE                        `}</SyntaxHighlighter>
        <li></li>
        <li>Jak najmniej zmiennych globalnych</li>
        <li>Nazwy zmiennych nie powinny dodać niepotrzebnego kontekstu</li>
        <SyntaxHighlighter language="javascript" style={a11yDark}>{`// DON'T
const product = {
  productId: 1,
  productName: "T-Shirt",
  productPrice: 8.99,
  productUnits: 12
};

product.productName;

// DO
const product = {
  productId: 1,
  productName: "T-Shirt",
  productPrice: 8.99,
  productUnits: 12
};

product.productName;        `}</SyntaxHighlighter>
        <li>
          Używanie domyślnych wartości argumentów zamiast warunkowej deklaracji
        </li>
        <SyntaxHighlighter language="javascript" style={a11yDark}>{`// DON'T
function createShape(type) {
  const shapeType = type || "circle";
  // ...
}

// DO 
function createShape(type = "circle") {
  // ...
}    `}</SyntaxHighlighter>
      </ul>
      <h3>Funkcje</h3>
      <ul>
        <li>
          {" "}
          Funkcje powinny wykonywać jedną czynność, na jednym poziomie
          abstrakcji. Średnia wielkosc to 30 linijek
        </li>
        <li>
          Pisanie kodu możliwie w linii, zamiast zagnieżdżania. Np. dlatego
          async/await > promise > callbacki
        </li>
        <li>
          Pętle i funkcje powinny jak najwcześniej coś zwracać, warto unikać
          głębokiego zagnieżdżania warunków if-else
        </li>
        <SyntaxHighlighter language="javascript" style={a11yDark}>{`// DON'T
function isPercentage(val) {
  if (val >= 0) {
    if (val < 100) {
      return true;
    } else {
      return false;
    }
  } else {
    return false;
  }
}

// DO
function isPercentage(val) {
  if (val < 0) {
    return false;
  }
  if (val > 100) {
    return false;
  }return true;
}
        `}</SyntaxHighlighter>
        <li>
          Nazwa funkcji powinna zawierać czasownik i opisywać jej działanie
        </li>
        <SyntaxHighlighter language="javascript" style={a11yDark}>{`// DON'T
/**
  * Invite a new user with its email address
  * @param {String} user email address
  */
function inv (user) { /* implementation */ }

// DO
function inviteUser (emailAddress) { /* implementation */ }   
`}</SyntaxHighlighter>
        <li>Unikanie negatywnych warunków "!"</li>
        <li>
          Zamiast długiej listy argumentów (max 3) warto użyć destrukturyzacji
        </li>
        <SyntaxHighlighter language="javascript" style={a11yDark}>{`// DON'T
function getRegisteredUsers (fields, include, fromDate, toDate) { /* implementation */ }
getRegisteredUsers(['firstName', 'lastName', 'email'], ['invitedUsers'], '2016-09-26', '2016-12-13')

// DO
function getRegisteredUsers ({ fields, include, fromDate, toDate }) { /* implementation */ }
getRegisteredUsers({
  fields: ['firstName', 'lastName', 'email'],
  include: ['invitedUsers'],
  fromDate: '2016-09-26',
  toDate: '2016-12-13'
})       `}</SyntaxHighlighter>
        <li>
          Używanie czystych funkcji w każdym możliwym przypadku. Są proste do
          używania i testowania
        </li>
        <SyntaxHighlighter language="javascript" style={a11yDark}>{`// DON'T
function addItemToCart (cart, item, quantity = 1) {
  const alreadyInCart = cart.get(item.id) || 0
  cart.set(item.id, alreadyInCart + quantity)
  return cart
}

// DO
// not modifying the original cart
function addItemToCart (cart, item, quantity = 1) {
  const cartCopy = new Map(cart)
  const alreadyInCart = cartCopy.get(item.id) || 0
  cartCopy.set(item.id, alreadyInCart + quantity)
  return cartCopy
}

// or by invert the method location
// you can expect that the original object will be mutated
// addItemToCart(cart, item, quantity) -> cart.addItem(item, quantity)
const cart = new Map()
Object.assign(cart, {
  addItem (item, quantity = 1) {
    const alreadyInCart = this.get(item.id) || 0
    this.set(item.id, alreadyInCart + quantity)
    return this
  }
})`}</SyntaxHighlighter>
        <li>
          Organizowanie funkcji zgodnie ze 'stepdown rule'. Funkcje wyższego
          poziomu na górze, niższego na dole.
        </li>
        <SyntaxHighlighter language="javascript" style={a11yDark}>{`// DON'T
// "I need the full name for something..."
function getFullName (user) {
  return \`\${user.firstName} \${user.lastName}\`
}

function renderEmailTemplate (user) {
  // "oh, here"
  const fullName = getFullName(user)
  return \`Dear \${fullName}, ...\`
}

// DO
function renderEmailTemplate (user) {
  // "I need the full name of the user"
  const fullName = getFullName(user)
  return \`Dear \${fullName}, ...\`
}

// "I use this for the email template rendering"
function getFullName (user) {
  return \`\${user.firstName} \${user.lastName}\`
}`}</SyntaxHighlighter>
      </ul>
    </>
  );
}

export default Functions;
