import { RightOutlined } from "@ant-design/icons";

export const Navigation = () => {
  return (
    <>
       <div className="navigation">
        <a href="#start" className="navigation-box">
          <div className="navigation-title">Start</div>
        </a>
        <div className="navigation-box">
          <a href="#1" className="navigation-title">Zmienne & Deklaracje</a>
          <div className="navigation-icon">
            <RightOutlined />
          </div>
        </div>
        <div className="navigation-box">
          <a href="#2" className="navigation-title">Funkcje</a>
          <div className="navigation-icon">
            <RightOutlined />
          </div>
        </div>
        <div className="navigation-box">
          <a href="#3" className="navigation-title">Instrukcje warunkowe</a>
          <div className="navigation-icon">
            <RightOutlined />
          </div>
        </div>    
        
      
      </div>
    
    </>
  );
}

