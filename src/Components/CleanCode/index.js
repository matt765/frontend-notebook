
import Start from './Start/Start'
import Sections from './Sections/Sections'

function CleanCode() {
    return (
        <div className="App">
            <div className="main-container">

                <div className="text-container">
                    <div className="start-container" id="start">
                        <Start />
                    </div>
                    <div className="text-box" id="1">
                        <Sections />
                    </div>

                </div>
            </div>
        </div>
    );
}

export default CleanCode;