import JavascriptLogo from "../../Images/Homepage/javascript.png";
import TypescriptLogo from "../../Images/Homepage/typescript.png";
import GitLogo from "../../Images/Homepage/git.png";
import TestsLogo from "../../Images/Homepage/javascript.png";
import NpmLogo from "../../Images/Homepage/typescript.png";
import DockerLogo from "../../Images/Homepage/typescript.png";
import CleanCodeLogo from "../../Images/Homepage/javascript.png";
import CICDLogo from "../../Images/Homepage/typescript.png";
import { useEffect } from 'react'
import { Link } from "react-router-dom";



function Homepage() {
  useEffect(() => {

  }, [])
  return (
    <div className="homepage-container">
      <div className="homepage-title">Wybierz technologię</div>
      <div className="homepage-logos">
        <Link to="/javascript">
          <div className="homepage-tech homepage-javascript">
            <img src={JavascriptLogo} />
            <div className="homepage-tech-name">Javascript</div>
          </div>
        </Link>
        <Link to="/typescript">
          <div className="homepage-tech homepage-typescript">
            <img src={TypescriptLogo} />
            <div className="homepage-tech-name">TypeScript</div>
          </div> </Link>
        <Link to="/git">
          <div className="homepage-tech homepage-git">
            <img src={GitLogo} />
            <div className="homepage-tech-name">Git</div>
          </div> </Link>
        <Link to="/testy">
          <div className="homepage-tech homepage-tests">
            <img src={TestsLogo} />
            <div className="homepage-tech-name">Testy</div>
          </div> </Link>
        <Link to="/npm">
          <div className="homepage-tech homepage-npm">
            <img src={NpmLogo} />
            <div className="homepage-tech-name">Npm</div>
          </div> </Link>
        <Link to="/docker">
          <div className="homepage-tech homepage-docker">
            <img src={DockerLogo} />
            <div className="homepage-tech-name">Docker</div>
          </div> </Link>
        <Link to="/czysty-kod">
          <div className="homepage-tech homepage-cleancode">
            <img src={CleanCodeLogo} />
            <div className="homepage-tech-name">Czysty Kod</div>
          </div> </Link>
        <Link to="/cicd">
          <div className="homepage-tech homepage-cicd">
            <img src={CICDLogo} />
            <div className="homepage-tech-name">CI/CD</div>
          </div>
        </Link>

      </div>
    </div>
  );
}

export default Homepage;