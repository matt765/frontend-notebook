
import Start from './Start/Start'
import Examples from './Examples/Examples'
import Intro from './Intro/Intro'

function Testing() {
    return (
        <div className="App">
            <div className="main-container">

                <div className="text-container">
                    <div className="start-container" id="start">
                        <Start />
                    </div>
                    <div className="text-box">
                        <Examples />
                    </div>

                </div>
            </div>
        </div>
    );
}

export default Testing;