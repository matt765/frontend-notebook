import SyntaxHighlighter from "react-syntax-highlighter";
import { a11yDark } from "react-syntax-highlighter/dist/esm/styles/hljs";

function Intro() {
  return (
    <>
      <h3> Wprowadzenie</h3>
      <ul class="visible">
        <li>Instalacja</li>
        <SyntaxHighlighter language="javascript" style={a11yDark}>
            {`npm install --save-dev jest`}
          </SyntaxHighlighter>
        <li>Package.json</li>
        <SyntaxHighlighter language="javascript" style={a11yDark}>
            {`{
  "scripts": {
    "test": "jest"
  }
}
            `}
          </SyntaxHighlighter>
        <li>Package.json - stale włączone testowanie</li>
        <SyntaxHighlighter language="javascript" style={a11yDark}>
            {`"scripts": {
    "test": "jest", 
    "testwatch": "jest --watchAll"
},`}
          </SyntaxHighlighter>
        <li>
          Podstawowa składnia z użyciem grupowania w .describe() które ma własny
          scope{" "}
        </li>
        <SyntaxHighlighter language="javascript" style={a11yDark}>
            {`describe('My work', () => {
  test('works', () => {
    expect(2).toEqual(2)
  })
})
`}
          </SyntaxHighlighter>
        <li>Dodatkowe opcje</li>
        <SyntaxHighlighter language="javascript" style={a11yDark}>
            {`beforeEach(() => { ... })
afterEach(() => { ... })

beforeAll(() => { ... })
afterAll(() => { ... })

`}
          </SyntaxHighlighter>
        <li>Cheatsheet</li>
        <SyntaxHighlighter language="javascript" style={a11yDark}>
            {`//Basic
  .not
  .toBe(value)
  .toEqual(value)
  .toBeTruthy()

//Truthiness
  .toBeNull()
  .toBeUndefined()
  .toBeDefined()
  .toBeTruthy()
  .toBeFalsy()

//Numbers
  .toBeCloseTo(number, numDigits)
  .toBeGreaterThan(number)
  .toBeGreaterThanOrEqual(number)
  .toBeLessThan(number)
  .toBeLessThanOrEqual(number)

//Objects
expect(value)
  .toBeInstanceOf(Class)
  .toMatchObject(object)
  .toHaveProperty(keyPath, value)

//Errors
  .toThrow(error)
  .toThrowErrorMatchingSnapshot()

//Asynchronous - Promises
  test('works with promises', () => {
    return new Promise((resolve, reject) => {
      ···
    })
  })

  test('works with async/await', async () => {
    const hello = await foo()
    ···
  })
`}
          </SyntaxHighlighter>
      </ul>
    </>
  );
}

export default Intro;
