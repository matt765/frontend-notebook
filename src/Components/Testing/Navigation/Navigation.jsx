import { RightOutlined } from "@ant-design/icons";

export const Navigation = () => {
  return (
    <>
     <div className="navigation">
        <a href="#start" className="navigation-box">
          <div className="navigation-title">Start</div>
        </a>
        <div className="navigation-box">
          <a href="#1" className="navigation-title">Podstawowe testy</a>
          <div className="navigation-icon">
            <RightOutlined />
          </div>
        </div>
        <div className="navigation-box">
          <a href="#2" className="navigation-title">Testowanie kodu asynchronicznego</a>
          <div className="navigation-icon">
            <RightOutlined />
          </div>
        </div>
        <div className="navigation-box">
          <a href="#3" className="navigation-title">Testowanie błędów</a>
          <div className="navigation-icon">
            <RightOutlined />
          </div>
        </div>
       
      </div>
     
    </>
  );
}

export default Navigation;
