import SyntaxHighlighter from "react-syntax-highlighter";
import { a11yDark } from "react-syntax-highlighter/dist/esm/styles/hljs";

function Examples() {
  return (
    <>
      <h3 id="1"> Przykłady prostych testów</h3>
      <ul class="visible">
        <li>Deklaracja testowanych funkcji</li>
        <SyntaxHighlighter language="javascript" style={a11yDark}>
            {`const axios = require('axios')

const functions = {
    add: (num1, num2) => num1 + num2,
    isNull: () => null,
    checkValue: (x) => x,
    createContent: () => {
        const content = { firstName: 'Matt' };
        content['message'] = 'Hello';
        return content
    }
}

module.exports = functions;                        `}
          </SyntaxHighlighter>
        <li>.toBe()</li>
        <SyntaxHighlighter language="javascript" style={a11yDark}>
            {`test('Adds 2 + 2 to equal 4', () => {
    expect(functions.add(2, 2)).toBe(4);
})
`}
          </SyntaxHighlighter>
        <li>.not.toBe()</li>
        <SyntaxHighlighter language="javascript" style={a11yDark}>
            {`test('Adds 2 + 2 to NOT equal 5', () => {
    expect(functions.add(2, 2)).not.toBe(5);
})
`}
          </SyntaxHighlighter>
        <li>.toBeNull()</li>
        <SyntaxHighlighter language="javascript" style={a11yDark}>
            {`test('Should be null', () => {
    expect(functions.isNull()).toBeNull();
})
`}
          </SyntaxHighlighter>
        <li>.toBeFalsy()</li>
        <SyntaxHighlighter language="javascript" style={a11yDark}>
            {`test('Should be falsy', () => {
    expect(functions.checkValue(NaN)).toBeFalsy();
})
`}
          </SyntaxHighlighter>
        <li>.toEqual()</li>
        <SyntaxHighlighter language="javascript" style={a11yDark}>
            {`test('Content should be Hello Matt object', () => {
    expect(functions.createContent()).toEqual({
        firstName: 'Matt',
        message: 'Hello'
    });
})
`}
          </SyntaxHighlighter>

        <li>.toMatch() - test obecności znaku w tablicy</li>
        <SyntaxHighlighter language="javascript" style={a11yDark}>
            {`test('There is no I in team', () => {
    expect('team').not.toMatch(/I/);
})
`}
          </SyntaxHighlighter>
        <li>.toBeLessThan() - test wielkości danej liczby</li>
        <SyntaxHighlighter language="javascript" style={a11yDark}>
            {`test('two plus two', () => {
  const value = 2 + 2;
  expect(value).toBeGreaterThan(3);
  expect(value).toBeGreaterThanOrEqual(3.5);
  expect(value).toBeLessThan(5);
  expect(value).toBeLessThanOrEqual(4.5);

  // toBe and toEqual are equivalent for numbers
  expect(value).toBe(4);
  expect(value).toEqual(4);
});
`}
          </SyntaxHighlighter>
        <li>.toContain() - test obecności wyrazu w tablicy</li>
        <SyntaxHighlighter language="javascript" style={a11yDark}>
            {`const shoppingList = [
  'diapers',
  'kleenex',
  'trash bags',
  'paper towels',
  'milk',
];

test('the shopping list has milk on it', () => {
  expect(shoppingList).toContain('milk');
  expect(new Set(shoppingList)).toContain('milk');
});
`}
          </SyntaxHighlighter>
      </ul>
      <h3 id="2">Testowanie kodu asynchronicznego</h3>
      <ul className="visible">
        <li>Przykładowe funkcje</li>
        <SyntaxHighlighter language="javascript" style={a11yDark}>
            {`const axios = require('axios')

const functions = {
   fetchUser: () => 
        axios
            .get('https://jsonplaceholder.typicode.com/users/1')
            .then(res =>  res.data)
            .catch(err => 'error')          
}

module.exports = functions;                        `}
          </SyntaxHighlighter>
        <li>Testowanie wartości z obietnicy</li>
        <SyntaxHighlighter language="javascript" style={a11yDark}>
            {`test('User fetched name should be Leanne Graham', () => {
    expect.assertions(1);
    return functions.fetchUser()
        .then(data => {
            expect(data.name).toEqual('Leanne Graham');
        })
})
`}
          </SyntaxHighlighter>
        <li>Składnia Async/Await</li>
        <SyntaxHighlighter language="javascript" style={a11yDark}>
            {`test('User fetched name should be Leanne Graham', async () => {
    expect.assertions(1);
    const data = await functions.fetchUser()
    expect(data.name).toEqual('Leanne Graham');
})
`}
          </SyntaxHighlighter>
        <li>Resolves/Rejects + Async/Await</li>
        <SyntaxHighlighter language="javascript" style={a11yDark}>
            {`test('the data is peanut butter', async () => {
  await expect(fetchData()).resolves.toBe('peanut butter');
});

test('the fetch fails with an error', async () => {
  await expect(fetchData()).rejects.toMatch('error');
});`}
          </SyntaxHighlighter>
        <li>Callback</li>
        <SyntaxHighlighter language="javascript" style={a11yDark}>
            {`test('the data is peanut butter', done => {
  function callback(data) {
    try {
      expect(data).toBe('peanut butter');
      done();
    } catch (error) {
      done(error);
    }
  }

  fetchData(callback);
});
`}
          </SyntaxHighlighter>
      </ul>
      <h3 id="3"> Testowanie błędów</h3>
      <ul className="visible">
        <li>.toThrow()</li>
        <SyntaxHighlighter language="javascript" style={a11yDark}>
            {`function compileAndroidCode() {
  throw new Error('you are using the wrong JDK');
}

test('compiling android goes as expected', () => {
  expect(() => compileAndroidCode()).toThrow();
  expect(() => compileAndroidCode()).toThrow(Error);

  // You can also use the exact error message or a regexp
  expect(() => compileAndroidCode()).toThrow('you are using the wrong JDK');
  expect(() => compileAndroidCode()).toThrow(/JDK/);
});
`}
          </SyntaxHighlighter>
      </ul>
    </>
  );
}

export default Examples;
