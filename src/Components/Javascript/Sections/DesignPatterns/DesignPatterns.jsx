import SyntaxHighlighter from "react-syntax-highlighter";
import { a11yDark } from "react-syntax-highlighter/dist/esm/styles/hljs";

function DesignPatterns() {
  return (
    <>
      <h2>Wzorce projektowe </h2>
      <ul className="visible">
        <li>
          {" "}
          Wzorce projektowe (design patterns) to zorientowane obiektowo gotowe
          rozwiązania popularnych problemów w programowaniu
        </li>
        <li>
          {" "}
          todo: Facade, State, Decorator, Publisher, Mediator, Constructor,
          Prototype, Command, Adapter
        </li>
      </ul>

      <h3> Singleton</h3>
      <ul>
        <li>
          Singleton jest wzorcem, który pozwala na stworzenie tylko jednej
          instancji obiektu z klasy bądź konstruktora funkcyjnego. Jest jednym z
          mniej popularnych i mniej stosowanych wzorców projektów{" "}
        </li>
        <li>
          W przypadku wielokrotnego wywoływania tej samej klasy zawsze będziemy
          otrzymywali tą samą instancję, która została stworzona podczas
          pierwszego wywołania.{" "}
        </li>
        <li>
          Zastosowania:
          <ul>
            <li>
              Obiekty konfiguracyjne - nie chcemy za każdym razem generować
              nowego obiektu, możemy zwracać to co już wygenerowane
            </li>
            <li>
              Połączenie z bazą danych - zazwyczaj chcemy ustanowić jedno
              połączenie z bazą danych, a nie inicjować wiele połączeń przy
              kazdym zapisie
            </li>
            <li>Logger danych</li>
          </ul>
        </li>
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`/// Implementacja z użyciem klasy
class AppConfig { 
  constructor(number = 5){ 
      if(AppConfig.exists){ 
        return AppConfig.instance 
      } 
      this.randomNumber = Math.random();
      this.number = number;
      AppConfig.exists = true; 
      AppConfig.instance = this; 
      return this 
  }  
} 

const configObject = new AppConfig(8);
const configObject2 = new AppConfig(1);

/// Obiekty zwrócone na koniec są identyczne
console.log(configObject);
console.log(configObject2);
console.log(configObject === configObject2); // true                      `}</SyntaxHighlighter>
      </ul>

      <h3>Strategia</h3>
      <ul>
        <li>
          Wzorzec strategii definiuje rodzinę algorytmów, z których każdy
          zdefiniowany jest w osobnej klasie implementującej wspólny interfejs -
          dzięki temu możliwe jest wymienne stosowanie każdego z tych
          algorytmów, nie będąc zależnymi od klas klienckich.
        </li>
        <li>
          Wzorzec strategii definiuje alternatywne algorytmy (lub strategie)
          będące rozwiązaniem jakiegoś zadania. Pozwala na wymianę metod i
          przetestowanie ich niezaleznie od klienta. Strategia to grupa
          algorytmów które są wymienne
        </li>
        <li>
          Kontekst dostarcza interfejs różnym Strategiom umożliwiając wywołanie
          algorytmów do kalkulacji
        </li>
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`var Shipping = function () {
  this.company = "";
};

Shipping.prototype = {
    setStrategy: function (company) {
        this.company = company;
    },

    calculate: function (package) {
        return this.company.calculate(package);
    }
};

var UPS = function () {
    this.calculate = function (package) {
        // calculations...
        return "$45.95";
    }
};

var USPS = function () {
    this.calculate = function (package) {
        // calculations...
        return "$39.40";
    }
};

var Fedex = function () {
    this.calculate = function (package) {
        // calculations...
        return "$43.20";
    }
};

function run() {

    var package = { from: "76712", to: "10012", weigth: "lkg" };

    // the 3 strategies

    var ups = new UPS();
    var usps = new USPS();
    var fedex = new Fedex();

    var shipping = new Shipping();

    shipping.setStrategy(ups);
    console.log("UPS Strategy: " + shipping.calculate(package));
    shipping.setStrategy(usps);
    console.log("USPS Strategy: " + shipping.calculate(package));
    shipping.setStrategy(fedex);
    console.log("Fedex Strategy: " + shipping.calculate(package));
}    `}</SyntaxHighlighter>
      </ul>

      <h3> Observer</h3>
      <ul>
        <li>
          Observer design pattern jest behawioralnym wzorcem projektowym w
          którym jeden obiekt nazywany Subject lub Observable informuje
          wszystkie inne obserwujące go obiekty o zmianach w jego wewnętrznym
          stanie. Inne obiekty wtedy zareagują na tę zmianę i obsłużą ją zgodnie
          z wymaganiami. Informowanie najczęściej jest realizowane przez
          wywołanie jednej z metod obserwatora.{" "}
        </li>
        <li>
          The Observer pattern offers a subscription model in which objects
          subscribe to an event and get notified when the event occurs. This
          pattern is the cornerstone of event driven programming, including
          JavaScript.
        </li>
        <li>
          Budując aplikacje używamy wielu event handlerów, czyli funkcji które
          zostaną poinformowane gdy zostanie wywołany jakiś event. Informacje
          mogą zawierać parametry
        </li>
        <li>
          Ta relacja eventów i event-handlerów jest manifestacją wzorca
          Obserwatora w Javascript.
        </li>
        <li>Wzorzec Obserwatora składa się z następujących obiektów</li>
        <ul>
          <li>
            Subject - zawiera listę dowolnej liczby subskrybentów,implementuje
            interfejs który pozwala im włączać i wyłączac subskrypcję, wysyła
            powiadomienia w momencie zmiany stanu{" "}
          </li>
          <li>
            Obserwatorzy - posiada funkcję która może być wywołana gdy zmieni
            się stan Subjecta (np. zdarzy się event)
          </li>
        </ul>

        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`function Click() {
    this.handlers = [];  // observers
}

Click.prototype = {

    subscribe: function (fn) {
        this.handlers.push(fn);
    },

    unsubscribe: function (fn) {
        this.handlers = this.handlers.filter(
            function (item) {
                if (item !== fn) {
                    return item;
                }
            }
        );
    },

    fire: function (o, thisObj) {
        var scope = thisObj || window;
        this.handlers.forEach(function (item) {
            item.call(scope, o);
        });
    }
}

function run() {

    var clickHandler = function (item) {
        console.log("fired: " + item);
    };

    var click = new Click();

    click.subscribe(clickHandler);
    click.fire('event #1');
    click.unsubscribe(clickHandler);
    click.fire('event #2');
    click.subscribe(clickHandler);
    click.fire('event #3');
}   `}</SyntaxHighlighter>
      </ul>

      <h3> Module</h3>
      <ul>
        <li>-</li>
      </ul>

      <h3>Revealing Module</h3>
      <ul>
        <li>
          Wzorzec projektowy który umożliwia podzielenie kody na moduły i
          zapewnia lepszą strukturę kodu. Daje możliwosć tworzenia publicznych i
          prywatnych zmiennych i unikania globalnego scope'u.
        </li>
        <li>
          {" "}
          Wzorzec ten używa IIFE, wewnątrz których umieszczane są
          funkcje/zmienne które można wywołać tylko odwołując się do nazwy
          rodzica
        </li>
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{` var namesCollection = (function() {
  // private members
  var objects = [];
  
  // Public Method
  function addObject(object) {
      objects.push(object);
      printMessage(object);
  }

  // Private Method
  function printMessage(object) {
      console.log("Object successfully added:", object);
  }    // public members, exposed with return statement
  return {
      addName: addObject,
  };
})();

namesCollection.addObject()
        
        `}</SyntaxHighlighter>
      </ul>

      <h3> Factory</h3>
      <ul>
        <li>
          To funkcje, które tworzą i zwracają obiekty. Głównym celem stosowania
          tego wzorca jest „produkowanie” obiektów związanych z jednym wspólnym
          interfejsem
        </li>
        <li>
          Konstruktory i klasy wymagają użycia słowa "new", a wzorzec fabryki
          zwraca bezpośrednio instację danego obiektu.
        </li>
        <li>
          Używając „Fabryki” w kodzie nie interesuje nas za bardzo, w jaki
          sposób ten obiekt zostanie stworzony. My jedynie podajemy „parametry”
          i oczekujemy na dostarczenie prawidłowo „wyprodukowanego” obiektu.
        </li>
        <li>
          Jeśli obiektów jest bardzo dużo (10 000+), warto użyć klas które są
          bardziej wydajne
        </li>
        <li>
          Pamiętajmy, że Simple Factory, jest jedynie wzorcem, a nie nowym
          sposobem na tworzenie obiektów. Fabryka jedynie decyduje jaki obiekt i
          w jaki sposób stworzyć.{" "}
        </li>
        <li>
          Dzięki temu będziemy tworzyć obiekty zawsze za pomocą tego samego
          interfejsu a ewentualne zmiany, poprawki, dodatkowe funkcjonalności
          będziemy już wykonywać w samej fabryce. Dzięki temu nasz kod będzie
          łatwiejszy w utrzymaniu i łatwo skalowalny.{" "}
        </li>

        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`var Factory = function () {
  this.createEmployee = function (type) {
      var employee;

      if (type === "fulltime") {
          employee = new FullTime();
      } else if (type === "parttime") {
          employee = new PartTime();
      } else if (type === "temporary") {
          employee = new Temporary();
      } else if (type === "contractor") {
          employee = new Contractor();
      }

      employee.type = type;

      employee.say = function () {
          console.log(this.type + ": rate " + this.hourly + "/hour");
      }

      return employee;
  }
}

var FullTime = function () {
  this.hourly = "$12";
};

var PartTime = function () {
  this.hourly = "$11";
};

var Temporary = function () {
  this.hourly = "$10";
};

var Contractor = function () {
  this.hourly = "$15";
};

function run() {

  var employees = [];
  var factory = new Factory();

  employees.push(factory.createEmployee("fulltime"));
  employees.push(factory.createEmployee("parttime"));
  employees.push(factory.createEmployee("temporary"));
  employees.push(factory.createEmployee("contractor"));

  for (var i = 0, len = employees.length; i < len; i++) {
      employees[i].say();
  }
}
`}</SyntaxHighlighter>
      </ul>
    </>
  );
}

export default DesignPatterns;
