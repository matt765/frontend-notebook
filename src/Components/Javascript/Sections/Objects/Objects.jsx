import SyntaxHighlighter from "react-syntax-highlighter";
import { a11yDark } from "react-syntax-highlighter/dist/esm/styles/hljs";

function Objects() {
  return (
    <>
      <h2>Programowanie obiektowe</h2>
      <h3>Wprowadzenie</h3>
      <ul>
        <li>
          Programowanie Obiektowe (ang. object-oriented programming – OOP) to
          paradygmat programowania, w którym struktura aplikacji oparta jest na
          obiektach.
        </li>
        <li>
          Obiekt to zbiór par klucz (key) - wartość (value). Wartością
          właściwości może być funkcja, nazywana w tym przypadku metodą.
          Istnieją obiekty predefiniowane
        </li>
        <li>W przypadku nazw w obiektach wielkość liter ma znaczenie </li>
        <li>Właściwości z samym kluczem zwracają undefined</li>
        <li>
          Obiekty możemy podzielić na pojedyncze instancje (pojedyncze
          egzemplarze), oraz grupy obiektów o podobnych właściwościach (np.
          tablice, linki, buttony itp).{" "}
        </li>
        <li>Obiekty mogą być rozbudowane, zawierając inne obiekty</li>
        <li>
          W przeglądarce występuje Global Object("window "), do którego prowadzi
          this jeśli nie jest przypisane do innego obiektu
        </li>

        <li>
          Właściwość __proto__ jest aktualnie rzadko używana, w nowoczesnym
          Javascript należy używać funkcji
          Object.getPrototypeOf/Object.setPrototypeOf
        </li>
        <li>
          Wartości mogą mieć dowolny typ, klucze muszą być stringami lub
          symbolami
        </li>
        <li>
          Obiekty są przypisywane do zmiennych/kopiowane z użyciem referencji.
          Czyli zmienna nie zawiera obiektu tylko referencję do miejsca w
          pamięci gdzie obiekt się znajduje, czyli kopiowanie zmiennej kopiuje
          referencję a nie sam obiekt.
        </li>
      </ul>

      <h3>4 sposoby na utworzenie wzoru obiektu</h3>

      <div class="text-section">
        Inicjator obiektu (object initializer)
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`/// Prototyp: Object.prototype - obiekt do którego dostęp ma każdy obiekt w JS
const cat = {
name: "Bill",
speed: 1000,
showText() {
return "Lubię walczyć ze złem";
}
//alternatywna deklaracja funkcji
showText: function() {
return "Lubię walczyć ze złem";
}
}
const mycat = new cat;       
`}</SyntaxHighlighter>
        Konstruktor + new
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`function Enemy(speed, power) {
this.live = 3;
this.speed = speed;
this.power = power;

this.print = function() {
console.log(\`
  Przeciwnik ma:
  życia: \${this.live}
  szybkości: \${this.speed}
  siły ataku: \${this.power}
\`);
}
} `}</SyntaxHighlighter>
        Klasa + new
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`class Animal {
constructor(name, age) {
this.name = name;
this.age = age;
}

method1() { }
method2() { }
method3() { }
}

const animal = new Animal("pies", 8);  
`}</SyntaxHighlighter>
        Object.create()
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`/// Prototyp: obiekt podany w parametrze
const name4 = Object.create(John) `}</SyntaxHighlighter>
      </div>

      <h3>Get/Set</h3>

      <div class="text-section">
        W obiektach możemy zdefiniować gettery i settery poprzedzając klucz
        słowem get lub set
        <SyntaxHighlighter language="javascript" style={a11yDark}>{`var o = {
a: 7,
get b() {
return this.a + 1;
},
set c(x) {
this.a = x / 2;
}
};

console.log(o.a); // 7
console.log(o.b); // 8 -- At this point the get b() method is initiated.
o.c = 50;         //   -- At this point the set c(x) method is initiated
console.log(o.a); // 25`}</SyntaxHighlighter>
      </div>
      <h3>Właściwości obiektów</h3>

      <div class="text-section">
        Odwołać się do właściwości i metod można na 3 sposoby
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`obiekt.klucz 
lub 
obiekt["klucz"]
lub 
destrukturyzacja`}</SyntaxHighlighter>
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`// dostęp do wybranej właściwości
cat.speed

// dostęp do metody
car1.displayCar();

// zmiana wartości
myCar.make = 'Ford';

// notacja nawiasowa
myCar['make'] = 'Ford';

// przykład
var myObj = new Object(),
str = 'myString',
rand = Math.random(),
obj = new Object();

myObj.type              = 'Dot syntax';
myObj['date created']   = 'String with space';
myObj[str]              = 'String value';
myObj[rand]             = 'Random Number';
myObj[obj]              = 'Object';
myObj['']               = 'Even an empty string';

console.log(myObj);

// usunięcie właściwości z obiektu
delete ob.klucz

// sprawdzenie czy wybrany obiekt to instancja wybranego konstruktora
console.log(enemyS instanceof EnemyShoot); //true

// warunek przy tworzeniu obiektu
if (cond) var x = {greeting: 'hi there'};`}</SyntaxHighlighter>
        Jeżeli pod klucze podstawiamy wartości z jakiś zmiennych, których nazwy
        są takie same jak danych kluczy, możemy taki zapis skrócić:
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`//definiowanie obiektu razem z jego funkcją
const tab = [];
const name = "Szama";
const speed = 1000;

//zamiast
const ob = {
name: name,
speed: speed
}
tab.push(ob);

//mogę
tab.push({
name: name,
speed: speed
});

//lub jeszcze lepiej
tab.push({name, speed});`}</SyntaxHighlighter>
      </div>
      <h3> This</h3>

      <ul>
        <li>
          Aby odwołać się do danego obiektu z wnętrza którejś z jego metod
          należy użyc słowa kluczowego 'this', które wskazuje na aktualny
          kontekst
        </li>
        <li>
          This w <span class="important">normalnej funkcji</span> wskazuje na
          obiekt przed kropką
        </li>
        <li>
          This w <span class="important">funkcji strzałkowej</span> jest
          pobierane z normalnej funkcji z zewnątrz
        </li>
        <li>
          Jeśli funkcja nie jest metodą żadnego obiektu, to this wskazuje na
          obiekt globalny. Wyjątek to stric mode
        </li>
        <li>
          Czasem metoda w obiekcie może pochodzić z innego obiektu i wtedy this
          na niego wskazuje. Można sobie z tym poradzić tworząc zmienną np.
          "const that = this" lub używając metod typu bind()
        </li>
        <li>
          Kiedy jest wykonywana odziedziczona metoda, wartość this wskazuje na
          obiekt, który dziedziczy, nie na obiekt w którym ta metoda została
          zadeklarowana jako własna właściwość
        </li>
        <li>
          This wewnątrz funkcji setTimeout traci połączenie ze swoim obiektem,
          bo this tej funkcji jest ustawiony na global object lub
          undefined(strict mode). Można tu użyć funkcji .bind()
        </li>
      </ul>

      <h3>Kopiowanie obiektów</h3>

      <ul>
        <li>
          Jeśli używamy funkcji które wykonują na obiektach różne czynności to
          może dojść do nadpisania wartości. Dlatego warto kopiować obiekty
        </li>
        <li>
          Płaskie obiekty można kopiować używając spread syntax lub funkcji
          Object.assign (cel, ...źródła){" "}
        </li>

        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`const John = {
name: 'John',
skill: {level: 4}
}

/// Skopiowanie referencji
const name1 = John

/// Spread syntax
const name2 = {...John}

/// Object.assign(target, source) - zwraca zmodyfikowany obiekt zrodlowy
const name3 = Object.assign({}, John)

/// Object.create(source) - tworzy nowy obiekt, którego prototypem jest obiekt podany w parametrze
const name4 = Object.create(John)

/// JSON - głębokie klonowanie, bez funkcji
const name5 = JSON.parse(JSON.stringify(Jon))
`}</SyntaxHighlighter>
      </ul>

      <h3>Konstruktor</h3>

      <ul>
        <li>
          Konstruktor to zwykła funkcja (z wyjątkiem funkcji strzałkowej) która
          tworzy wzór obiektu. Jej nazwę piszemy wielką literą{" "}
        </li>
        <li>Używamy go tylko z użyciem operatora "new"</li>
        <li>
          Domyślnie wszystkie funkcje (w tym konstruktor) mają w prototypie
          właściwość constructor która prowadzi do niej samej. Dotyczy to też
          obiektów stworzonych pozniej za pomocą "new"{" "}
        </li>
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`function Enemy(speed, power) {
this.live = 3;
this.speed = speed;
this.power = power;

this.print = function() {
console.log(\`
    Przeciwnik ma:
    życia: \${this.live}
    szybkości: \${this.speed}
    siły ataku: \${this.power}
\`);
}
}

/// Teraz uzywamy konstruktora

function Enemy(speed, power) { ... }

const enemy1 = new Enemy(3, 10);
enemy1.print();

const enemy2 = new Enemy(5, 15);
enemy2.print();

// Podobnie do innych typów
const str = new String("Ala ma Konczenti");
const nr = new Number(102);
const arr = new Array(1, 2, 3);
const bool = new Boolean(true);


`}</SyntaxHighlighter>
      </ul>
      <h3>Prototyp i dziedziczenie</h3>
      <ul>
        <li>
          Każdy obiekt posiada prywatną własność łączącą go z innym obiektem
          zwanym jego prototypem. Obiekt prototype posiada swój własny prototyp,
          i tak dalej aż do Object.prototype, którego prototyp ma wartość null i
          jest zakończeniem łańcucha prototypów.
        </li>

        <li>
          Podczas wywołania metody obiektu najpierw sprawdzane jest, czy
          znajduje się ona bezpośrednio na obiekcie, a jeśli nie ma to szukana
          jest na prototypie obiektu, prototypie jego prototypu i tak dalej, aż
          do odnalezienia właściwości o pasującej nazwie bądź końca łańcucha
          prototypów. Obiekt otrzymuje metody i właściwości z prototypu. Proces
          ten to <span class="important">dziedziczenie prototypowe</span>
        </li>
        <li>
          W Javascript nie występuje klasyczny (classical) model dziedziczenia,
          w którym klasa to abstrakcja (poziom 2) obiektu(poziom 1), a obiekt to
          abstrakcja Real World Entity (poziom 0).{" "}
        </li>
        <li>
          W prototypowym modelu dziedziczenia występuje tylko jeden typ
          abstrakcji - obiekt. W Javascript obiekty są abstrakcjami Real World
          Entitites lub innych obiektów, które nazywane są wtedy prototypami.
        </li>
        <li>
          Klasa w modelu prototypowym sama w sobie jest i abstrakcją i obiektem
        </li>
        <li>
          Tablice dziedziczą z Array.prototype i Object.prototype: a ---
          Array.prototype --- Object.prototype --- null
        </li>
        <li>
          <span class="important">__proto__</span> to historyczny getter/setter
          dla właściwości Prototype. Aktualnie częściej używa się funkcji{" "}
          <span class="important">
            Object.getPrototypeOf/Object.setPrototypeOf
          </span>
        </li>
        <li>
          Prototyp zawiera wszystkie właściwości i metody, które chcemy aby
          zostały odziedziczone przez wszystkie nowo tworzone instancje
        </li>
        <li>
          Prototyp używany jest tylko do odczytywania właściwości, tzn.
          wszystkie operacje modyfikujące działają tylko na danym obiekcie a nie
          na jego prototypie
        </li>
        <li>
          Niezależnie od umiejscowienia metody/właściwości,{" "}
          <span class="important">This</span> zawsze wskazuje na obiekt przed
          kropką, a nie na prototyp z którego pochodzi.
        </li>
        <li>
          Pętla for...in iteruje też po odziedziczonych wartościach. Jeśli
          chcemy tego uniknąć, możemy użyc w kodzie funkcji
          obj.hasOwnProperty(key
        </li>
        <li>
          Zagłębianie się w kolejnej prototypy w celu wyszukania wywołanej
          metody (lub wlasciwosci) nazywane jest korzystaniem z
          <span class="important"> łańcucha prototypów</span>
        </li>
        <li>
          Wszystkie obiekty w Javascript mają dostęp do prototypu
          Object.prototype, który jest na samej górze każdego łańcucha
          prototypów.{" "}
        </li>
        <li>
          {" "}
          Typowa funkcja obiekt ma poza prototypem kilka właściwości, które
          zostały automatycznie dodane przez Javascript. Są to między innymi
          name (nazwa funkcji), arguments (przekazane wartości), caller
          (funkcja, która wywołała aktualną funkcję).
        </li>
        <li>Tablice dziedziczą z Object.prototype i Array.prototype</li>
        <img src="https://kursjs.pl/kurs/obiekty/proto-to-prototype.jpg" />

        <li>
          Prototyp jest obiektem, więc możemy go rozbudować tak samo jak to
          robiliśmy z innymi obiektami do tej pory. Jeżeli kiedykolwiek coś do
          niego dodamy stanie się to dostępne dla wszystkich instancji już
          stworzonych i tworzonych w przyszłości na bazie tego konstruktora.{" "}
        </li>
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`/// Funkcja tworząca pusty obiekt z podanym prototypem, plus property descriptor opcjonalnie
Object.create(proto, [descriptors])

/// Zwraca prototyp obiektu, działa jak __proto__ getter
Object.getPrototypeOf(obj) 

/// Ustawia prototyp obiektu, działa jak __proto__ setter
Object.setPrototypeOf(obj.proto)

/// Inne przykłady
function Enemy(speed, power) {
this.live = 3;
this.speed = speed;
this.power = power;
}

/// Dodajemy nowe metody do prototypu
Enemy.prototype.attack = function() {
console.log(\`Atakuje z siłą \${this.power} i szybkością \${this.speed}\`);
}

Enemy.prototype.fly = function() {
console.log(\`Lecę z szybkością \${this.speed}\`);
}

/// Tworzę nowe obiekty
const enemy1 = new Enemy(3, 10);
enemy1.attack(); //Atakuje z siłą 10
enemy1.fly(); //Lecę z szybkością 3

const enemy2 = new Enemy(5, 15);
enemy2.attack(); //Atakuje z siłą 15
enemy2.fly(); //Lecę z szybkością 5
`}</SyntaxHighlighter>
        <li>
          {" "}
          Zastosowanie prototypu ułatwia modyfikowanie funkcjonalności obiektów
          bo nie trzeba aktualizować wczesniej utworzonych instancji, poza tym
          oszczędza nam zasoby
        </li>
      </ul>
      <h3>Object.create()</h3>
      <div class="text-section">
        Funkcja która tworzy nowy obiekt. Jako pierwszy parametr przyjmuje ona
        obiekt, który stanie się prototypem nowo tworzonego obiektu, czyli
        kopiowane są wszystkie właściwości obiektu{" "}
        <SyntaxHighlighter language="javascript" style={a11yDark}>{`
const car = { 
drive() {
console.log(this.name + " jadę");
},
refuel() {
console.log(this.name + " tankuję");
},
stop() {
console.log(this.name + " zatrzymuję się");
}
}

const c1 = Object.create(car);
c1.name = "Samochód 1";
c1.drive(); //Samochód 1 jadę

const c2 = Object.create(car);
c2.name = "Samochód 2";
c2.drive(); //Samochód 2 jadę

console.log(c1);
console.log(c2);            
  `}</SyntaxHighlighter>
        Drugim opcjonalnym parametrem jest obiekt, w którym możemy ustawić
        właściwości nowo tworzonego obiektu
        <SyntaxHighlighter language="javascript" style={a11yDark}>{`
    const c1 = Object.create(car, {
      name: {
          value: 'Maluch'
      },
      km: {
          value: 0
      }
    }
    `}</SyntaxHighlighter>
      </div>
      <h3>Object.assign()</h3>
      <div class="text-section">
        Funkcja kopiująca wszystkie iterowalne właściwości z źródłowego obiektu
        do docelowegu obiektu. Zwraca zmodyfikowany docelowy obiekt
        <SyntaxHighlighter language="javascript" style={a11yDark}>{`
Object.assign(target, ...sources)

const target = { a: 1, b: 2 };
const source = { b: 4, c: 5 };

const returnedTarget = Object.assign(target, source);

console.log(target);
// expected output: Object { a: 1, b: 4, c: 5 }

console.log(returnedTarget);
// expected output: Object { a: 1, b: 4, c: 5 }
  `}</SyntaxHighlighter>
      </div>
      <h3>Klasy</h3>
      <ul>
        <li>
          W OOP klasa to kod który jest wzorem dla tworzenia obiektów,
          dostarczającym początkowe wartości zmiennych i implementującym metody{" "}
        </li>
        <li>
          Klasy to nowy sposób zapisu opartego o prototypowy model
          dziedziczenia. Nie zmienia starego mechanizmu, jest tylko
          przyjemniejszym zapisem tego samego
        </li>
        <li>
          Klasa w modelu prototypowym sama w sobie jest i abstrakcją i obiektem
        </li>
        <li>Klasa ES6 to w zasadzie funkcja</li>
        <li>
          Słowa kluczowe składni klas:
          <ul>
            <li>
              <span class="important">Class</span> - deklaracja klasy z daną
              nazwą
            </li>
            <li>
              <span class="important">Constructor</span> - specjalna metoda
              tworzenia i inicjowania obiektu utworzonego w klasie
            </li>
            <li>
              <span class="important">Static</span> - definiuje statyczną metodę
              lub właściwość klasy. Przydatne np. do stałych konfiguracji lub
              innych właściwości, które nie muszą być powielane w instancjach
            </li>
            <li>
              <span class="important">Extends</span> - używane do tworzenia
              klasy jako elementu potomnego innej klasy
            </li>
            <li>
              <span class="important">Super</span> - umożliwia korzystanie z
              funkcji klasy po której nasz obiekt dziedziczy.
            </li>
          </ul>
        </li>
        <li>
          Każda klasa ma funkcję constructor() która jest wykonywana przy
          tworzeniu nowej instancji za pomocą new
        </li>
        <li>Kod klasy jest automatycznie w strict mode</li>
        <li>
          Użycie "#" przed nazwą zmiennej lub metody w klasie sprawia że będzie
          ona prywatna, dostępna tylko dla kodu wewnątrz tej klasy. Możemy wtedy
          ustawić np. gettery i settery, a nie możemy modyfikować prywatnej
          treści kodem z zewnątrz
        </li>
        <li>
          Metody klasy są nie iterowalne, dzięki czemu możemy loopować np.
          for...in po właściwościach obiektu{" "}
        </li>
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`class MyClass {
constructor(name, age) {
this.name = name;
this.age = age;
}

method1() { }
method2() { }
method3() { }
}

const animal = new Animal("pies", 8);

/// Właściwości można też definiować poza konstruktorem
class Animal {
legs = 4;
type = "animal";
}

/// Klasę można też zdefiniować wewnątrz zmiennej
let User = class {
sayHi() {
alert("Hello");
}
};
`}</SyntaxHighlighter>
        <li>
          W JavaScript możemy też tworzyć metody statyczne, które są dostępne
          dla danej klasy. Metody takie nie są dostępne dla nowych instancji, a
          tylko dla samych klas
        </li>
        <li>
          Najczęściej wykorzystywane są do tworzenia użytecznych funkcji dla
          danej klasy. Można dzięki temu pogrupować funkcjonalności dotyczące
          danych klas w jednym miejscu
        </li>
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`class Human {
constructor(name) {
this.name = name;
}

say() {
console.log("Jestem człowiek");
}

static create() {
console.log("Tworzę");
}
}

const ob = new Human("Marcin");
ob.create(); //błąd
Human.create(); //"Tworzę"
`}</SyntaxHighlighter>
      </ul>

      <h3>Konstruktor - dziedziczenie</h3>
      <ul>
        <li>
          {" "}
          Jeśli chcielibyśmy aby obiekt otrzymał metody innego obiektu
          moglibyśmy użyć kodu 'Obiekt1.prototype = Obiekt2.prototype;' ale
          wtedy stają się tym samym obiektem i metoda dodana do jednego obiektu
          trafia do wszystkich obiektów korzystających z tego prototypu.{" "}
        </li>
        <li>
          Rozwiązaniem nie jest równanie prototypów, a stworzenie nowego obiektu
          na bazie innego prototypu.
        </li>
        <SyntaxHighlighter language="javascript" style={a11yDark}>{`  
EnemyShoot.prototype = Object.create(Enemy.prototype);
//lub
EnemyShoot.prototype = Object.assign({}, Enemy.prototype);

//lub
EnemyShoot.prototype = Object.create(...Enemy.prototype);
`}</SyntaxHighlighter>
        <li>
          Jeśli chcemy aby docelowy prototyp zawierał dziedziczone metody ale
          opierał się na konstruktorze, możemy go ustawić jako własną właściwość{" "}
        </li>
        <SyntaxHighlighter language="javascript" style={a11yDark}>{`  
function Enemy(name, x, y) {
this.name = name;
this.x = x;
this.y = y;
console.log("Tworzę przeciwnika: " + this.name);
}

Enemy.prototype.fly = function() {
return this.name + " latam";
}

//dziedziczymy prototyp

function EnemyShoot(name, x, y) {
this.name = name;
this.x = x;
this.y = y;
this.type = "shooter";
}

EnemyShoot.prototype = Object.create(Enemy.prototype);
EnemyShoot.prototype.constructor = EnemyShoot;

EnemyShoot.prototype.shoot = function() {
return this.name + " strzelam";
}


const enemyN = new Enemy("Normal");
console.log(enemyN.fly()); //Normalny latam
console.log(enemyN.shoot()); //błąd - nie ma takiej metody

const enemyS = new EnemyShoot("Shooter");
console.log(enemyS.fly()); //Shooter latam
console.log(enemyS.shoot()); //Shooter strzelam
`}</SyntaxHighlighter>
      </ul>
      <h3>Metoda call() i apply()</h3>
      <ul>
        <li>
          Pozwala obiektowi zapożyczyć metodę z innego obiektu. Jako pierwszy
          parametr podajemy obiekt którego ma dotyczyć this. Kolejne parametry
          to wskazane przez nas parametry.
        </li>
        <li>
          Jesli wiemy że funkcja nie obsługuje this to pierwszy parametr nie
          jest obslugiwany, mozemy wpisac null albo undefined.
        </li>
        <SyntaxHighlighter language="javascript" style={a11yDark}>{`
const ob = {
  name : "x-wing",
  print(shotCount, speed) {
      console.log(\`\${this.name} strzela \${shotCount} razy z szybkością\${speed}\`);
  }
}

const tie = {
  name : "Tie fighter"
}

ob.print.call(tie, 5, 200); //Tie fighter strzela 5 razy z szybkością 200  
`}</SyntaxHighlighter>
        <li>
          Metodą apply() jest podobna ale przyjmuje 1 atrybut - tablicę z
          parametrami. Po wywołaniu funkcji za pomocą apply() skłądowej tablicy
          są przekazywane jako kolejne parametry. Rzadko używana bo mając
          tablicę mozemy zamienic ją na pojeyncze atrybuty za pomocą spread
          syntax.
        </li>
        <SyntaxHighlighter language="javascript" style={a11yDark}>{`
const ob = {
  name : "nikt",
  print(pet1, pet2) {
      console.log(\`Nazywam się \${this.name} i mam 2 zwierzaki: \${pet1} i \${pet2}\`);
  }
}

const user = {
  name : "Marcin"
}

ob.print.apply(user, ["pies", "kot"]); //Nazywam się Marcin i mam dwa zwierzaki: pies i kot  
`}</SyntaxHighlighter>
        <li>Używając metody call możemy nadpisać metodę wybranego prototypu</li>
        <SyntaxHighlighter language="javascript" style={a11yDark}>{` 
function EnemyShoot(name, x, y) {
  Enemy.call(this, name, x, y);
  this.type = "shooter";
}

EnemyShoot.prototype = Object.create(Enemy.prototype);
EnemyShoot.prototype.constructor = EnemyShoot;

EnemyShoot.prototype.fly = function() {
  const text = Enemy.prototype.fly.call(this); //tamta funkcja nie ma parametrów
  return text + " i czasami strzelam!!!";
}

const enemyN = new Enemy("Normal");
enemyN.fly(); //Normal latam

const enemyS = new EnemyShoot("Shooter");
console.log(enemyS.fly()); //Shooter latam i czasami strzelam!!!
`}</SyntaxHighlighter>
      </ul>

      <h3>Klasy - dziedziczenie</h3>
      <ul>
        <li>
          Aby rozszerzyć jakąś klasę, a tym samym dziedziczyć po niej jej
          funkcjonalności, skorzystamy z instrukcji extends
        </li>
        <li>Instrukcja super() służy do wywołania kodu rozszerzanej metody</li>

        <SyntaxHighlighter language="javascript" style={a11yDark}>{` 
import React from "react";

class Clock extends React.Component {
    constructor(props) {
        super(props);
        this.state = {date: new Date()};
    }

    render() {
        return (
            <div>
                <h1>Hello, world!</h1>
                <h2>It is {this.state.date.toLocaleTimeString()}.</h2>
            </div>
        );
    }
}
`}</SyntaxHighlighter>
        <li>
          Klasa abstrakcyjna to klasa, na bazie której nie powinniśmy tworzyć
          nowych instancji, a jedynie ją w przyszłości rozszerzać. Na przykład
          gdy jest bazą dla innych klas i nie mamy potrzeby robić jej instacji
        </li>
        <li>W Javascript możemy rozszerzać wbudowane typy używając klas</li>
        <SyntaxHighlighter language="javascript" style={a11yDark}>{` 
class MyArray extends Array {
  constructor(...param) {
      super(...param);
  }

  sortNr() {
      return this.sort((a, b) => a - b);
  }
}

const tab1 = new Array(4, 5, 20, 21, 2.1, 1.2, 3);
tab1.sortNr(); //błąd : nie ma takiej metody

const tab2 = new MyArray(4, 5, 20, 21, 2.1, 1.2, 3);
tab2.sortNr();
console.log(tab2); //[1.2, 2.1, 3, 4, 5, 20, 21]
`}</SyntaxHighlighter>
      </ul>
    </>
  );
}

export default Objects;
