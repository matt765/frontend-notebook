import stos from "../../../../Images/stos.png";
import SyntaxHighlighter from "react-syntax-highlighter";
import { a11yDark } from "react-syntax-highlighter/dist/esm/styles/hljs";

function Asynchronous() {
  return (
    <>
      <h2>Asynchroniczność</h2>
      <h3>Wprowadzenie</h3>
      <ul>
        <li>
          Standardowo kod wykonuje się synchronicznie tzn. linia po linii,
          jednowątkowo. Oznacza to, że w danym momencie może wykonywać tylko
          jeden kod, a dopóki go nie wykona, nie jest w stanie wykonać nic
          innego.{" "}
        </li>
        <li>
          W Javascript występują asynchroniczne funkcje, jak setTimeout(),
          fetch() lub addEventlistener(). Pochodzą z globalnego obiektu window z
          API przeglądarki
        </li>
        <li>
          Asynchroniczny kod może być wykonywany równolegle do reszty aplikacji
          nie przeszkadzając jej w działaniu, ale też rezultat działania nie
          jest natychmiastowy.
        </li>
        <li>
          Asynchroniczność powstała żeby strona nie była zamrożona gdy node
          czeka na wykonanie jakiejś funkcji
        </li>
        <li>
          Jeśli wykonujemy funkcję, trafia on na stos (call stack). Kolejne
          funkcje w niej zawarte podwyższają stos. Potem wykonywany jest kod od
          góry stosu, czyli od najgłębszej funkcji.
        </li>
        <li>
          Jeśli funkcja jest asynchroniczna to trafia do przeglądarki (Web API)
          która przejmuje oczekiwanie na ich wykonanie. Gdy tam się załaduje to
          trafia do kolejki zdarzeń (event loop)
        </li>
        <li>
          Event loop cały czas "krąży" i sprawdza czy stos jest pusty. Nie może
          na niego nic wrzucić tylko czeka. Czyli w praktyce asynchroniczne
          funkcje robią się na końcu
        </li>
        <img src={stos} />
      </ul>

      <h3> Callbacks - funkcje zwrotne</h3>
      <ul>
        <li>
          Callback to funkcja przekazana jako argument (bez nawiasow) do innej
          funkcji. Najczęściej używana w przypadku funkcji asynchronicznych{" "}
        </li>
        <li>
          Nie każdy callback jest asynchroniczny, czyli sam callback nie
          wystarczy zeby funkcja była asynchroniczna. Np. forEach jest
          synchroniczne{" "}
        </li>
        <li>
          Żeby callback był asynchroniczny musi wykonywać asynchroniczną
          operację. Np. setTimeout/setInterval/korzystanie z API/nasłuchiwanie
          eventu/operacje na plikach
        </li>
        <SyntaxHighlighter language="javascript" style={a11yDark}>{`
  function myDisplayer(some) {
    document.getElementById("demo").innerHTML = some;
  }
  
  function myCalculator(num1, num2, myCallback) {
    let sum = num1 + num2;
    myCallback(sum);
  }
  
  myCalculator(5, 5, myDisplayer);

`}</SyntaxHighlighter>
        <li>
          {" "}
          Jeśli tworzymy funkcję ładującą dane z zewnątrz (jak skrypt lub plik),
          musi ona poczekać na załadowanie, w przeciwnym wypadku nie zdąży
          pobrać danych przed wykonaniem kodu. Dobrze wtedy używać callbacków
        </li>
        <SyntaxHighlighter language="javascript" style={a11yDark}>{`
  function myDisplayer(some) {
    document.getElementById("demo").innerHTML = some;
  }
  
  function getFile(myCallback) {
    let req = new XMLHttpRequest();
    req.open('GET', "mycar.html");
    req.onload = function() {
      if (req.status == 200) {
        myCallback(this.responseText);
      } else {
        myCallback("Error: " + req.status);
      }
    }
    req.send();
  }
  
  getFile(myDisplayer);

`}</SyntaxHighlighter>
        <li>
          {" "}
          Callbacki to dobra opcja gdy mamy 1 lub 2 funkcje. W przypadku ich
          większej ilości wpadamy w "callback hell" jeśli chcemy żeby funkcje
          wykonały się jedna po drugiej. Dlatego używamy obietnic
        </li>
      </ul>

      <h3> Promise - obietnice</h3>
      <ul>
        <li>
          Obietnice to obiekty które wyznaczają możliwy wynik asynchronicznej
          operacji.{" "}
        </li>
        <li>
          Może być w 1 z 3 stanów: pending, resolved/fullfilled lub rejected{" "}
        </li>
        <li>
          Dzięki nim możemy wykonać jakiś kod, a następnie odpowiednio
          zareagować na jego wykonanie. Można powiedzieć, żę to taka inna
          odmiana funkcji callback.
        </li>
        <li>
          Praca z obietnicami w zasadzie zawsze składa się z 2 kroków. Po
          pierwsze za pomocą konstruktora Promise tworzymy obietnicę. Po drugie
          za pomocą odpowiedniej funkcji reagujemy na jej zakończenie
          (konsumujemy ją)
        </li>
        <li>
          Resolve() i reject() to funkcje podawane jako parametr do funkcji
          wykonawczej. Resolve zmienia status obietnicy z pending na fullfilled
        </li>

        <SyntaxHighlighter language="javascript" style={a11yDark}>{` 
        let myPromise = new Promise(function(myResolve, myReject) {
          // "Producing Code" (May take some time)
          
            myResolve(); // when successful
            myReject();  // when error
          });
          
          // "Consuming Code" (Must wait for a fulfilled Promise)
          myPromise.then(
            function(value) { /* code if successful */ },
            function(error) { /* code if some error */ }
          );

          //
          const promise = new Promise((resolve, reject) => {
            if (zakończono_pozytywnie) {
                resolve("Wszystko ok 😁");
            } else {
                reject("Nie jest ok 😥");
            }
           });
        `}</SyntaxHighlighter>
        <li>
          Każda obietnica może zakończyć się na dwa sposoby - powodzeniem
          (resolve) i niepowodzeniem (reject). Gdy obietnica zakończy się
          powodzeniem (np. dane się wczytają), powinniśmy wywołać funkcję
          resolve(), przekazując do niej rezultat działania. W przypadku błędów
          powinniśmy wywołać funkcję reject(), do której przekażemy błędne dane
          lub komunikat błędu.{" "}
        </li>
        <li>
          Po stworzeniu nowego obiektu Promise, w pierwszym momencie ma ona
          właściwości state (w debugerze widoczna jako [[PromiseStatus]]
          ustawioną na "pending" oraz właściwość value ([[PromiseValue]]), która
          początkowo wynosi undefined.
        </li>
        <li>
          W momencie zakończenia wykonywania asynchronicznych operacji Promise
          przechodzi w stan "settled" (ustalony/załatwiony) i zostaje zwrócony
          jakiś wynik. Status takiego promise przełączany jest odpowiednio w
          "fulfilled" lub "rejected"
        </li>
        <li>
          Po rozwiązaniu (zakończeniu) Promise możemy zareagować na jego wynik.
          Służą do tego dodatkowe metody, które Promise nam udostępnia. Pierwszą
          z tych metoda jest <span className="important">then()</span>, druga to{" "}
          <span className="important">catch()</span> która służy reakcji na
          błąd, a trzecia to <span className="important">finally()</span>{" "}
        </li>
        <SyntaxHighlighter language="javascript" style={a11yDark}>{` 
function doSomething() {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            //resolve("Gotowe dane");
            reject("Przykładowy błąd"); //uwaga zwracamy błąd
        }, 1000);
    });
}

doSomething()
    .then(result => {
        ...
    })
    .catch(error => {
        console.error(error);
    });
          `}</SyntaxHighlighter>
        <li>
          Jeśli czynności chcemy zacząć po pozytywnym zakończeniu wszystkich
          asynchronicznych operacji używamy metody
          <span className="important">Promise.all()</span> a gdy nie ma
          znaczenia czy pozytywnie używamy{" "}
          <span className="important">allSettled()</span>
        </li>
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`Promise.all([
loadUser(),
loadBooks(),
loadPets(),
])
.then(res => {
    console.log(res);
})
.catch(err => {
    console.log(err);
})
    `}</SyntaxHighlighter>
        <li>
          {" "}
          Finally() odpalana jest na zakończenie operacji niezależnie od jej
          wyniku. Dobra np. do zastopowania wskaźnika ładowania
        </li>
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`button.classList.add("loading"); //pokazujemy loading
button.disabled = true; //i wyłączamy button

fetch("....")
    .then(res => res.json())
    .then(res => console.log(res))
    .catch(err => console.log(err))
    .finally(() => {
        button.classList.remove("loading");
        button.disabled = false;
    });        
      `}</SyntaxHighlighter>
      </ul>
      <h3> Async i Await</h3>
      <ul>
        <li>
          Używamy ich jako zamiennik konsumowania promisu przy pomocy .then
        </li>
        <li>
          Słowo async postawione przed dowolną funkcją tworzy z niej funkcję
          asynchroniczną, która zwraca obietnicę
        </li>
        <li>
          Słowo kluczowe await sprawia, że JavaScript poczeka na wykonanie
          asynchronicznego kodu. Dzięki temu zapis bardzo przypomina
          synchroniczny kod. Słowa await możemy używać tylko wewnątrz funkcji
          poprzedzonej słowem async
        </li>
        <li>
          Instrukcja await przed nazwą funkcji, oznacza że kolejna operacja
          rozpocznie się dopiero, gdy ta funkcja zakonczy dzialanie.
        </li>
      </ul>
    </>
  );
}

export default Asynchronous;
