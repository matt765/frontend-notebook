import SyntaxHighlighter from "react-syntax-highlighter";
import { a11yDark } from "react-syntax-highlighter/dist/esm/styles/hljs";

function Functions() {
  return (
    <>
      <h2>Funkcje</h2>
      <br />
      <h3 id="funkcje/map"> Map()</h3>
      <div class="text-section visible">
        Tworzy nową tablicę z wynikami użycia funkcji callback po kolei na
        każdym elemencie tablicy
        <SyntaxHighlighter language="javascript" style={a11yDark}>
          {`array.map(function(currentValue, index, arr), thisValue)

var numbers = [4, 9, 16, 25];
var x = numbers.map(Math.sqrt) // zwraca [2,3,4,5]`}
        </SyntaxHighlighter>
      </div>
      <h3 id="funkcje/reduce">Reduce()</h3>
      <div class="text-section">
        Redukuje tablicę do pojedynczej wartości, wykonując funkcję callback na
        każdym elemencie oryginalnej tablicy, od lewej do prawej.
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`array.reduce(function(total, currentValue, currentIndex, arr), initialValue)

const array = [‘J’, ‘U’, ‘S’, ‘T’, ‘J’, ‘O’, ‘I’, ‘N’, ‘.’, ‘I’, ‘T’];
const newText = array.reduce((prev, next) => \`\${prev}\${next}\`); // zwraca string “JUSTJOIN.IT”
`}</SyntaxHighlighter>
      </div>

      <h3 id="funkcje/filter">Filter()</h3>
      <div class="text-section">
        Tworzy nową tablicę wypełnioną elementami z oryginalnej tablicy które
        zdadzą test (z funkcji callback)
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`array.filter(function(currentValue, index, arr), thisValue)

var ages = [32, 33, 16, 40];
function checkAdult(age) {
  return age >= 18;
}
function myFunction() {
  document.getElementById("demo").innerHTML = ages.filter(checkAdult);
} 
`}</SyntaxHighlighter>
      </div>

      <h3 id="funkcje/every">Every() </h3>
      <div class="text-section">
        Sprawdza czy wszystkie elementy tablicy zdają test z funkcji callback.
        Zwraca true lub false
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`array.every(function(currentValue, index, arr), thisValue)

var ages = [32, 33, 16, 40];
function checkAdult(age) {
  return age >= 18;
}
function myFunction() {
  document.getElementById("demo").innerHTML = ages.every(checkAdult);
} 
`}</SyntaxHighlighter>
      </div>

      <h3 id="funkcje/some">Some()</h3>
      <div class="text-section">
        Sprawdza czy jakikolwiek element tablicy zda test z funkcji callback.
        Zwraca true lub false
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`array.some(function(currentValue, index, arr), thisValue)

var ages = [3, 10, 18, 20];
function checkAdult(age) {
  return age >= 18;
}
function myFunction() {
  document.getElementById("demo").innerHTML = ages.some(checkAdult);
} 
`}</SyntaxHighlighter>
      </div>

      <h3 id="funkcje/includes">Includes()</h3>
      <div class="text-section">
        Sprawdza czy tablica zawiera dany element. Zwraca true lub false
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`array.includes(element, start)

var fruits = ["Banana", "Orange", "Apple", "Mango"];
var n = fruits.includes("Mango"); //zwraca true
`}</SyntaxHighlighter>
      </div>

      <h3 id="funkcje/find">Find()</h3>
      <div class="text-section">
        Zwraca wartość pierwszego elementu z tablicy który zda test z funkcji
        callback
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`const array1 = [5, 12, 8, 130, 44];

const found = array1.find(element => element > 10);

console.log(found);
// expected output: 12
`}</SyntaxHighlighter>
      </div>

      <h3 id="funkcje/object.keys">Object.keys(wybrany_obiekt)</h3>
      <div class="text-section">
        Tworzy tablicę składającą się z kluczy wybranego obiektu
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`const object1 = {
  a: 'somestring',
  b: 42,
  c: false
};

console.log(Object.keys(object1));
// expected output: Array ["a", "b", "c"]
`}</SyntaxHighlighter>
      </div>
      <h3 id="funkcje/object.values">Object.values(wybrany_obiekt)</h3>
      <div class="text-section">
        Tworzy tablicę skłądającą się z właściwości wybranego obiektu. Podobna
        do działania pętli for...in
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`const object1 = {
  a: 'somestring',
  b: 42,
  c: false
};

console.log(Object.values(object1));
// expected output: Array ["somestring", 42, false]
`}</SyntaxHighlighter>
      </div>

      <h3 id="funkcje/object.entries">Object.entries(wybrany_obiekt)</h3>
      <div class="text-section">
        Tworzy tablicę składającą się z par klucz - właściwość, pobranych z
        wybranego obiektu. Kolejność nie jest gwarantowana, tablica może wymagać
        posortowania
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`const obj = { name: "Adam", age: 20, location: "Nepal" };
console.log(Object.entries(obj)); // [ [ 'name', 'Adam' ], [ 'age', 20 ], [ 'location', 'Nepal' ] ]
`}</SyntaxHighlighter>
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`const object1 = {
  a: 'somestring',
  b: 42
};

for (const [key, value] of Object.entries(object1)) {
  console.log(\`\${key}: \${value}\`);
}

// expected output:
// "a: somestring"
// "b: 42"
// order is not guaranteed
`}</SyntaxHighlighter>
      </div>
      <h3 id="funkcje/bind"> Bind()</h3>
      <div class="text-section">
        Metoda bind() zwraca nową funkcję, której wywołanie powoduje ustawienie
        this na podaną w parametrze wartość. Pozwala obiektowi pożyczyć metodę
        innego obiektu bez tworzenia kopii jego metody.
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`let runner = {
  name: 'Runner',
  run: function(speed) {
      console.log(this.name + ' runs at ' + speed + ' mph.');
  }
};

let flyer = {
  name: 'Flyer',
  fly: function(speed) {
      console.log(this.name + ' flies at ' + speed + ' mph.');
  }
};
let run = runner.run.bind(flyer, 20);
run(); /// Flyer runs at 20 mph.
`}</SyntaxHighlighter>
      </div>
      <h3 id="funkcje/setTimeout">setTimeout()</h3>
      <div class="text-section">
        Funkcja przydatna gdy chcemy wykonać akcję po czasie. Jego wartość jest
        przybliżona.
        <SyntaxHighlighter language="javascript" style={a11yDark}>{` 
        function myFunc() {
          console.log("Jakiś tekst");
        }
      
      setTimeout(myFunc, 1200); //odpali po 1.2s

      //funkcja przerywająca 
      const time = setTimeout(() => {
        console.log("Pełne zaskoczenie");
      }, 10000);
    
       clearTimeout(time);

      

      `}</SyntaxHighlighter>
      </div>
      <h3 id="funkcje/setInterval"> setInterval()</h3>
      <div class="text-section">
      ???????????????????????????????????
        <SyntaxHighlighter language="javascript" style={a11yDark}>{` 
        function myFunc() {
          console.log("Jakiś tekst");
        }    
    
       //interwał + jego przerwanie, przyjmuje 1 parametr 
       let i = 0;
       const interval = setInterval(() => {
             i++;
             console.log(i);
             if (i >= 10) {
                 clearInterval(interval);
             }
       }, 1000);

      `}</SyntaxHighlighter>
      </div>
      <h3 id="funkcje/IIFE">IIFE </h3>
      <ul>
        <li>
          Immediately-invoked function expression - czyli samo wywołujące się
          wyrażenie funkcyjne to wzorzec funkcji, która sama się wywołuje.
        </li>
        <li>
          Pierwszy () - zawiera funkcję której scope nie jest dostępny z
          zewnątrz, drugi () powoduje wykonanie funkcji po deklaracji
        </li>
        <li>Kiedyś używano tego do ograniczania zasięgu zmiennych var</li>
      </ul>
    </>
  );
}

export default Functions;
