import formhtml from "../../../../Images/formhtml.png";
import dom from "../../../../Images/dom.png";
import eventf from "../../../../Images/eventf.png";
import atrybuty from "../../../../Images/atrybuty.png";
import events2 from "../../../../Images/events.png";
import formp from "../../../../Images/formp.png";
import formz from "../../../../Images/formz.png";
import SyntaxHighlighter from "react-syntax-highlighter";
import { a11yDark } from "react-syntax-highlighter/dist/esm/styles/hljs";

function DOM() {
  return (
    <>
      <h2>Document Object Model</h2>
      <h3>Wprowadzenie</h3>
      <ul>
        <li>
          Do odzwierciedlenia struktury elementów na stronie Javascript (ale też
          niektóre inne języki) korzysta z DOM czyli Document Object Model.
          Model ten opisuje jak zaprezentować tekstowe dokumenty HTML w postaci
          modelu obiektowego w pamięci komputera. Przeglądarka wiec czyta kod
          naszej strony, parsuje go a następnie podaje nam zbudowane na jego
          podstawie drzewo DOM.{" "}
        </li>
        <img src={dom} />
        <li>Dostęp do elementów DOM </li>
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`document.getElementById('someid');
document.getElementsByClassName('someclass');
document.getElementsByTagName('LI');
document.querySelector('ul li'); // pierwszy pasujący element
document.querySelectorAll('div.note, div.alert'); // kolekcja elementów
        `}</SyntaxHighlighter>

        <li>
          Dostęp do dzieci/rodziców - każdy element na stronie tworzy tak zwany
          węzeł. Takimi węzłami są nie tylko elementy, ale także tekst w nich
          zawarty.{" "}
        </li>
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`const p = document.querySelector("p");

// pobierające elementy html
p.parentElement //wskazuje na nadrzędny element - div.text-cnt
p.firstElementChild //pierwszy element w #text
p.lastElementChild //ostatni element w #text
p.children //[strong, span] - kolekcja dzieci elementu #text
p.children[0] //wskazuje na 1 element - <strong style="color:red">Ala</strong>
p.nextElementSibling //następny brat-element
p.previousElementSibling //poprzedni brat-element

// pobierające węzły
p.parentNode //wskazuje na nadrzędny węzeł - div.text-cnt
p.firstChild //pierwszy node - w naszym przypadku to tekst "Mała "
p.lastChild //ostatni node - "" - html jest sformatowany, wiec ostatnim nodem jest znak nowej linii
p.childNodes //[text, strong, text] - kolekcja wszystkich dzieci - nodów
p.childNodes[0] //"Mała"
p.nextSibling //następny węzeł
p.previousSibling //poprzedni węzeł
        `}</SyntaxHighlighter>
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`//metoda element.closest("selektor") odnajduje najbliższy element w górę drzewa

const btn = document.querySelector(".button")
btn.addEventListener("click", e => {
  const module = btn.parentElement.parentElement.parentElement;
  // lub
  const module = btn.closest(".module");
});   
        `}</SyntaxHighlighter>
        <li>Gotowe kolekcje </li>
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`document.body //element body
document.all //kolekcja ze wszystkimi elementami na stronie
document.forms //kolekcja z formularzami na stronie
document.images //kolekcja z grafikami img na stronie
document.links //kolekcja z linkami na stronie
document.anchors //kolekcja z linkami będącymi kotwicami`}</SyntaxHighlighter>
        <li>Tworzenie nowych elementów </li>
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{` // create new elments
var newHeading = document.createElement('h1');
var newParagraph = document.createElement('p');

// create text nodes for new elements
var h1Text= document.createTextNode('This is a nice header text!');
var pText= document.createTextNode('This is a nice paragraph text!');

//Klonowanie
const cloneEl1 = el.cloneNode();
        `}</SyntaxHighlighter>

        <li>Dodawanie elementów do DOM </li>
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`// grab element on page you want to add stuff to
var firstHeading = document.getElementById('firstHeading');

// wstawia element na koniec wybranego elementu
firstHeading.appendChild(newHeading);
firstHeading.appendChild(newParagraph);

// can also insert before like so

// get parent node of firstHeading
var parent = firstHeading.parentNode;

// insert newHeading before FirstHeading
parent.insertBefore(newHeading, firstHeading);
        `}</SyntaxHighlighter>

        <li>Usuwanie elementów</li>
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`const div = document.querySelector("div")
const el = div.querySelector("span");
const btn = document.querySelector("button");

btn.addEventListener("click", e => {
    parent.removeChild(el);

    //lub
    el.parentElement.removeChild(el);

    //lub najprościej
    el.remove();
});        `}</SyntaxHighlighter>

        <li>Modyfikacja klas </li>
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`el.classList.add("btn");
el.classList.add("btn", "btn-primary");
el.classList.remove("btn");
el.classList.toggle("btn");
el.classList.contains("btn");
el.classList.replace("old", "new");        `}</SyntaxHighlighter>

        <li> HTML & inline CSS</li>
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`const element = document.querySelector("div");
element.style.color = "red"; //ustawiam kolor tekstu na czerwony

const btn = document.querySelector(".btn");
console.log(btn.innerHTML); // odczyt html
console.log(btn.innerText); /// tekst ze stylami
console.log(btn.textContent); // tekst oryginalny z pliku html, z wcięciami itp.

//tagName - nazwa elementu
const elements = document.querySelectorAll("body *");
for (const el of elements) {
    if (el.tagName === "STRONG") {
        el.style.border = "1px solid red";
    }
}

//kilka wartosci naraz
el.style.cssText = \`
    color: red;
    background: blue;
    padding: 10px;
\`;

//pobranie CSS - poza inline mozemy pobrac tylko computed tzn. to co juz w przeglądarce
window.getComputedStyle(elem, pseudoElement*).   `}</SyntaxHighlighter>
        <li>
          {" "}
          setProperty() i getPropertyValue() - ustawienie stylowania, uzywane ze
          zmiennymi CSSa
        </li>
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`el.style.setProperty("--size", "1em");
el.style.getPropertyValue("--size"); //"1em"       `}</SyntaxHighlighter>
      </ul>
      <h3>Pętle w DOM</h3>
      <ul>
        <li>Pętla po kolekcji</li>
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`const elements = document.querySelectorAll(".module");

    elements[0].style.color = "red"; //ok bo pierwszy element w kolekcji
    
    //ok, bo robimy pętlę
    for (const el of elements) {
        el.style.color = "red";
    }
`}</SyntaxHighlighter>
        <li>
          Na kolekcji nie możemy używać metod tablic (filter,some,map). Wymagana
          jest konwersja za pomocą spread syntax/Array.from() lub użycie forEach
        </li>
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`const buttons = document.querySelectorAll("button");
[...buttons].map(el => el.style.color = "red");
          `}</SyntaxHighlighter>
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`const elements = document.querySelectorAll(".module");

            elements.forEach(el => {
                el.style.color = "blue"
            });`}</SyntaxHighlighter>
      </ul>
      <h3>Obsługa zdarzeń</h3>
      <ul>
        <li>
          <span className="important">Event bubbling</span> - gdy ma miejsce
          event na elemencie, wywoływany jest jego handler, potem handler jego
          rodzica, a potem kolejnych rodziców aż do góry DOM
        </li>
        <li>
          Gdy event handler jest na rodzicu, najgłębiej zagnieżdżony kliknięty
          element powodujący event to 'target', do którego dostęp zapewnia
          'event.target'
        </li>
        <li>
          'Event.target' to element który wywołał event, a 'this' to w tym
          przypadku element do którego dołączony jest handler. Czyli this i
          event.target są równoważne przy kliknięciu w samego rodzica
        </li>
        <li>
          Event bubbling możemy zatrzymać funkcją
          <span className="important">.stopPropagation()</span>
        </li>
        <li>
          Gdy klikniemy zagnieżdżony element, przed fazą bubblingu następuje
          faza <span className="important">event capturing</span>. Normalnie
          jest dla nas niewidzialna, rzadko używana. Możemy jej użyc dodając
          'true' jako parametr w addEventListener
        </li>
        <li>
          <span className="important">Event delegation</span> - użycie
          event.target w handlerze rodzica, w celu wykrycia elementu
          zagnieżdżonego bez potrzeby dołączania handlerów do wszystkich dzieci
        </li>

        <img src={eventf} />
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`element.addEventListener('nameOfEvent', functionToRun);
element.removeEventListener('nameOfEvent', functionToStopRunning);
e.preventDefault() // zapobieganie domyślnej akcji
console.log("e.target: ", e.target); // e.target wskazuje element na którym zdarzenie się wydarzyło (nastapiła faza target)
console.log("e.currentTarget: ", e.currentTarget, parent); // wskazuje na element do którego podpięliśmy funkcję nasłuchującą
 `}</SyntaxHighlighter>
        <img src={events2} />
      </ul>

      <h3>Praca z atrybutami HTMLa</h3>
      <ul>
        <img src={atrybuty} />
      </ul>

      <h3>dataset</h3>
      <ul>
        <li>
          Własne atrybuty możemy obsługiwać za pomocą powyższych metod, ale
          możemy dla nich skorzystać z właściwości dataset. Jest to obiekt,
          którego kolejne właściwości są budowane na bazie niestandardowych
          atrybutów
        </li>
        <li>
          Przy podawaniu nazwy danego atrybutu pomijamy początek data-, a
          myślniki w nazwie zamieniamy na zapis camelCase.{" "}
        </li>
        <SyntaxHighlighter language="javascript" style={a11yDark}>{`</code>
    tooltip.dataset.myCustomData = "nowa wartość"; //utworzy w elemencie atrybut data-my-custom-data="nowa wartość"
    tooltip.dataset.style = "primary"; //utworzy atrybut data-style="primary"
    tooltip.dataset.moduleSize = "big" //utworzy atrybut data-module-size="big"
            
    //to samo możemy uzyskać za pomocą getAttribute i setAttribute
    tooltip.setAttribute("data-custom-data", "nowa wartość");
    tooltip.setAttribute("data-style", "primary");
    tooltip.setAttribute("data-module-size", "big");
          `}</SyntaxHighlighter>
      </ul>

      <h3>Formularze</h3>
      <ul>
        <li>Przykładowy formularz</li>
        <form
          action=""
          method="get"
          oninput="x.value=parseInt(a.value)+parseInt(b.value)"
        >
          <br />
          <div className="formcolumn">
            <label for="fname">Tekst</label>
            <br />
            <input
              type="text"
              id="firstId"
              name="firstName"
              value="firstValue"
            />
            <br />
            <label for="fname">Radio</label>
            <br />
            <input type="radio" />
            <br />
            <label for="fname">Checkbox</label>
            <br />
            <input type="checkbox" />
            <br />
            <label for="fname">Button</label>
            <br />
            <button type="button" onclick="">
              Click Me!
            </button>{" "}
            <br />
          </div>
          <div className="formcolumn">
            <label for="fname">Submit</label>
            <br />
            <input type="submit" />
            <br />
            <label for="cars">Choose a car:</label>
            <br />
            <select id="cars" name="cars">
              <br />
              <option value="volvo">Volvo</option>
              <option value="saab">Saab</option>
              <option value="fiat">Fiat</option>
              <option value="audi">Audi</option>
            </select>{" "}
            <br />
            <textarea name="message" rows="10" cols="30">
              The cat was playing in the garden.
            </textarea>{" "}
            <br />
            0
            <input type="range" id="a" name="a" value="50" />
            100 +
            <input type="number" id="b" name="b" value="50" />=
            <output name="x" for="a b"></output>
            <br />
            <br />
            <br />
            <br />
          </div>
        </form>
        <li>Właściwości i metody dla całych formularzy</li>
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`//właściwości
form.elements - kolekcja wszystkich elementów formularza 
form.action - atrybut action formularza, czyli adres na który formularz zostanie wysłany
form.method - metoda jaką zostanie wysłany formularz (omawiana w dziale Ajax)

//metody
form.submit(); //funkcja wysyłająca formularz
form.reset(); //funkcja resetująca formularz   
        `}</SyntaxHighlighter>
        <li>
          Główną interesującą nas wartością jest{" "}
          <span className="important">value</span> czyli aktualna wartość danego
          pola, która zawsze jest stringiem{" "}
        </li>
        <li>Najczęściej wykorzystywane właściwości</li>
        <img src={formp} />
        <li>Najczęściej wykorzystywane zdarzenia</li>
        <img src={formz} />
        <li>
          Skrypt wykonujący pętlę po radio buttonach i wyswietlający wynik który
          pobiera z napisu obok radio buttona
        </li>
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`const form = document.querySelector("#myForm");
const genderRadio = form.querySelectorAll("input[name=gender]");
const resultElement = document.querySelector("#genderResult");

for (const radio of genderRadio) {
    radio.addEventListener("change", e => {
        for (const radio of genderRadio) {
            if (radio.checked) {
                resultElement.innerText = radio.nextSibling.data; //pobieram tekst leżący obok radio
                break;
            }
        }
    });
}

`}</SyntaxHighlighter>
        <li>
          Dzięki Javascript dla checkboxów możemy ustawić trzecią, niedostępną z
          HTML opcję - czyli <span className="important">indeterminate</span>,
          która oznacza pół zaznaczenie
        </li>
        <li>Select-właściwości</li>
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`select.value //wartość pola
select.options //kolekcja elementów option
select.selectedIndex //indeks wybranego optiona
select.options[selectedIndex] - zaznaczony option
`}</SyntaxHighlighter>
        <li>
          Dla select możemy ustawić nowe opcje używają funkcji
          <span className="important"> createElement</span>lub konstruktora{" "}
          <span className="important">
            Option(text*, value*, defaultSelected*, selected*)
          </span>
        </li>
      </ul>

      <h3>Walidacja formularzy</h3>
      <ul>
        <li>
          Walidację można przeprowadzać 1) Po stronie HTML 2) W czasie
          wprowadzania danych przez użytkownika, z wykorzystaniem zdarzeń typu
          change/focus/keypress itd, 3) Tuż przed wysłaniem 4) Po stronie
          serwera
        </li>
        <li>Walidacja po stronie HTML</li>
        <img src={formhtml} />
        <li>
          Aby wykonać walidację przed wysłaniem danych podpinamy się pod
          zdarzenie <span className="important">submit</span> i używamy{" "}
          <span className="important">preventDefault()</span>
        </li>
        <li>
          Gdy pojawi się w naszym formularzu kilka pól, trzeba jakoś zebrać
          wyniki. Rozwiązań jest wiele. Jednym z nich jest ręczne sprawdzanie
          kolejnych pól, gdzie wyniki testów możemy trzymać w oddzielnej
          tablicy.
        </li>
      </ul>
    </>
  );
}

export default DOM;
