import gen from "../../../../Images/gen.png";
import SyntaxHighlighter from "react-syntax-highlighter";
import { a11yDark } from "react-syntax-highlighter/dist/esm/styles/hljs";

function Iterators() {
  return (
    <>
      <h2>Iteratory i generatory</h2>
      <h3>Sposoby na iterowanie po obiektach</h3>
      <ul>
        <li>Pętla for...in - iteruje po kluczach obiektu</li>
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`const object = { a: 1, b: 2, c: 3 };

for (const property in object) {
  console.log(\`\${property}: \${object[property]}\`);
}

// expected output:
// "a: 1"
// "b: 2"
// "c: 3" `}</SyntaxHighlighter>
        <li>Pętla for...of - iteruje po właściwościach obiektu</li>
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`const array1 = ['a', 'b', 'c'];

for (const element of array1) {
  console.log(element);
}

// expected output: "a"
// expected output: "b"
// expected output: "c" `}</SyntaxHighlighter>
        <li>Metoda .forEach na tabeli kluczy i/lub wartości</li>
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`Object.keys(myObj).forEach((key) => {
  let val = myObj[key];
  // do something with key and val
}); `}</SyntaxHighlighter>
        <li>Metoda .map na tabeli kluczy i/lub wartości</li>
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`Object.values(myObj).map(val => ... )
Object.entries(myObj).map(([key, val]) => ({
  [key]: val
})) `}</SyntaxHighlighter>
      </ul>

      <h3>Sposoby na iterowanie po tablicach</h3>
      <ul>
        <li>Pętle for i while</li>
        <li>Pętla for...of</li>
        <li>Funkcja forEach()</li>
        <li>Funkcja map()</li>
        <li>Funkcja filter()</li>
        <li>Funkcja reduce()</li>
      </ul>

      <h3>Pętla for...of i generatory</h3>
      <ul>
        <li>
          Wprowadzona w ES5 pętla for...of pozwala iterować praktycznie po
          każdej strukturze danych. W Javascript funkcje zwracające iteratory
          zaimplementowane są pod kluczami Symbol.iterator
        </li>
        <li>
          Iterować można po typach: arrays, strings, Maps/Sets, kolekcje DOM
        </li>
        <li>
          Każdy z tych typów danych ma zaimplementowaną metodę, która zwraca tak
          zwany Iterator, czyli obiekt który potrafi odwołać się do kolejnych
          elementów z danej struktury, a równocześnie wywoływany w sekwencji
          potrafi zapamiętać swoją bieżącą pozycję. Taki obiekt zawiera metodę
          next(), która służy do zwracania kolejnego elementu w kolekcji.{" "}
        </li>
        <SyntaxHighlighter language="javascript" style={a11yDark}>{`
            const tab = ["Ala", "Bala", "Cala"];

            for (const el of tab) {
                console.log(el); //"Ala", "Bala", "Cala"...
            }
            
            const iter = tab[Symbol.iterator]();
            iter.next(); //{value: "Ala", done: false}
            iter.next(); //{value: "Bala", done: false}
            iter.next(); //{value: "Cala", done: false}
            iter.next(); //{value: undefined, done: done}

          `}</SyntaxHighlighter>
        <li>
          Implementacja iteratorów dla innych obiektów jest problematyczna,
          dlatego wprowadzono <span className="important">generatory</span>
        </li>
        <li>
          Funkcja staje się generatorem, gdy zawiera przynajmniej jedno
          wystąpienie słowa yield, oraz przy jej deklaracji pojawia się znak *.
          Generator automatycznie zwraca metodę next(), która przy każdorazowym
          użyciu będzie zwracać kolejne wystąpienia yield:
        </li>
        <SyntaxHighlighter language="javascript" style={a11yDark}>{`
  function* tabLoop() {
    const tab = ["ala", "bala", "cala"];

    for (const el of tab) {
        yield el;
    }
}

const gen = tabLoop();
gen.next(); //{value: "ala", done: false}
gen.next(); //{value: "bala", done: false}
gen.next(); //{value: "cala", done: false}
gen.next(); //{value: undefined, done: true} 
`}</SyntaxHighlighter>
        <li>Implementacja w obiekcie</li>
        <SyntaxHighlighter language="javascript" style={a11yDark}>{`
const ob = {
    names: ["Ala", "Bala", "Cala"],

    *[Symbol.iterator]() {
        for (const el of this.names) {
            yield el;
        }
    }
}

const iterator = ob[Symbol.iterator]();
iterator.next(); //{value: "Ala", done: false}
iterator.next(); //{value: "Bala", done: false}
iterator.next(); //{value: "Cala", done: false}
iterator.next(); //{value: undefined, done: true}

for (const el of ob) {
    console.log(el); //"Ala", "Bala", "Cala"...
}
`}</SyntaxHighlighter>
        <li>
          Javascript udostępnia nam dodatkowo kilka funkcji, dzięki którym
          możemy w przyjemniejszy sposób iterować po wartościach czy kluczach
          danego obiektu.
        </li>
        <img src={gen} />
        <SyntaxHighlighter language="javascript" style={a11yDark}>{`
  const car = {
    brand : "BMW",
    color : "red",
    speed : 150
}

for (const key of Object.keys(car)) {
    console.log(key); //brand, color, speed
}

for (const val of Object.values(car)) {
    console.log(val); //"BMW", "red", 150
}

for (const [key, val] of Object.entries(car)) {
    console.log(key, val); //[brand, "BMW"], [color, "red"], [speed, 150]
}

//...
//pętli for of bezpośrednio po powyższym obiekcie nie zrobimy, bo nie zaimplementowaliśmy
//mu funkcji *[Symbol.iterator]()

`}</SyntaxHighlighter>
      </ul>
    </>
  );
}

export default Iterators;
