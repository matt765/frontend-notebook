import map from "../../../../Images/map.png";
import set from "../../../../Images/set.png";
import SyntaxHighlighter from "react-syntax-highlighter";
import { a11yDark } from "react-syntax-highlighter/dist/esm/styles/hljs";

function MapSet() {
  return (
    <>
      <h2>Map & Set</h2>
      <h3>Map()</h3>
      <ul>
        <li>
          Map i Set to dwie struktury danych, które są czymś pomiędzy tablicami
          i klasycznymi obiektami.
        </li>

        <li>
          Mapy służą do tworzenia zbiorów z parami [klucz - wartość]. Kluczami
          może być tutaj dowolny typ danych, gdzie w przypadku obiektów i tablic
          są one konwertowane na tekst{" "}
        </li>
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`const map = new Map();
map.set("kolor1", "red");
map.set("kolor2", "blue");

//lub

const map = new Map([
    ["kolor1", "red"],
    ["kolor2", "blue"],
]);
        `}</SyntaxHighlighter>
        <li>Dla każdej mapy mamy dostęp do kilku metod</li>
        <img src={map} />

        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`          const map = new Map();

          map.set("kolor1", "red");
          map.set("kolor2", "blue");
          map.set("kolor3", "yellow");
          
          console.log(map.get("kolor1")); //red
          console.log(map.delete("kolor2"));
          console.log(map.keys()); //MapIterator {"kolor1", "kolor3"} `}</SyntaxHighlighter>

        <li>
          Jeżeli będziemy chcieli iterować po mapie, możemy wykorzystać pętlę
          for of i poniższe funkcje zwracające iteratory lub użyć funkcji
          forEach()
        </li>
        <SyntaxHighlighter language="javascript" style={a11yDark}>{`
        const map = new Map([
            ["kolor1", "red"],
            ["kolor2", "blue"],
            ["kolor3", "yellow"]
        ]);
        
        for (const key of map.keys()) {
            //kolor1, kolor2, kolor3
        }
        
        for (const key of map.values()) {
            //red, blue, yellow
        }
        
        for (const entry of map.entries()) {
            //["kolo1", "red"]...
        }
        
        for (const [key, value] of map.entries()) {
            //key : "kolo1", value : "red"...
        }
        
        for (const entry of map) {
            //["kolor1", "red"]...
        }

        ///////

        const map = new Map([
        ["kolor1", "red"],
        ["kolor2", "blue"],
        ["kolor3", "yellow"]
        ]);
        
        map.forEach((value, key, map) => {
        console.log(\`
            Wartość: \${value}
            Klucz: \${key}
        \`);
        });
          `}</SyntaxHighlighter>
      </ul>
      <h3>Set()</h3>
      <ul>
        <li>
          Obiekt Set jest kolekcją składającą się z unikalnych wartości, gdzie
          każda wartość może być zarówno typu prostego jak i złożonego. W
          przeciwieństwie do mapy jest to zbiór pojedynczych wartości.
        </li>
        <img src={set} />

        <SyntaxHighlighter language="javascript" style={a11yDark}>{` 
            const set = new Set();
            set.add(1);
            set.add("text");
            set.add({name: "kot"});
            console.log(set); //{1, "text", {name : "kot"}}
            
            //lub
            //const set = new Set(elementIterowalny);
            const set = new Set([1, 1, 2, 2, 3, 4]); //{1, 2, 3, 4}
            const set = new Set("kajak"); //{"k", "a", "j"}

            //metody
            const mySet = new Set();
            mySet.add(1);
            mySet.add(5);
            mySet.add(5);
            mySet.add("text"); //Set { 1, 5, "text"}

            mySet.has(5); // true
            mySet.delete(5); //Set {1, "text"}
            console.log(mySet.size); //2
          `}</SyntaxHighlighter>
        <li>
          Dzięki temu, że Set zawiera niepowtarzające się wartości, możemy to
          wykorzystać do odsiewania duplikatów w praktycznie dowolnym elemencie
          iteracyjnym - np. w tablicy
        </li>
        <SyntaxHighlighter language="javascript" style={a11yDark}>{`
            const tab = [1, 1, 1, 2, 2, 2, 3, 3, 3, 4, 5, 5];

            const set = new Set(tab);
            console.log(set); //{1, 2, 3, 4, 5}
            
            const uniqueTab = [...set];
            console.log(uniqueTab); //[1, 2, 3, 4, 5] 
          `}</SyntaxHighlighter>
      </ul>
      <h3>WeakMap() i WeakSet()</h3>
      <ul>
        <li>
          WeakMap() to odmiana Mapy, różniąca się od niej tym że nie można po
          niej iterować, kluczami mogą być tylko obiekty, a jej elementy są
          automatycznie usuwane gdy do obiektu(klucza) nie ma referencji
        </li>
        <li>
          Podobnie jak dla Map istnieją WeakMap, tak dla Setów istnieją WeakSet.
          Są to kolekcje składające się z unikalnych obiektów. Podobnie do
          WeakMap obiekty takie będą automatycznie usuwane z WeakSet, jeżeli do
          danego obiektu zostaną usunięte wszystkie referencje.
        </li>
      </ul>
    </>
  );
}

export default MapSet;
