import SyntaxHighlighter from "react-syntax-highlighter";
import { a11yDark } from "react-syntax-highlighter/dist/esm/styles/hljs";

function FP() {
  return (
    <>
      <h2>Programowanie funkcyjne</h2>
      <h3>Wprowadzenie</h3>
      <ul>
        <li>
          Paradygmat programowania wg którego w większości przypadków piszemy
          kod używając funkcji, które nie używają danych z zewnętrznego scope'u
          i mutacji obiektów.
        </li>
        <li>
          Pozwala pisać kod z mniejszą ilością błędów, łatwiejszy do obsługi i w
          krótszym czasie.{" "}
        </li>
        <li>
          <span className="important">Pure functions</span> - funkcje które nie
          modyfikują nic poza swoim scope'm i zawsze zwracają ten sam wynik przy
          tych samych parametrach
        </li>
        <li>
          Programowanie funkcyjne nie pozwala na mutacje danych lub
          współdzielony stan, funkcje powinny pozostać czyste (pure functions)
        </li>
        <li>
          W Javascript funkcje są obiektami{" "}
          <span className="important">pierwszej klasy</span>, czyli są
          traktowane jako wartość, przez co mogą być
          <ul>
            <li>Przechowywane w zmiennej, obiekcie lub tablicy</li>
            <li>Przekazane jako argument do funkcji</li>
            <li>Zwrócone z funkcji</li>
          </ul>
        </li>
        <li>
          <span className="important">Funkcje wyższego rzędu</span> to funkcje,
          które otrzymują inną funkcję jako argument i opcjonalnie mogą zwracać
          funkcję
        </li>
        <li>
          Mechanizm łączenia funkcji z użyciem funkcji wyższego rzędu nazywamy
          <span className="important">komponowaniem</span>{" "}
        </li>
        <li>
          W JS funkcje mozna traktować jak wartości. Można to wykorzystać do
          dzielenia kodu na mniejsze funkcje i komponować je używając funkcji
          wyższego rzędu
        </li>
        <li>Funkcje Unary przyjmują tylko 1 argument, a funkcje Binary dwa</li>
        <li>
          <span className="important">Currying</span> to proces, w którym
          rozkładamy funkcję przyjmującą wiele argumentów na serię funkcji
          przyjmujących 1 argument. Upraszcza kod, czyniąc debugowanie i ponowne
          użycie kodu łatwiejszym. Pomocne przy obsłudze eventów
        </li>
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`function add (a, b) {
  return a + b;
}

add(3, 4); /// zwraca 7

/// Currying
function add (a) {
  return function (b) {
    return a + b;
  }
}

/// Wywołanie funkcji
add(3)(4);

/// Alternatywnie
var add3 = add(3);
add3(4);
            
          `}</SyntaxHighlighter>
      </ul>
      <h3>Immutability</h3>

      <ul>
        <li>
          Właściwość pewnych typów danych, oznaczająca że raz utworzone nie mogą
          już zmienić swojej struktury i wartości.
        </li>

        <li>Immutability ułatwia śledzenie zmian</li>
        <li>
          Prymitywne typy danych są immutable: boolean, null, undefined, number,
          bigint, string, symbol
        </li>
        <li>
          Jeśli chcemy zaaplikować w OOP immutability możemy użyć np.
          Object.assign() tworząc nowy obiekt bez zmieniania poprzedniego.
        </li>
        <li>
          W Reakcie stan to obiekt. Proces "Reconciliation" determinuje czy
          komponent powinien się ponownie wyrenderować. React sam w sobie nie
          może sprawdzić czy stan się zmienił i nie wie czy aktualizować Virtual
          DOM. Po zaaplikowaniu Immutabiility React może śledzić zmiany stanu.
          Dlatego w Reakcie nie poleca się bezpośredniej modyfikacji stanu.
        </li>
      </ul>
      <h3> Domknięcia (closures) </h3>
      <ul>
        <li>
          Domknięcie jest funkcją skojarzoną z odwołującym się do niej
          środowiskiem. Oznacza to, że funkcja ma dostęp do wszystkiego na
          poziomie jej zakresu i wyżej
        </li>
        <li>
          Gdy funkcja rodzic ma wewnątrz inną funkcję która korzysta ze
          zmiennych rodzica, zagnieżdżona funkcja ma dostęp do tych zmiennych
          nawet gdy zostaną usunięte przez Garbage Collector po wykonaniu
          funkcji rodzica, bo wytwarza własne "domknięcie
        </li>
        <li>
          Domknięcie to jak "migawka" środowiska w którym umieszczona jest
          funkcja. Domknięcie zagnieżdżonej funkcji to inaczej wewnętrzna kopia
          tego środowiska.
        </li>
        <li>
          W wielu językach programowania funkcja nie ma zasięgu do elementów
          poza nią. W Javascript dostęp zapewniają domknięcia
        </li>
        <li>
          Najczęściej o użyciu domknięć mówi się w kontekście umieszczania
          funkcji wewnątrz funkcji
        </li>
        <li>
          Przykładem użycia domknięć są funkcje jak fetch(), gdzie wszystkie
          funkcje po .then mają dostęp do obiektu zwróconego w obietnicy.
        </li>
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`function makeAdder(x) {
  return function(y) {
    return x + y;
  };
}

var add5 = makeAdder(5);
var add10 = makeAdder(10);

console.log(add5(2));  // 7
console.log(add10(2)); // 12
        `}</SyntaxHighlighter>
      </ul>

      <h3> Funkcje Filter i Reject</h3>
      <ul>
        <li>
          {" "}
          Można nią zastąpić pętlę for. Funkcja filter loopuje przez elementy
          tablicy i przekazuje je do callback function (podanej w argumencie)
        </li>

        <li>
          Iteruje przez tablicę. Jako argument podajemy inną funkcję która
          zwraca jakąś wartość.
        </li>
        <li>
          {" "}
          Funkcja filter tworzy nową wersję tablicy, "przefiltrowaną" wg
          podanego warunku
        </li>
        <li> Funkcja reject to odwrotność funkcji filter</li>

        <SyntaxHighlighter language="javascript" style={a11yDark}>{` 
var animals = [
  { name: 'Fluffykins', species: 'rabbit' },
  { name: 'Caro',       species: 'dog' },
  { name: 'Hamilton',   species: 'dog' },
  { name: 'Harold',     species: 'fish' },
  { name: 'Ursula',     species: 'cat' },
  { name: 'Jimmy',      species: 'fish' }
]

var names = animals.filter(function(animal) { 
  return animal.species === 'dog'
})


    `}</SyntaxHighlighter>
      </ul>

      <h3> Funkcja Map</h3>
      <ul>
        <li>
          Iteruje przez tablicę. Jako argument podajemy inną funkcję która
          zwraca jakąś wartość.
        </li>
        <li> Można nią zastąpić pętlę for</li>

        <SyntaxHighlighter language="javascript" style={a11yDark}>{` 
var animals = [
  { name: 'Fluffykins', species: 'rabbit' },
  { name: 'Caro',       species: 'dog' },
  { name: 'Hamilton',   species: 'dog' },
  { name: 'Harold',     species: 'fish' },
  { name: 'Ursula',     species: 'cat' },
  { name: 'Jimmy',      species: 'fish' }
]

var names = animals.map(function(animal) { 
  return animal.name
})

~~~~ES6
var names = animals.map((x) => x.name)
    `}</SyntaxHighlighter>
      </ul>
      <h3> Funkcja Reduce</h3>
      <ul>
        <li>Funkcja obiektu tablicy. Akceptuje DWA argumenty</li>
        <li> Można nią zastąpić pętlę for</li>

        <SyntaxHighlighter language="javascript" style={a11yDark}>{` 
var animals = [
{ name: 'Fluffykins', species: 'rabbit' },
{ name: 'Caro',       species: 'dog' },
{ name: 'Hamilton',   species: 'dog' },
{ name: 'Harold',     species: 'fish' },
{ name: 'Ursula',     species: 'cat' },
{ name: 'Jimmy',      species: 'fish' }
]

var names = animals.map(function(animal) { 
return animal.name
})

~~~~ES6
var names = animals.map((x) => x.name)
  `}</SyntaxHighlighter>
      </ul>
    </>
  );
}

export default FP;
