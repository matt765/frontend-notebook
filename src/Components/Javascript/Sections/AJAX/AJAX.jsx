import SyntaxHighlighter from "react-syntax-highlighter";
import { a11yDark } from "react-syntax-highlighter/dist/esm/styles/hljs";

function AJAX() {
  return (
    <>
      <h2>AJAX</h2>
      <h3>Wprowadzenie</h3>
      <ul>
        <li>
          AJAX czyli Asynchronous JavaScript and XML to technika, wzorzec, który
          umożliwia nam dynamiczne ściąganie i wysyłanie danych bez potrzeby
          przeładowania całej strony.
        </li>
        <li>
          Żądanie trafia na serwer DNS który kieruje ruch na odpowiedni numer
          IP. Po dotarciu do serwera, ten analizuje zapytanie i zwraca odpowiedź
        </li>
        <li>
          Odpowiedź składa się z 1) statusu odpowiedzi 2) 0 lub więcej nagłówków
          3) ciała odpowiedzi - body
        </li>
        <li>
          Status odpowiedzi to oznaczenie, czy dane połączenie zakończyło się
          sukcesem. Na obrazku poniżej wypisano najczęściej spotykane statusy
        </li>

        <li>
          Nagłówek content/type, określa typ MIME danych. Jest to rodzaj danego
          pliku czy dokumentu. Dla przykładu dokument HTML ma typ text/html,
          filmy mp4 mają typ video/mp4, a pliki dźwiękowe WAV mają audio/x-wav.{" "}
        </li>
      </ul>
      <h3>JSON</h3>
      <ul>
        <li>
          JSON to sposób na przechowywanie i przekazywanie danych za pomocą
          zapisu obiektowego składni Javascriptu.
        </li>
        <li>Obiekt ten udostepnia nam 2 metody </li>
        <li>
          Stringify() zamienia dany obiekt na format JSON. Parse() zamienia
          zakodowany wcześniej tekst na obiekt Javascript
        </li>
        <li>JSON nie jest w stanie przechowywać funkcji więc je wycina</li>
      </ul>
      <h3>Fetch API</h3>
      <ul>
        <li>
          Nowy interfejs, a także następca XMLHttpRequest, który podobnie do
          swego brata pozwala pracować z dynamicznymi połączeniami
        </li>
        <li>
          Zwraca obietnicę, którą możemy skonsumować metodami then()/catch() lub
          async/await{" "}
        </li>
        <li>
          Stringify() zamienia dany obiekt na format JSON. Parse() zamienia
          zakodowany wcześniej tekst na obiekt Javascript
        </li>

        <SyntaxHighlighter language="javascript" style={a11yDark}>{` 
    fetch("https://restcountries.eu/rest/v2/name-anka-kaszanka/Poland")
    .then(res => {
        if (res.ok) {
            return res.json()
        } else {
            return Promise.reject(\`Http error: \${res.status}\`);
            //lub rzucając błąd
            //throw new Error(\`Http error: \${res.status}\`);
        }
    })
    .then(res => {
        console.log(res)
    })
    .catch(error => {
        console.error(error)
    });


    try {
      const res = await fetch("https://restcountries.eu/rest/v2/name-anka-kaszanka/Poland")
      if (!res.ok) {
          throw new Error(\`Http error: \${res.status}\`);
      }
      const json = await res.json();
      console.log(json);
    } catch (error) {
      console.error(error);
    }
`}</SyntaxHighlighter>
        <li>
          Drugim parametrem fetch jest obiekt, który pozwala nam przekazywać
          dodatkowe opcje.
        </li>
        <SyntaxHighlighter language="javascript" style={a11yDark}>{` 
  fetch("...", {
    method: 'POST', //*GET, POST, PUT, DELETE, etc.
    mode: 'cors', //no-cors, *cors, same-origin
    cache: 'no-cache', //*default, no-cache, reload, force-cache, only-if-cached
    credentials: 'same-origin', //include, *same-origin, omit
    headers: {
        'Content-Type': 'application/json'
        //'Content-Type': 'application/x-www-form-urlencoded',
    },
    redirect: 'follow', // manual, *follow, error
    referrerPolicy: 'no-referrer', // no-referrer, *client
    body: JSON.stringify(data) //treść wysyłana
  })
`}</SyntaxHighlighter>

        <li>
          Nagłówki takie możemy ustawiać jak powyżej (i zapewne tak będziemy
          robić najczęściej), ale możemy też skorzystać z interfejsu Headers(),
          który udostępnia nam dodatkowe metody do manipulacji pojedynczymi
          nagłówkami
        </li>
        <li> Inny przykład użycia tego API</li>
        <SyntaxHighlighter language="javascript" style={a11yDark}>{`
const ob = {
  name : "Piotrek",
  age : 10,
  pet : {
      type : "ultra dog",
      speed: 1000,
      power : 9001
  }
}

fetch("...", {
      method: "post",
      headers: {
          "Content-Type": "application/json"
      },
      body: JSON.stringify(ob)
  })
  .then(res => res.json())
  .then(res => {
      console.log("Dodałem użytkownika:");
      console.log(res);
  })
`}</SyntaxHighlighter>
        <li>Przykład użycia fetch z async/await</li>
        <SyntaxHighlighter language="javascript" style={a11yDark}>{`
  const getSuggestions = async () => {
    const wordQuery = inputField.value;
    const endpoint = \`\${url}\${queryParams}\${wordQuery}\`;
    try{
  const response = __~await~__ __~fetch(endpoint, {cache: 'no-cache'});
      if(response.ok){
        const jsonResponse = await response.json()
      }
    }
    catch(error){
      console.log(error)
    }
  }
`}</SyntaxHighlighter>
      </ul>
    </>
  );
}

export default AJAX;
