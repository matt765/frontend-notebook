import SyntaxHighlighter from "react-syntax-highlighter";
import { a11yDark } from "react-syntax-highlighter/dist/esm/styles/hljs";

function Modules() {
  return (
    <>
      <h2>Moduły</h2>
      <h3>Wprowadzenie</h3>
      <ul>
        <li>
          Dołączając do index.html pliki ze skryptami są one traktowane jak
          jeden duży skrypt. Oznacza to, że bez problemu możemy z jednego pliku
          odwołać się do zmiennych z innego pliku, ale też powoduje to, że nie
          możemy ponownie zadeklarować takiej samej zmiennej (chyba, że wrzucimy
          ją w jakiś blok lub funkcję).
        </li>
        <li>
          Dlatego wprowadzono mechanizm modułów, aby każdy oddzielny plik był
          zamkniętym środowiskiem z własnymi zmiennymi
        </li>
      </ul>
      <h3>Import i Eksport</h3>
      <ul>
        <li>
          Jeżeli chcemy coś wystawić na zewnątrz stosujemy instrukcję{" "}
          <span className="important">export</span>. Jeżeli chcemy coś
          zaimportować, stosujemy <span className="important">import:</span>{" "}
        </li>
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`//plik functions.js ------
function smallText(txt) {
    return txt.toLowerCase();
}

function bigText(txt) {
    return txt.toUpperCase();
}

function mixText(txt) {
    return [...txt].map((el, i) => i%2 === 0 ? el.toLowerCase() : el.toUpperCase());
}

export { smallText, bigText, mixText }

//plik app.js ------
import { smallText, bigText } from "./functions"; //wybieramy rzeczy do eksportu

console.log( smallText("Ala ma kota") ); //ala ma kota
console.log( bigText("Ala ma kota") ); //ALA MA KOTA
          `}</SyntaxHighlighter>
          </ul>
          <h3>Pojedyncze elementy</h3>
      <ul>
        <li>Eksportowanie pojedynczych elementów indywidualnie</li>
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`//functions.js

function smallText() { }
function bigText() { }
function mixText() { }
export { smallText, bigText, mixText }

//lub

export function smallText() { }
export function bigText() { }
export function mixText() { }

//plik app.js

import { smallText, bigText, mixText } from "./functions";

//lub

import { smallText } from "./functions";
import { bigText } from "./functions";
import { mixText } from "./functions.js";

/// import wielu rzeczy pod wspólną nazwą
import * as fn from "./functions";

fn.smallText();
fn.bigText();
fn.mixText();
`}</SyntaxHighlighter>
      </ul>
    </>
  );
}

export default Modules;
