import SyntaxHighlighter from "react-syntax-highlighter";
import { a11yDark } from "react-syntax-highlighter/dist/esm/styles/hljs";

function StorageCookies() {
  return (
    <>
      <h2>Storage & Cookies</h2>
      <h3>Interfejs Storage</h3>
      <ul>
        <li>
          Przeglądarki udostępniają nam interfejs{" "}
          <span className="important">Storage</span>, który służy do
          przetrzymywania danych. Jest to taki swoisty schowek, w którym możemy
          przetrzymywać różne dane naszej strony. Dostęp do takiego schowka ma
          tylko dana strona w danej przeglądarce danego użytkownika.{" "}
        </li>
        <li>
          Interfejs Storage składa się z session storage oraz local storage. Ten
          pierwszy służy do obsługi danych tylko w czasie trwania sesji (czyli
          do zamknięcia przeglądarki). Ten drugi służy do zapisywania danych na
          nieokreślony czas (aż do ich usunięcia). Obu używa się identycznie{" "}
        </li>
        <li>
          Aby utworzyć nowy element w localStorage możemy skorzystać z funkcji
          setItem(), lub dodać nową właściwość tak samo jak do każdego innego
          obiektu:{" "}
        </li>
        <li>
          Przy odczycie sytuacja jest bardzo podobna. Możemy użyć funkcji
          getItem() lub odwołać się tak samo jak przy innych obiektach
        </li>
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`localStorage.setItem("myElement", "Przykładowa wartość"); //zalecany sposób
localStorage.myElement = "Przykładowa wartość";
localStorage["myElement"] = "Przykładowa wartość"

const element = localStorage.getItem("myElement"); //zalecany sposób
const element = localStorage.myElement;
const element = localStorage["myElement"];
`}</SyntaxHighlighter>

        <li>
          Domyślnie localStorage podobnie do dataset umożliwia przetrzymywanie
          danych jako tekst. Aby móc przetrzymywać w nim obiekty (w tym
          tablice), musimy je zamienić na tekst za pomocą funkcji
          JSON.stringify() oraz pobierając odmienić JSON.parse:{" "}
        </li>
        <SyntaxHighlighter language="javascript" style={a11yDark}>{`const ob = {
    one: 1,
    two: 2,
    three: 3
};

localStorage.setItem("obj", JSON.stringify(ob));

const retrievedObject = JSON.parse(localStorage.getItem("obj"));
console.log(retrievedObject);
`}</SyntaxHighlighter>

        <li>
          Aby usunąć element z localStorage, skorzystamy z funkcji
          localStorage.removeItem()
        </li>
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`localStorage.removeItem("element")`}</SyntaxHighlighter>
        <li>
          Jeżeli chcemy wyczyścić cały localStorage dla danej witryny, posłużymy
          się funkcją localStorage.clear()
        </li>
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`if (confirm("Czy chcesz wyczyścić zapisane dane?")) {
  localStorage.clear()
}`}</SyntaxHighlighter>
      </ul>
      <h3>Cookies</h3>
      <ul>
        <li>
          Ciasteczka są to małe ilości danych, które są przechowywane na twoim
          komputerze za pomocą plików tekstowych i wysyłane są do serwera z
          żądaniami HTTP. Takie pliki zawierają w sobie króciutkie informacje
          takie jak np. id usera, jakiś zakodowany numer, ostatnią wizytę, numer
          aktualnej sesji itp
        </li>
        <li>
          Bardzo często ciasteczka służą do komunikacji przeglądarka-serwer,
        </li>
        <li>
          Jeśli serwer przesyła ciasteczko, w nagłówkach pojawia się
          'Set-Cookie'. Przeglądarka zachowuje ciateczko i następnie przy każdym
          requescie wysyła je w skróconej formie z nagłówkiem 'Cookie'
        </li>
        <li>
          Ciasteczka to jeden duży ciąg danych, który możemy podzielić funkcją
          .split.{" "}
        </li>
        <li> Parametry ciasteczka</li>
        <ul>
          <li>path - określa które podstrony mają do niego dostęp</li>
          <li>
            domain - domena z której będzie dostęp do ciateczka, domyślnie
            dotyczy tylko domeny z której zostały ustawione
          </li>
          <li>
            expires/max-age - ustawienie jak długo ciasteczko będzie aktywne
          </li>
          <li>secure - http/https</li>
        </ul>

        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`/// Utworzenie ciasteczka
document.cookie = "nazwa=wartosc"

/// Utworzenie ciasteczka wraz z enkodowaniem
document.cookie = encodeURIComponent("nazwa użytkownika") + "=" + encodeURIComponent("Karol Nowak");

/// Przykładowe ciasteczko, składa się z kilku części
document.cookie = "user=John; path=/; expires=Tue, 19 Jan 2038 03:14:07 GMT; secure"`}</SyntaxHighlighter>
      </ul>
    </>
  );
}

export default StorageCookies;
