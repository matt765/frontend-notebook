import SyntaxHighlighter from "react-syntax-highlighter";
import { a11yDark } from "react-syntax-highlighter/dist/esm/styles/hljs";

function Misc() {
  return (
    <>
      <h2>Różne</h2>
      <ul>
        <li>
          Ternary operator
          <pre>
            <code className="language-javascript">{`condition ? value1 : value2 `}</code>
          </pre>
        </li>
      </ul>

      <h3>Destrukturyzacja</h3>
      <ul>
        <li>
          Przypisanie destrukturyzujące jest wyrażeniem w JavaScript, które
          pozwala na wyciągnięcie danych z tablic bądź obiektów do odrębnych
          zmiennych.
        </li>
        <li>
          Destrukturyzacja tablic wykonywana jest po kolei, a obiektów po
          kluczach
        </li>

        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`const tab = ["Ala", "Ola", "Ela"];

/// Klasycznie
const name1 = tab[0];
const name2 = tab[1];

/// Za pomocą destrukturyzacji
const [name1, name2] = tab;

/// Operator rest 
const tab = [1, 2, 3, 4, 5];
const [first, ...vars] = tab;

/// Pominięcie 1 wartości
const [name1, name2, , name4] = tab;

/// Ustawienie domyślnej wartości w razie jej braku
const [ name1="brak", name2="brak", name3="brak" ] = tab;

/// Destrukturyzacja obiektu
const obj = {
  first_name : "Marcin",
  last_name : "Kowalski",
}
const {first_name, last_name} = obj `}</SyntaxHighlighter>
      </ul>

      <h3>Strict mode</h3>
      <ul>
        <li>
          Strict mode to funkcjonalność wprowadzona w ES5, która pozwala odpalać
          nasz skrypt w bardziej restrykcyjnym trybie - tak zwanym "strict
          mode".{" "}
        </li>
        <li>
          Tryb ten eliminuje niektóre "ciche" błędy (takie, które nie są
          sygnalizowane przez przeglądarkę) wynikające z przestarzałych
          rozwiązań, lepiej sygnalizuje powszechne kodowe wpadki oraz
          niebezpieczne operacje.{" "}
        </li>
        <li>
          Gdy nasz kod piszemy z wykorzystaniem modułów w ES6 lub wykorzystując
          klasy, nie musimy wtedy używać powyższego przełącznika, ponieważ w
          obydwu przypadkach strict mode jest włączone.{" "}
        </li>
      </ul>

      <h3>Wczytywanie grafik</h3>
      <ul>
        <li>
          Obiekty wczytywane (np. window, image, iframe itp.) posiadają
          zdarzenie load, które wykrywa, czy dany obiekt został w pełni
          załadowany.{" "}
        </li>
        <li>
          Właściwość complete wskazuje czy dany np. obrazek jest wczytany czy
          nie{" "}
        </li>

        <SyntaxHighlighter language="javascript" style={a11yDark}>{` 
//tablica z nazwami obrazków do załadowania
const imgNames = [
    "obrazek1.gif",
    "obrazek2.gif",
    "obrazek3.gif",
    "obrazek4.gif",
    "obrazek5.gif",
    "obrazek6.gif"
];

const loadingStep = (100 / imgNames.length); //szerokość oznaczająca % paska po załadowaniu 1 obrazka

const images = []; //tablica będzie zawierała obiekty Image

const loading = document.querySelector(".loading");
const loadingBar = loading.querySelector(".loading-progress");

//funkcja rozpoczynająca ładowanie obrazków
function startLoading(cb) {
    imgNames.forEach(name => {
        const img = new Image();
        //po wczytaniu grafiki wrzucam nowe Image() do tablicy images
        //sprawdzam ile elementów ma ta tablica i na tej podstawie ustawiam szerokość paska postępu

        img.addEventListener("load", e => {
            images.push(img);
            loadingBar.style.width = \`\${images.length * loadingStep}%\`; //zmieniamy szerokość paska w %

            if (images.length >= imgNames.length) {
                cb(); //odpalam jakąś funkcję po zakończeniu wczytywania
            }
        });
        img.src = name;

        if (img.complete) {
            img.dispatchEvent(new Event("load"));
        }
    });
}

startLoading(function() {
    alert("Zakończono wczytywanie")
});        
  `}</SyntaxHighlighter>

        <li>
          {" "}
          Wczytywanie to proces asynchroniczny, więc jest to idealne miejsce by
          zastosować obietnice lub async/await.
        </li>
        <li>
          Lazy loading to metoda wczytywania obrazków polegająca na tym, że dany
          obrazek wczytywany jest dopiero w momencie gdy pojawi się na ekranie.
        </li>
      </ul>
      <h3>Spread syntax i operator Rest</h3>
      <ul>
        <li>
          Spread syntax, to nowy zapis, który umożliwia rozbijanie iterowanej
          wartości na składowe.
        </li>
        <SyntaxHighlighter language="javascript" style={a11yDark}>{`
//rozbijanie tablicy na poszczególne liczby
const tab = [1, 2, 3, 4];
console.log(...tab); //1, 2, 3, 4

//kopiowanie tablicy
const tab2 = [...tab];

//łączenie tablic
const tabPart = [3, 4]
const tabFull = [1, 2, ...tabPart, 5, 6]; //1, 2, 3, 4, 5, 6

//rozdzielanie tekstu na poszczególne litery
const str = "Ala ma kota";
const tab = [...str]; //["A", "l", "a", " ", "m", "a", " ", "k", "o", "t", "a"]
`}</SyntaxHighlighter>
        <li>
          Identycznie jak spread syntax wygląda rest parameter, różnicą jest
          miejsce użycia - w tym przypadku jako parametr funkcji. Zapis ten
          umożliwia zbieranie w jedną zmienną (będącą tablicą) wielu parametrów
          przekazywanych do funkcji. Musi w parametrach występować jako ostatni
        </li>
        <SyntaxHighlighter language="javascript" style={a11yDark}>{`
function myF(...param) {
      const newTab = [...param];
      newTab.push("Ala");
      console.log(param, newTab); //[1,2,3], [1,2,3,"Ala"]
}
  
myF(1,2,3);
  //pobieranie "pozostałych" wartości z tablicy
function printAbout(name = "Ala", ...other) {
    console.log("To jest " + name);

    if (other.length) {
        console.log(\`\${name} ma zwierzaki: \${other.join()}\`);
    }
}

printAbout("Marcin", "pies", "kot"); //To jest Marcin. Marcin ma zwierzaki: pies,kot
printAbout(); //To jest Ala
    `}</SyntaxHighlighter>
      </ul>

      <h3>Interpolacja i funkcje tagujące</h3>
      <ul>
        <li>
          Interpolacja to zapis, za pomocą którego w łatwy sposób możemy do
          wnętrza tekstu wstawiać inne wartości. Bezpośrednio do takiego tekstu
          możemy wstawiać kod JavaScript, w tym wywoływanie funkcji, zmienne
          itp:{" "}
        </li>
        <li>
          Funkcje tagujące to funkcje, które umożliwiają przekształcanie
          template strings. Są to zwykłe funkcje - jedyną różnicą jest sposób
          ich użycia. Jeżeli chcemy danej funkcji użyć jako funkcji tagującej,
          jej nazwę podajemy tuż przed początkiem template string.{" "}
        </li>
        <li>
          Do funkcji takiej automatycznie są przekazywane w pierwszym parametrze
          poszczególne części template string (znajdujące się między zmiennymi),
          a do kolejnych parametrów zostaną przekazane kolejne zmienne użyte
          wewnątrz tekstu.
        </li>
        <SyntaxHighlighter
          language="javascript"
          style={a11yDark}
        >{`const formatString = (parts, ...vars) => {
    let str = "";
    parts.forEach((el, i) => {
        str += el;
        if (vars[i]) str += \`\${vars[i].toFixed(2)}zl\`;
    });
    return str;
}

const price = 2000;
const diff = 150;

const text = formatString\`Cena produktu A to \${price} i jest o \${diff} tańsza od produktu B\`;

console.log(text); //"Cena produktu A to 2000.00zl i jest o 150.00zl tańsza od produktu B"
`}</SyntaxHighlighter>
      </ul>

      <h3>Garbage Collector</h3>
      <ul>
        <li>
          W Javascript podobnie do wielu języków wysokopoziomowych stosuje się
          automatyczne zarządzanie pamięcią czyli mechanizm zwany Garbage
          Collection. Oznacza to, że gdy tworzymy nowe obiekty, Javascript
          automatycznie przydziela im pamięć, a gdy nie są już przez nas
          używane, są one z niej automatycznie usuwane
        </li>
        <li>
          Garbage Collector cyklicznie co jakiś czas rozpoczyna swoją pracę od
          głównego obiektu. Oznacza go jako obiekt z referencją (mark).
          Następnie poprzez jego odwołania przechodzi do innych obiektów - je
          także oznaczając. Następnie GC sprawdza te obiekty i ich odwołania do
          kolejnych obiektów, oznacza je i tak dalej, aż dojdzie do końca. Po
          oznaczeniu wszystkich obiektów do których prowadzą jakiekolwiek
          referencje, GC zaczyna usuwać obiekty nieużywalne, do których nie było
          żadnych odwołań.
        </li>
        <li>
          {" "}
          Oznacza to że jeśli pod zmienną prowadzącą do obiektu podstawimy coś
          innego to GC usuwa ten obiekt z pamięci, ale jeśli w innym miejscu
          kodu istnieje inna referencja do tego obiektu to nie zostaje on
          usunięty.
        </li>
      </ul>

      <h3>Symbole</h3>
      <ul>
        <li>
          Typ danych symbol to prymitywny typ danych, który zawsze zawiera
          unikalną wartość. Nie wiemy ile ona wynosi, wiemy jedynie to, że jest
          unikalna, niepowtarzalna. Aby wygenerować taką wartość, posłużymy się
          funkcją Symbol()
        </li>
        <li>
          Podczas generowania symbolu możemy podać dodatkową opcjonalną wartość,
          która będzie opisem danego symbolu. Opis ten nie zmienia wartości, a
          najczęściej używany jest do celów debugowania.
        </li>
        <li>
          Symbole przydają się w sytuacjach gdzie chcemy dodawać do obiektów
          dodatkowe funkcjonalności, a równocześnie nie chcemy się martwić o to,
          że przypadkowo w takim obiekcie coś nadpiszemy.{" "}
        </li>
        <li>
          Właściwości kryjące się za symbolem nie są dodatkowo iterowalne, więc
          idealnie nadają się do tworzenia "ukrytych" właściwości:
        </li>
        <SyntaxHighlighter language="javascript" style={a11yDark}>{`
            const person = {
              firstName: "John",
              lastName: "Doe",
              age: 50,
              eyeColor: "blue"
            };
            
            let id = Symbol('id');
            person[id] = 140353;
            // Now Person[id] = 140353
            // but person.id is still undefined 

          `}</SyntaxHighlighter>
      </ul>
      <h3>Hoisting</h3>
      <ul>
        <li>
          Zmienne i funkcje w Javascript przed wykonaniem kodu są umieszczone w
          przeznaczonej dla nich przestrzeni w pamięci, stając się od razu
          dostępne w momencie rozpoczęcia wykonywania kodu.
        </li>
        <li>Zmienne var są hoistowane z wartością undefined</li>
      </ul>
    </>
  );
}

export default Misc;
