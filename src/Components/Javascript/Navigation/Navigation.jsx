import { RightOutlined } from "@ant-design/icons";

export const Navigation = () => {
 
  return (
    
    <>
      <div className="navigation">
        <a href="#start" className="navigation-box">
          <div className="navigation-title">Start</div>
        </a>
       
        <div className="navigation-box">
          <a href="#1" className="navigation-title">Funkcje</a>
          <div className="navigation-icon">
            <RightOutlined />
          </div>
        </div>
        <div className="navigation-list-container invisible">
          <div className="navigation-list-element">
            <a href="#funkcje/map">Map()</a>
            </div>
          <div className="navigation-list-element">
          <a href="#funkcje/reduce">Reduce()</a>
            </div>
          <div className="navigation-list-element">
          <a href="#funkcje/filter">Filter()</a>
            </div>
          <div className="navigation-list-element"><a href="#funkcje/every">Every()</a></div>
          <div className="navigation-list-element"><a href="#funkcje/some">Some()</a></div>
          <div className="navigation-list-element"><a href="#funkcje/includes">Includes()</a></div>
          <div className="navigation-list-element"><a href="#funkcje/find">Find()</a></div>
          <div className="navigation-list-element"><a href="#funkcje/object.keys">Object.keys()</a></div>
          <div className="navigation-list-element"><a href="#funkcje/object.values">Object.values()</a></div>
          <div className="navigation-list-element"><a href="#funkcje/object.entries">Object.entries()</a></div>
          <div className="navigation-list-element"><a href="#funkcje/bind">Bind()</a></div>
          <div className="navigation-list-element"><a href="#funkcje/setTimeout">setTimeout()</a></div>
          <div className="navigation-list-element"><a href="#funkcje/setInterval">setInterval()</a></div>
          <div className="navigation-list-element"><a href="#funkcje/IIFE">IIFE</a></div>
        </div>
        <a href="#2" className="navigation-box">
          <div className="navigation-title">Programowanie obiektowe</div>
          <div className="navigation-icon">
            <RightOutlined />
          </div>
        </a>
        <a href="#3" className="navigation-box">
          <div className="navigation-title"> Map & Set</div>
          <div className="navigation-icon">
            <RightOutlined />
          </div>
        </a>
        <a href="#4" className="navigation-box">
          <div className="navigation-title">DOM</div>
          <div className="navigation-icon">
            <RightOutlined />
          </div>
        </a>
        <a href="#5" className="navigation-box">
          <div className="navigation-title">Asynchronicznosc</div>
          <div className="navigation-icon">
            <RightOutlined />
          </div>
        </a>
        <a href="#6" className="navigation-box">
          <div className="navigation-title">AJAX</div>
          <div className="navigation-icon">
            <RightOutlined />
          </div>
        </a>
        <a href="#7" className="navigation-box">
          <div className="navigation-title">Moduły</div>
          <div className="navigation-icon">
            <RightOutlined />
          </div>
        </a>
        <a href="#8" className="navigation-box">
          <div className="navigation-title">Wzorce projektowe</div>
          <div className="navigation-icon">
            <RightOutlined />
          </div>
        </a>
        <a href="#9" className="navigation-box">
          <div className="navigation-title">Programowanie funkcyjne</div>
          <div className="navigation-icon">
            <RightOutlined />
          </div>
        </a>
        <a href="#10" className="navigation-box">
          <div className="navigation-title">Storage & Cookies</div>
          <div className="navigation-icon">
            <RightOutlined />
          </div>
        </a>
        <a href="#11" className="navigation-box">
          <div className="navigation-title">Iteratory i generatory</div>
          <div className="navigation-icon">
            <RightOutlined />
          </div>
        </a>
        <a href="#12" className="navigation-box">
          <div className="navigation-title">Różne</div>
          <div className="navigation-icon">
            <RightOutlined />
          </div>
        </a>
        <a href="#13" className="navigation-box">
          <div className="navigation-title">Źródła</div>
        </a>
      </div>
    </>
  );
}


