import Start from './Start/Start'
import Functions from './Sections/Functions/Functions'
import Objects from './Sections/Objects/Objects'
import MapSet from './Sections/MapSet/MapSet'
import DOM from './Sections/DOM/DOM'
import Asynchronous from './Sections/Async/Async'
import AJAX from './Sections/AJAX/AJAX'
import Modules from './Sections/Modules/Modules'
import DesignPatterns from './Sections/DesignPatterns/DesignPatterns'
import FP from './Sections/FP/FP'
import StorageCookies from './Sections/StorageCookies/StorageCookies'
import Iterators from './Sections/Iterators/Iterators'
import Misc from './Sections/Misc/Misc'
import Sources from './Sources/Sources'

function Javascript(props) {

    return (
        <div className="App">
            <div className="main-container">
               
              
                <div className="text-container">
                    <div className="start-container" id="start">
                        <Start />
                    </div>
                    <div className="text-box" id="1">
                        <Functions />
                    </div>
                    <div className="text-box" id="2">
                        <Objects />
                    </div>
                    <div className="text-box" id="3">
                        <MapSet />
                    </div>
                    <div className="text-box" id="4">
                        <DOM />
                    </div>
                    <div className="text-box" id="5">
                        <Asynchronous />
                    </div>
                    <div className="text-box" id="6">
                        <AJAX />
                    </div>
                    <div className="text-box" id="7">
                        <Modules />
                    </div>
                    <div className="text-box" id="8">
                        <DesignPatterns />
                    </div>
                    <div className="text-box" id="9">
                        <FP />
                    </div>
                    <div className="text-box" id="10">
                        <StorageCookies />
                    </div>
                    <div className="text-box" id="11">
                        <Iterators />
                    </div>
                    <div className="text-box" id="12">
                        <Misc />
                    </div>
                    <div className="text-box" id="13">
                        <Sources />
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Javascript;