import SyntaxHighlighter from "react-syntax-highlighter";
import { a11yDark } from "react-syntax-highlighter/dist/esm/styles/hljs";

function Intro() {
  return (
    <>
      <h3>Wprowadzenie</h3>
      <div className="text-section">
        <ul class="visible">
          <li>
            Docker to platforma, dzięki której możemy rozwijać i wdrażać
            aplikacje w postaci samowystarczalnych kontenerów, które mogą
            działać na dowolnym systemie operacyjnym, pozwalając programistom na
            odtworzenie środowiska aplikacji na wybranym komputerze. Docker
            umożliwia też automatyzację procesów związanych z wdrażaniem
            skonteneryzowanego oprogramowania
          </li>

          <li>
            Kontener pozwala na uruchomienie programu jako pojedynczego procesu
            będącego odizolowanym środowiskiem zawierającym wszystkie zależności
            wymagane do jego działania
          </li>

          <li>
            Plik tekstowy nazywany Dockerfile zawiera informacje, jakie wersje
            bibliotek powinny być zainstalowane, skonfigurowane i uruchomione w
            określonej kolejności. Jest wzorem na podstawie którego budowany
            jest obraz Dockera. Każda kolejna instrukcja w Dockerfile nazywana
            jest warstwą.
          </li>
          <SyntaxHighlighter language="javascript" style={a11yDark}>
            {`# syntax=docker/dockerfile:1
FROM ubuntu:18.04
COPY . /app
RUN make /app
CMD python /app/app.py
`}
          </SyntaxHighlighter>
          <li>
            Obrazy są plikami tylko do odczytu i zawierają wszystkie biblioteki
            systemowe, narzędzia i środowisko uruchomieniowe potrzebne do
            uruchomienia aplikacji. Docker potrafi odczytać taki obraz i
            korzystając z niego uruchomić kontener, który jest instancją obrazu.
            Każdy obraz Dockera powstaje w wyniku wykonania szeregu poleceń.
            Każde z tych poleceń tworzy osobną warstwę obrazu. Kontener nadal
            działa nawet po zamknięciu okna terminala
          </li>
          <li>
            Docker Hub to publiczna platforma będąca darmowym repozytorium
            obrazów, w którym znajdziemy np. NodeJS czy MongoDB, a także czyste
            systemy operacyjne jak Ubuntu. Raz zapisany obraz nie będzie
            pobierany ponownie przy uruchamianiu nowego kontenera
          </li>
          <li>
            Wszystkie uruchamiane kontenery tak naprawdę są uruchamiane w
            Linuxowej maszynie wirtualnej. W takim przypadku będziemy mieli
            dostęp do Shella. Jest to swego rodzaju wiersz poleceń dla środowisk
            Unix-owych. Możemy w nim za pomocą komend wydawać polecenia i
            poruszać się po systemie.
          </li>
          <li>
            Docker pozwala na udostępnianie portów kontenerów w celu wymiany
            informacji między nimi. Innym sposobem jest współdzielenie zasobów z
            użyciem Volume, który jest wydzielonym folderem zawierającym
            wspóldzielone pliki różne kontenery
          </li>
          <li>
            ???Plik docker-compose ?? docker build -t
            uzytkownikDockerHub/nazwaObrazu:wersja ?? ?? docker run [nazwa lub
            id obrazu] ls - pokazuje listę plików kopiowanych z obrazu do
            kontenera
          </li>
          <li>
            Na systemie Windows z Dockera możemy korzystać instalując Docker
            Desktop, który i zawiera Dashboard umożliwiający m.in. zarządzanie
            kontenerami i zawierający ich logi. Docker składa się z 2
            podstawowych elementów: Docker Server/Demon (silnik zarządzający
            obrazami i kontenerami) i Docker Client (wiersz poleceń).
            Komunikacja między nimi odbywa się za pomocą protokołu HTTP Warto
            zainstalować też wtyczkę Docker do VSC.
          </li>
          <li>
            Jeżeli wewnątrz kontenera nie jest uruchomiony żaden proces bądź
            uruchomiony proces się zakończy, kontener samoczynnie się wyłączy.
          </li>
        </ul>
      </div>
    </>
  );
}

export default Intro;
