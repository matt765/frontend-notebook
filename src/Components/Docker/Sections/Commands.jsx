import SyntaxHighlighter from "react-syntax-highlighter";
import { a11yDark } from "react-syntax-highlighter/dist/esm/styles/hljs";

function Commands() {
  return (
    <>
      <h3>Podstawowa obsługa Dockera</h3>
      <div className="text-section">
     
          <SyntaxHighlighter language="javascript" style={a11yDark}>
            {`//  Lista wszystkich komend 
docker

// Szczegóły na temat pojedynczego polecenia
docker [nazwa polecenia] --help  

// Wersja Dockera 
docker version

// Lista uruchomionych kontenerów 
docker ps

// Lista wszystkich kontenerów 
docker ps --all

// Lista zapisanych obrazów
docker images`}
          </SyntaxHighlighter>
        
      </div>
      <h3>Obsługa kontenerów</h3>
      <div className="text-section">
   
          <SyntaxHighlighter language="javascript" style={a11yDark}>
            {`// Tworzy kontener na podstawie obrazu, zwraca unikalny ID kontenera 
docker create [nazwa lub id obrazu] 

// Uruchomienie istniejącego już kontenera, wystarczy kilka pierwszych znaków 
docker start [id lub nazwa kontenera] 

// Uruchomienie kontenera bezpośrednio z obrazu z Docker Hub 
docker run [nazwa lub id obrazu] 

// Drugi argument pozwala na nadpisanie domyślnego startowego polecenia z obrazu 
docker run [nazwa lub id obrazu] [polecenie startowe] 

// Tworzy kontener i nadaje mu nazwę 
docker run --name="[nazwa dla kontenera]" [nazwa lub id obrazu] 

// Uruchomienie kontenera w tle, a nie domyślnie jako proces pierwszoplanowy 
docker run -d [nazwa lub id obrazu] 

// Wyświetlenie logów
docker logs [id lub nazwa kontenera] 

// Wyłączenie kontenera, daje aplikacji 10 sekund
docker stop [id lub nazwa kontenera]

// Natychmiastowe wyłączenie kontenera 
docker kill [id lub nazwa kontenera] 

// Usuwa kontener 
docker rm [id lub nazwa kontenera] 

// Usuwa wszystkie zatrzymane kontenery 
docker rm $(docker ps -a -q) 

// Podgląd aktywności głównego procesu działającego kontenera, 
// poprzez połączenie własnego okna terminala ze strumieniami wejścia wyjścia procesu kontenera
docker attach [id lub nazwa kontenera] 

// Wykonuje polecenie w kontenerze
docker exec [id lub nazwa kontenera] [polecenie]`}
          </SyntaxHighlighter>
        
      </div>
      <h3>Obsługa obrazów</h3>
      <div className="text-section">
     
          <SyntaxHighlighter language="javascript" style={a11yDark}>
            {`// Pobranie najnowszej wersji obrazu z Docker Hub
docker pull [nazwa lub id obrazu] 

// Pobranie wybranej wersji obrazu z Docker Hub
docker pull [nazwa lub id obraz]:[tag / numer wersji] 

// Lista warstw składających się na obraz
docker history [nazwa lub id obrazu] 

// Usunięcie obrazu
docker rmi [nazwa lub id obrazu] 

// Usunięcie wszystkich obrazów
docker rmi $(docker images -q) 
`}
          </SyntaxHighlighter>
        
      </div>
      <h3>Udostępnianie portów</h3>
      <div className="text-section">
       
          <SyntaxHighlighter language="javascript" style={a11yDark}>
            {`// Uruchomienie kontenera wraz z przekierowaniem z jego portu na wybrany port gospodarza
docker run [id lub nazwa kontenera] -p [port lokalny]:[port kontenera] 

// Uruchomienie kontenera wraz z wyeksponowaniem wybranego portu
docker run --expose=[numer portu] [nazwa lub id obrazu] 

// Uruchamia kontener i łączy go z wybranym kontenerem podanym przy parametrze --link
docker run --link="[id lub nazwa kontenera]" [nazwa lub id obrazu]          `}
          </SyntaxHighlighter>
        
      </div>
      <h3>Obsługa volume</h3>
      <div className="text-section">
      
          <SyntaxHighlighter language="javascript" style={a11yDark}>
            {`// Utworzenie volume
docker volume create shared-stuff

// Usunięcie volume 
docker volume rm 

// Usunięcie wszystkich nieużywanych Volume'ów
docker volume prune

// Usunięcie wszystkich nieużywanych zasobów takich  jak kontenery i obrazy
docker system prune `}
          </SyntaxHighlighter>
        
      </div>
    </>
  );
}

export default Commands;
