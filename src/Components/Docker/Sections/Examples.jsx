import SyntaxHighlighter from "react-syntax-highlighter";
import { a11yDark } from "react-syntax-highlighter/dist/esm/styles/hljs";

function Examples() {
  return (
    <>
      <h3> Przykłady użycia</h3>
      <div className="text-section">
        
            Stworzenie kontenera z nazwą "postgres-container" z obrazu postgres
            (baza danych)
          
          <SyntaxHighlighter language="javascript" style={a11yDark}>
            {`docker run --name="postgres-container" postgres`}
          </SyntaxHighlighter>

          
            Utworzenie konteneru z obrazu "ubuntu" o losowej nazwie, który
            odczeka 10 sekund
          
          <SyntaxHighlighter language="javascript" style={a11yDark}>
            {`docker run ubuntu sleep 10`}
          </SyntaxHighlighter>

          
            Uruchamia shell jako kolejny proces wewnątrz uruchomionego kontenera{" "}
          
          <SyntaxHighlighter language="javascript" style={a11yDark}>
            {`docker exec -it [id lub nazwa kontenera] sh `}
          </SyntaxHighlighter>

          Lista baz danych posgres dostępnych w kontenerze
          <SyntaxHighlighter language="javascript" style={a11yDark}>
            {`docker exec postgres-container psql -U postgres -c"\list"`}
          </SyntaxHighlighter>

          Stworzenie własnej bazy danych postgres o nazwie "docker"
          <SyntaxHighlighter language="javascript" style={a11yDark}>
            {`docker exec postgres-container psql -U postgres -c"CREATE DATABASE docker" `}
          </SyntaxHighlighter>

          
            Swobodne przeglądanie plików przez uzyskanie dostępu do strumieni
            wejścia i wyjścia
          
          <SyntaxHighlighter language="javascript" style={a11yDark}>
            {`docker exec -it [id lub nazwa kontenera] bash`}
          </SyntaxHighlighter>

          
            Utworzenie kontenera bazy danych z przekierowaniem portu 5432 na
            9500
          
          <SyntaxHighlighter language="javascript" style={a11yDark}>
            {`docker run -d --name="postgres-container-9.5" -p 9500:5432 postgres:9.5`}
          </SyntaxHighlighter>

          Uruchomienie kontenera bazy danych z wyeksponowaniem portu
          <SyntaxHighlighter language="javascript" style={a11yDark}>
            {`docker run -d --expose=5432 --name="postgres-container" postgres`}
          </SyntaxHighlighter>

          
            Uruchomienie kontenera ubuntu i połączenie go z kontenerem bazy
            danych
          
          <SyntaxHighlighter language="javascript" style={a11yDark}>
            {`docker run -d -it --name="ubuntu-container" --link="postgres-container" ubuntu`}
          </SyntaxHighlighter>
     
      </div>
    </>
  );
}

export default Examples;
