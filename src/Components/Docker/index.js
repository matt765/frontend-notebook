import Start from './Start/Start'
import Examples from './Sections/Examples'
import Intro from './Sections/Intro'
import Commands from './Sections/Commands'

function Docker() {
    return (
        <div className="App">
            <div className="main-container">

                <div className="text-container">
                    <div className="start-container" id="start">
                        <Start />
                    </div>
                    <div className="text-box" id="1">
                        <Intro />
                    </div>
                    <div className="text-box" id="2">
                        <Commands />
                    </div>
                    <div className="text-box" id="3">
                        <Examples />
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Docker;