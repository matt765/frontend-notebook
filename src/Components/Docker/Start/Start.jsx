import logo from "../../../Images/js.png";
import arrow from "../../../Images/arrow.png";

function Start() {
  return (
    <>
      <div class="title-container">
        <div class="logo-container">
          <div class="logo-image-container">
            <img src={logo} />
          </div>
          <div class="logo-title-container">
            <div class="logo-title">Docker</div>
            
          </div>
        </div>
        <div class="basics-container">
          <div class="basics-title"></div>
          <div class="basics-links">
            <div class="basics-link">
              <a href="https://developer.mozilla.org/pl/docs/Web/JavaScript/Guide/Introduction">
                Wprowadzenie
              </a>
            </div>
            <div class="basics-link">
              <a href="https://developer.mozilla.org/pl/docs/Web/JavaScript/Guide/Grammar_and_types#deklaracje">
                Zmienne
              </a>
            </div>
            <div class="basics-link">
              <a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Expressions_and_Operators">
                Operatory
              </a>
            </div>
            <div class="basics-link">
              <a href="https://developer.mozilla.org/pl/docs/Web/JavaScript/Guide/Grammar_and_types#struktury_i_typy_danych">
                Typy danych
              </a>
            </div>
            <div class="basics-link">
              <a href="https://developer.mozilla.org/pl/docs/Web/JavaScript/Guide/Control_flow_and_error_handling#instrukcje_warunkowe">
                Instrukcje warunkowe
              </a>
            </div>
          </div>
        </div>
        <div class="arrow">
          <a href="#1">
            <img src={arrow} />
          </a>
        </div>
      </div>
    </>
  );
}

export default Start;
